Contacting Us
==============

General information on the review can be found on the Review pages at https://ddi-alliance.atlassian.net/wiki/display/DDI4/How+to+Review+the+2016+Q2+Development+Draft

Comments and questions on the content of this document and the review process more generally should be directed to the Technical Committee list <mailto:ddi-srg@icpsr.umich.edu> or the Technical
Committee Chair, Wendy Thomas wlt@umn.edu.



