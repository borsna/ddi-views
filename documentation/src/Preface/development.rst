Development of the Model
========================

The development of the model has been underway since October 2012 when a first workshop was held
at Schloss Dagstuhl. The work has been carried forward subsequently through a series of Sprints, virtual
meetings and online discussions.

* Sprint  1: Schloss Dagstuhl, October 28-November 1, 2013
* Sprint  2: Paris, December 6-7, 2013
* Sprint  3: Vancouver, March 24-28, 2014
* Sprint  4: Toronto, May 26-30, 2014
* Sprint  5: Schloss Dagstuhl, October 20-24, 2014
* Sprint  6: London, November 24 - 28, 2014
* Sprint  7: Minneapolis, May 25 - 30 2015
* Sprint  8: Schloss Dagstuhl, October 19 - 23 2015
* Sprint  9: Copenhagen, December 23 - 27 2015
* Sprint 10: Edmonton, April 11 - 15 2016
* Sprint 11: Knutholmen, May 23 - 27 2016
