Funding
========

Development of the model has been primarily supported by the DDI Alliance, and by in-kind contributions
from participants in the effort. DDI Alliance members and their host institutions have generously provided
venues and infrastructure for Sprints. The Schloss Dagstuhl sprints are part of a long running series of
annual events hosted by the Leibniz Centre for Informatics. The October 2014 Sprint was funded in part
by the National Science Foundation under award 1448107.
