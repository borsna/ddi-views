==============================
Views
==============================
.. image:: ../../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   AgentRegistryView/index.rst
   DataDictionaryView/index.rst
   DiscoveryView/index.rst
   InstrumentView/index.rst
   StatisticalClassificationView/index.rst
