.. _ConceptPartWholePair:


ConceptPartWholePair
********************
Identifies the concept defining the Whole and the concept defining the Part.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========  ================
Name      Type         Cardinality  allways external
========  ===========  ===========  ================
part      Concept      0..n           no
realizes  OrderedPair  0..n           yes
whole     Concept      0..n           no
========  ===========  ===========  ================


part
####
Specialization of "target" in OrderedPair




realizes
########
realizes pattern OrderedPair




whole
#####
Specialization of "source" in OrderedPair.






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptPartWholePair.dot