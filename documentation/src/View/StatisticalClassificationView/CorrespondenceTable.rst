.. _CorrespondenceTable:


CorrespondenceTable
*******************
A Correspondence Table expresses a relationship between two NodeSets.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
effectiveDates  DateRange  0..1
==============  =========  ===========


effectiveDates
##############
Effective period of validity of the CorrespondenceTable. The correspondence table expresses the relationships between the two NodeSets as they existed on the period specified in the table.

`


Relationships
=============

===============  ==================  ===========  ================
Name             Type                Cardinality  allways external
===============  ==================  ===========  ================
contactPersons   Agent               0..n           no
contains         Map                 1..n           no
hasPublication   ExternalMaterial    0..n           no
maintenanceUnit  Agent               0..n           no
maps             NodeSet             0..n           no
owners           Agent               0..n           no
realizes         AsymmetricRelation  0..n           yes
sourceLevel      Level               0..n           no
targetLevel      Level               0..n           no
===============  ==================  ===========  ================


contactPersons
##############
The person(s) who may be contacted for additional information about the Correspondence Table. Can be an individual or organization.




contains
########
Set of mappings between nodes that participate in the correspondence.




hasPublication
##############
A list of the publications in which the Correspondence Table has been published.




maintenanceUnit
###############
The unit or group of persons who are responsible for the Correspondence Table, i.e. for maintaining and updating it.




maps
####
The NodeSet(s) from which the correspondence is made. 




owners
######
The statistical office, other authority or section that created and maintains the Correspondence Table. A Correspondence Table may have several owners.




realizes
########
Class of the Collection pattern realized by this class. 




sourceLevel
###########
Level from which the correspondence is made. Correspondences might be restricted to a certain Level in the NodeSet. In this case, target items are assigned only to source items on the given level. If no level is indicated, source items can be assigned to any level of the target NodeSet.




targetLevel
###########
Level to which the correspondence is made. Correspondences might be restricted to a certain Level in the NodeSet. In this case, target items are assigned only to source items on the given level. If no level is indicated, target items can be assigned to any level of the source NodeSet.






Graph
=====

.. graphviz:: /images/graph/Representations/CorrespondenceTable.dot