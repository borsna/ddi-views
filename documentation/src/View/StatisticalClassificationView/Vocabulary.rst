.. _Vocabulary:


Vocabulary
**********
A vocabulary is an established list of standardized terminology for use in indexing and retrieval of information.



Extends
=======
:ref:`ConceptSystem`


Properties
==========

============  ===================  ===========
Name          Type                 Cardinality
============  ===================  ===========
abbreviation  InternationalString  0..n
comments      StructuredString     0..n
location      URI                  0..1
============  ===================  ===========


abbreviation
############
Abbreviation of vocabulary title.


comments
########
Information for the user regarding the reasons for use of the vocabulary and appropriate usage constraints.


location
########
Location of external resource providing information about the vocabulary.

`


Relationships
=============

====================  ===========  ===========  ================
Name                  Type         Cardinality  allways external
====================  ===========  ===========  ================
containsDesignations  Designation  0..n           no
====================  ===========  ===========  ================


containsDesignations
####################
Vocabularies contain Designations






Graph
=====

.. graphviz:: /images/graph/Representations/Vocabulary.dot