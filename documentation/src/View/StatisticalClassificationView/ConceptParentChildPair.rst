.. _ConceptParentChildPair:


ConceptParentChildPair
**********************
Specifies the Parent Concept and Child Concept in the Concept Parent Child Relationship.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========  ================
Name      Type         Cardinality  allways external
========  ===========  ===========  ================
child     Concept      0..n           no
parent    Concept      0..n           no
realizes  OrderedPair  0..n           yes
========  ===========  ===========  ================


child
#####
Specialization of "target" in OrderedPair




parent
######
Specialization of "source" in OrderedPair




realizes
########
realizes the pattern OrderedPair






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptParentChildPair.dot