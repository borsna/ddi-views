*****************************
StatisticalClassificationView
*****************************
Purpose:
The Statistical Classification View brings together the structures need to record, organize, manage, and index Statistical Classifications. These classifications are often developed cooperatively and managed over time. They may be organized into series and families of classifications and use specified vocabularies. Mapping may be provided to express relationships between two or more Statistical Classifications in order to clarify change over time or the similarities and differences between Statistical Classifications in the same family (i.e. Industry, Occupation, Education).

Use Cases:
Industry: US-SIC, NAICS, GICS, ISIC; Occupational: ISCO, US-SOC; Geographic: US-FIPS, NUTS

Target Audience:
Organizations maintaining and managing shared Statistical Classifications.

Included Classes: 
AuthorizationSource, Category, ClassificationFamily, ClassificationIndex, ClassificationIndexEntry, ClassificationItem, ClassificationSeries, Code, Concept, ConceptParentChild, ConceptParentChildPair, ConceptPartWhole, ConceptPartWholePair, ConceptSystem, CorrespondenceTable, ExternalMaterial, Individual, Level, Map, Organization, StatisticalClassification, Vocabulary.

General Documentation:
The abstract class Designation serves as a relationship target for the following classes: ClassificationItem and Vocabulary. The only included class belonging to this abstract group is Code.
The abstract class Node serves as a relationship target for the following classes: Map and Level. The only included class belonging to this abstract group is ClassificationItem.
The abstract class NodeSet serves as a relationship target for the following class: CorrespondenceTable. The only included class belonging to this abstract group is StatisticalClassification.
The abstract class Agent serves as a relationship target for the following classes: CorrespondenceTable and AuthorizationSource. The only included classes belonging to this abstract group are Organization and Individual.
The abstract pattern class OrderedPair serves as a relationship target for the following class: Map. The only included class realizing this pattern is ConceptParentChildPair


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AuthorizationSource
   Category
   ClassificationFamily
   ClassificationIndex
   ClassificationIndexEntry
   ClassificationItem
   ClassificationSeries
   Code
   Concept
   ConceptParentChild
   ConceptParentChildPair
   ConceptPartWhole
   ConceptPartWholePair
   ConceptSystem
   CorrespondenceTable
   ExternalMaterial
   Individual
   Level
   Map
   Organization
   StatisticalClassification
   Vocabulary



Graph
=====

.. graphviz:: /images/graph/StatisticalClassificationView/StatisticalClassificationView.dot