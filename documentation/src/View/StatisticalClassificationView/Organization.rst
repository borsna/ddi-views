.. _Organization:


Organization
************
A framework of authority designated to act toward some purpose.



Extends
=======
:ref:`Agent`


Properties
==========

==================  ==================  ===========
Name                Type                Cardinality
==================  ==================  ===========
contactInformation  ContactInformation  0..1
ddiId               String              0..n
organizationName    OrganizationName    1..n
==================  ==================  ===========


contactInformation
##################
Contact information for the organization including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.


ddiId
#####
The agency identifier of the organization as registered at the DDI Alliance register.


organizationName
################
Names by which the organization is known.




Graph
=====

.. graphviz:: /images/graph/Agents/Organization.dot