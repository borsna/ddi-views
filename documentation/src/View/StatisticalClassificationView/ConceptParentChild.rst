.. _ConceptParentChild:


ConceptParentChild
******************
Parent-child specialization of OrderRelation between Concepts within a ConceptSystem.



Extends
=======
:ref:`Identifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti_Reflexive


semantics
#########
Controlled vocabulary for the ImmediatePrecedenceRelation semantics. It should contain, at least, the following: Parent_Of, Child_Of, Next, Previous, Instance_Of, Temporal_Meets.


symmetry
########
Fixed to Anti_Symmetric


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


transitivity
############
Fixed to Anti_Transitive

`


Relationships
=============

==========  ===========================  ===========  ================
Name        Type                         Cardinality  allways external
==========  ===========================  ===========  ================
contains    ConceptParentChildPair       0..n           no
realizes    ImmediatePrecedenceRelation  0..n           yes
structures  ConceptSystem                0..n           no
==========  ===========================  ===========  ================


contains
########
Ordered sets of Parent Child pairs.




realizes
########
Uses the pattern ImmediatePrecedenceRelation




structures
##########
Concept System whose members are grouped by pairs to form a variety of structures.






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptParentChild.dot