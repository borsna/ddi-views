.. _ClassificationFamily:


ClassificationFamily
********************
A Classification Family is a group of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept (e.g. economic activity).[GSIM1.1]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

======================  ====================  ===========  ================
Name                    Type                  Cardinality  allways external
======================  ====================  ===========  ================
groups                  ClassificationSeries  1..n           no
hasClassificationIndex  ClassificationIndex   0..n           no
realizes                Collection            0..n           yes
======================  ====================  ===========  ================


groups
######
Specialization of contains in Collection




hasClassificationIndex
######################
ClassificationIndexes associated to the ClassificationFamily.




realizes
########
Class of the Collection pattern realized by this class.







Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationFamily.dot