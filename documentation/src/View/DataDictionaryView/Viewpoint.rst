.. _Viewpoint:


Viewpoint
*********
The assignment of measure, identifier and attribute roles to InstanceVariables



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

=================  ==============  ===========  ================
Name               Type            Cardinality  allways external
=================  ==============  ===========  ================
hasAttributeRole   AttributeRole   1..n           no
hasIdentifierRole  IdentifierRole  1..1           no
hasMeasureRole     MeasureRole     1..1           no
=================  ==============  ===========  ================


hasAttributeRole
################
The InstanceVaribles functioning as attributes




hasIdentifierRole
#################
The InstanceVaribles functioning as identifiers




hasMeasureRole
##############
The InstanceVaribles functioning as measures






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/Viewpoint.dot