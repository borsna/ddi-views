.. _InstanceVariable:


InstanceVariable
****************
The use of a Represented Variable within a Data Set.  



Extends
=======
:ref:`RepresentedVariable`


Properties
==========

================  =================================  ===========
Name              Type                               Cardinality
================  =================================  ===========
physicalDataType  ExternalControlledVocabularyEntry  0..1
variableRole      StructuredString                   0..1
================  =================================  ===========


physicalDataType
################
The data type of this variable. Supports the optional use of an external controlled vocabulary.


variableRole
############
An Instance Variable can take different roles, e.g. Identifier, Measure and Attribute. Note that DataStructure takes care of the ordering of Identifiers.

`


Relationships
=============

==========================  ===================  ===========  ================
Name                        Type                 Cardinality  allways external
==========================  ===================  ===========  ================
basedOnConceptualVariable   ConceptualVariable   0..n           no
basedOnRepresentedVariable  RepresentedVariable  0..n           no
measures                    Population           0..n           no
takesSentinelValuesFrom     SentinelValueDomain  0..n           no
==========================  ===================  ===========  ================


basedOnConceptualVariable
#########################
The ConceptualVariable that can be shared by multiple InstanceVariables. Indicates comparability ConceptualDomain and UnitType. If a relationship to a RepresentedVariable is defined there should be no relationship to a ConceptualVariable.




basedOnRepresentedVariable
##########################
The RepresentedVariable that can be shared by multiple InstanceVariables. Indicates comparability in substantive representation, Universe, and indirectly ConceptualDomain. If a relationship to a RepresentedVariable is defined there should be no relationship to a ConceptualVariable.




measures
########
Set of specific units (people, entities, objects, events), usually in a given time and geography, being measured. Can be a specialization of the Universe measured by a related RepresentedVariable.




takesSentinelValuesFrom
#######################
The association to the possible sentinel (missing) values.






Graph
=====

.. graphviz:: /images/graph/Conceptual/InstanceVariable.dot