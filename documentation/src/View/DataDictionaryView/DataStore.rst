.. _DataStore:


DataStore
*********
An organized collection of data 

Additional information:
Using its datapoints and their processing instructions, a datastore can host questionnaire and variable datum wiring them together to form processing pipelines in all shapes and forms. Also, in line with a spreadsheet, a datastore can host multiple data structures. So a datastore can contain unit data as well as a pivot table that represents the unit data dimensionally. Likewise, the same datastore may contain both a normalized recordset in which entities are columns and its denormalized counterpart in which the rows are entities. Given processing instructions, one can losslessly be constructed from the other.




Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
characterSet   String                             0..1
dataStoreType  ExternalControlledVocabularyEntry  0..1
name           Name                               0..n
purpose        StructuredString                   0..1
recordCount    Integer                            0..1
type           CollectionType                     0..1
=============  =================================  ===========


characterSet
############
Default character set used in the Data Store.


dataStoreType
#############
The type of DataStore. Could be delimited file, fixed record length file, relational database, etc. Points to an external definition which can be part of a controlled vocabulary maintained by the DDI Alliance.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


recordCount
###########
The number of records in the Data Store.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

==========  ===================  ===========  ================
Name        Type                 Cardinality  allways external
==========  ===================  ===========  ================
contains    DataRecord           0..1           no
realizes    Collection           0..n           yes
references  LogicalRecordLayout  0..1           no
==========  ===================  ===========  ================


contains
########
Data in the form of Data Records contained in the Data Store.




realizes
########
Allows the DataStore to function as a Collection, enabling, for example,  ordering of its components.




references
##########
Can point to one or more descriptions of the structure (type) of the Data Record.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/DataStore.dot