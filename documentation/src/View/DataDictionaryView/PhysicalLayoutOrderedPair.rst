.. _PhysicalLayoutOrderedPair:


PhysicalLayoutOrderedPair
*************************
The pair of Value Mappings in a Physical Layout which are being placed in a sequence.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ============  ===========  ================
Name      Type          Cardinality  allways external
========  ============  ===========  ================
realizes  OrderedPair   0..n           yes
source    ValueMapping  0..n           no
target    ValueMapping  0..n           no
========  ============  ===========  ================


realizes
########
Class of the Collection pattern realized by this class.




source
######
Specialization of source in OrderedPair for Value Mappings in a Physical Layout.




target
######
Specialization of target in OrderedPair for Value Mappings in a Physical Layout.






Graph
=====

.. graphviz:: /images/graph/FormatDescription/PhysicalLayoutOrderedPair.dot