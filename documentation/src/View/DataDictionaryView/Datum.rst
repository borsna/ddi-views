.. _Datum:


Datum
*****
A Datum is the designation of a concept with a notion of equality defined.



Extends
=======
:ref:`Designation`


Relationships
=============

===============  ================  ===========  ================
Name             Type              Cardinality  allways external
===============  ================  ===========  ================
isBoundedBy      InstanceVariable  0..n           no
isConstrainedOf  ValueDomain       0..n           no
===============  ================  ===========  ================


isBoundedBy
###########
A Datum is bounded by an InstanceVariable.




isConstrainedOf
###############
A Datum is drawn from a set of values, either substantive or sentinel






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/Datum.dot