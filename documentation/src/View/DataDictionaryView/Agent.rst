.. _Agent:


Agent
*****
An actor that performs a role in relation to a process. 



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
agentId  AgentId           0..n
image    PrivateImage      0..n
purpose  StructuredString  0..1
=======  ================  ===========


agentId
#######
An identifier within a specified system for specifying an agent


image
#####
References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.

`


Relationships
=============

========  ======  ===========  ================
Name      Type    Cardinality  allways external
========  ======  ===========  ================
realizes  Member  0..n           yes
========  ======  ===========  ================


realizes
########
An Agent can perform the role of a Member in an Agent Registry






Graph
=====

.. graphviz:: /images/graph/Agents/Agent.dot