.. _IdentifierRole:


IdentifierRole
**************
An IdentifierRole  identifies an InstanceVariable as being an identifier within a ViewPoint.




Extends
=======
:ref:`ViewpointRole`


Relationships
=============

======  ================  ===========  ================
Name    Type              Cardinality  allways external
======  ================  ===========  ================
mapsTo  InstanceVariable  0..n           no
======  ================  ===========  ================


mapsTo
######
The InstanceVariables being assigned the role of identifier






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/IdentifierRole.dot