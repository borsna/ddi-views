.. _Population:


Population
**********
Set of specific units (people, entities, objects, events), usually in a given time and geography.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
descriptiveText  StructuredString  0..1
displayLabel     DisplayLabel      0..n
name             Name              0..n
===============  ================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.

`


Relationships
=============

================  ==========  ===========  ================
Name              Type        Cardinality  allways external
================  ==========  ===========  ================
contains          Unit        0..n           no
hasSubpopulation  Population  1..1           no
narrowsUniverse   Universe    0..n           no
realizes          Member      0..n           yes
usesConcept       Concept     0..n           no
================  ==========  ===========  ================


contains
########
Units in the Population 




hasSubpopulation
################
Populations can have sub populations. For example the Sub-Universe Class of Gender for the Universe Population of Canada in 2011 may contain the Universe Canadian Males in 2011 and the Universe Canadian Females in 2011.




narrowsUniverse
###############
Reference to a Universe that the Population narrows




realizes
########
Class can be used in the role of Member within a Collection




usesConcept
###########
Reference to the Concept that is being used






Graph
=====

.. graphviz:: /images/graph/Conceptual/Population.dot