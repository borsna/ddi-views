.. _SegmentByText:


SegmentByText
*************
Defines the location of a segment of text.



Extends
=======
:ref:`PhysicalSegmentLocation`


Properties
==========

========================  ===============  ===========
Name                      Type             Cardinality
========================  ===============  ===========
definedByCharacterOffset  CharacterOffset  0..n
definedByLineParameters   LineParameter    0..n
========================  ===============  ===========


definedByCharacterOffset
########################
Points to parameter pairs using start character (from the beginning of the document) and end character


definedByLineParameters
#######################
Points to parameters using start line and character withing line and end line and character within that line




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByText.dot