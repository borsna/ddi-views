.. _Code:


Code
****
A Designation for a Category.



Extends
=======
:ref:`Designation`


Relationships
=============

=======  ========  ===========  ================
Name     Type      Cardinality  allways external
=======  ========  ===========  ================
denotes  Category  0..n           no
=======  ========  ===========  ================


denotes
#######
A definition for the code. Specialization of denotes for Categories.






Graph
=====

.. graphviz:: /images/graph/Representations/Code.dot