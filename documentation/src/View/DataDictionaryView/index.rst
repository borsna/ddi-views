******************
DataDictionaryView
******************
Purpose:
The Data Dictionary View provides the limited information on a data file structure including: the name of the data file, name of variable, physical location (order) of variable, type of variable (character, numeric, etc.), variable label, variable universe/population, substantive (valid) and sentinel (invalid) values with labels if appropriate, and variable concept. 

Use Cases:
This is a core section of most codebooks as well as often being supplied as a structured document as import to various statistical package set-up files.

Target Audience:
Creators of data files for access and distributors.

Included Classes:
Access, AttributeRole, Category, CategorySet, Code, CodeItem, CodeList, Concept, Coverage DataPoint, DataRecord, DataStore, Datum, FundingInformation, IdentifierRole, InstanceVariable, Level, LogicalRecordLayout, MeasureRole, PhysicalLayoutOrder, PhysicalLayoutOrderPairs, Population, RectangularLayout (physicalLayout), SegmentByText, SentinalConceptualDomain, SentinelValueDomain, StructureDescription, SpatialCoverage, SubstantiveConceptualDomain, SubstantiveValueDomain, TemporalCoverage, TopicalCoverage, Unit, UnitType, Universe, ValueAndConceptDescription, ValueMapping, Viewpoint

Restricted Classes: 
The following classes contain relationships NOT found in this view. (If the instance of the related class is available in another functional view or other DDI store it may be identified with an external reference):
BoundingBox from SpatialCoverage
RepresentedVariable from InstanceVariable
ConceptualVariable from InstanceVariable

General Documentation:
InstanceVariable contains a relationship to a RepresentedVariable and ConceptualVariable. These two classes have not been included in the Data Dictionary View. The specific content of these two classes are found in the InstanceVariable. Reference may be made instances of these two classes held externally to the Data Dictionary View instance. The entry point for this view is generally the StructureDescription which identifies the DataStore (leading to logical and physical descriptions) and the specific layout information (CSV, Rectangular, etc.).

A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access
   Agent
   AttributeRole
   Category
   CategorySet
   Code
   CodeItem
   CodeList
   Concept
   Coverage
   DataPoint
   DataRecord
   DataStore
   Datum
   FundingInformation
   IdentifierRole
   InstanceVariable
   Level
   LogicalRecordLayout
   MeasureRole
   PhysicalLayoutOrder
   PhysicalLayoutOrderedPair
   Population
   RectangularLayout
   SegmentByText
   SentinelConceptualDomain
   SentinelValueDomain
   SpatialCoverage
   SubstantiveConceptualDomain
   SubstantiveValueDomain
   TemporalCoverage
   TopicalCoverage
   Unit
   UnitType
   Universe
   ValueAndConceptDescription
   ValueMapping
   Viewpoint



Graph
=====

.. graphviz:: /images/graph/DataDictionaryView/DataDictionaryView.dot