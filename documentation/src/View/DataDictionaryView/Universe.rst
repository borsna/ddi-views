.. _Universe:


Universe
********
A defined class of people, entities, events, or objects, with no specification of time and geography, contextualizing a Unit Type



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
descriptiveText  StructuredString  0..1
displayLabel     DisplayLabel      0..n
isInclusive      Boolean           0..1
name             Name              0..n
===============  ================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


isInclusive
###########
The default value is "true". The description statement of a universe is generally stated in inclusive terms such as "All persons with university degree". Occasionally a universe is defined by what it excludes, i.e., "All persons except those with university degree". In this case the value would be changed to "false".


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

===============  ========  ===========  ================
Name             Type      Cardinality  allways external
===============  ========  ===========  ================
hasSubuniverse   Universe  1..1           no
narrowsUnitType  UnitType  0..n           no
realizes         Member    0..n           yes
usesConcept      Concept   0..n           no
===============  ========  ===========  ================


hasSubuniverse
##############
Universes can have sub universes. A sub-universe class provides a definition to the universes contained within it. For example the Sub-Universe Class of Gender for the Universe Population may contain the Universe Males and the Universe Females




narrowsUnitType
###############
Reference to the Unit Type that the Universe definition narrows.




realizes
########
Class can be used in the role of Member within a Collection




usesConcept
###########
Reference to the Concept that is being used






Graph
=====

.. graphviz:: /images/graph/Conceptual/Universe.dot