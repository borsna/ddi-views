.. _UnitType:


UnitType
********
A Unit Type is a class of objects of interest.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
descriptiveText  StructuredString  0..1
displayLabel     DisplayLabel      0..n
name             Name              0..1
===============  ================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

===========  =======  ===========  ================
Name         Type     Cardinality  allways external
===========  =======  ===========  ================
realizes     Member   0..n           yes
usesConcept  Concept  0..n           no
===========  =======  ===========  ================


realizes
########

Class can be used in the role of Member within a Collection




usesConcept
###########
Reference to the Concept that is being used






Graph
=====

.. graphviz:: /images/graph/Conceptual/UnitType.dot