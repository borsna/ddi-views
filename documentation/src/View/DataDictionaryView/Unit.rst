.. _Unit:


Unit
****
The object of interest in a process step related to the collection or use of observational data.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ============  ===========
Name          Type          Cardinality
============  ============  ===========
displayLabel  DisplayLabel  0..n
name          Name          0..n
============  ============  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

===========  ========  ===========  ================
Name         Type      Cardinality  allways external
===========  ========  ===========  ================
hasUnitType  UnitType  0..n           no
===========  ========  ===========  ================


hasUnitType
###########
The UnitType of the Unit






Graph
=====

.. graphviz:: /images/graph/Conceptual/Unit.dot