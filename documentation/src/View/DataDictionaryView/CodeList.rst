.. _CodeList:


CodeList
********
A list of Codes and associated Categories. May be flat or hierarchical. 



Extends
=======
:ref:`NodeSet`


Relationships
=============

==========  ===========  ===========  ================
Name        Type         Cardinality  allways external
==========  ===========  ===========  ================
contains    CodeItem     1..n           no
references  CategorySet  1..n           no
represents  ValueDomain  1..n           no
==========  ===========  ===========  ================


contains
########
Specialization of contains in NodeSet for CodeItems.





references
##########
CategorySet associated with the CodeList.




represents
##########
Enumerated Value Domain represented by the CodeList.






Graph
=====

.. graphviz:: /images/graph/Representations/CodeList.dot