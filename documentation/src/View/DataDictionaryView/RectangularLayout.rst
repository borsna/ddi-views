.. _RectangularLayout:


RectangularLayout
*****************
RectangularLayout supports the description of unit-record ("wide") data sets, where each row in the data set provides a group of values for variables all relating to a single unit. The columns will contain data relating to the values for a single variable. 



Extends
=======
:ref:`PhysicalLayout


Graph
=====

.. graphviz:: /images/graph/FormatDescription/RectangularLayout.dot