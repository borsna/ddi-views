.. _AttributeRole:


AttributeRole
*************
A MeasureRole  identifies an InstanceVariable as being an attribute within a ViewPoint.





Extends
=======
:ref:`ViewpointRole`


Relationships
=============

======  ================  ===========  ================
Name    Type              Cardinality  allways external
======  ================  ===========  ================
mapsTo  InstanceVariable  0..n           no
======  ================  ===========  ================


mapsTo
######
The InstanceVariables being assigned the role of attribute







Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/AttributeRole.dot