.. _LogicalRecordLayout:


LogicalRecordLayout
*******************
Collection of Instance Variables that function as a record type describing the structure of individual records. 




Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

============  ===================  ===========  ================
Name          Type                 Cardinality  allways external
============  ===================  ===========  ================
contains      InstanceVariable     0..1           no
isViewedFrom  Viewpoint            0..n           no
nests         LogicalRecordLayout  0..n           no
realizes      Collection           0..n           yes
============  ===================  ===========  ================


contains
########
Instance Variable(s) defining this record layout.




isViewedFrom
############
Viewpoint associated with the record layout




nests
#####
LogicalRecordLayout nested in a LogicalRecordLayout




realizes
########
Class of the Collection pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/LogicalRecordLayout.dot