.. _MeasureRole:


MeasureRole
***********
A MeasureRole  identifies an InstanceVariable as being a measure within a ViewPoint.




Extends
=======
:ref:`ViewpointRole`


Relationships
=============

======  ================  ===========  ================
Name    Type              Cardinality  allways external
======  ================  ===========  ================
mapsTo  InstanceVariable  0..n           no
======  ================  ===========  ================


mapsTo
######
The InstanceVariables being assigned the role of measure






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/MeasureRole.dot