.. _IfThenElse:


IfThenElse
**********
IfThenElse describes an if-then-else decision type of control construct. If the stated condition is met, then the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered, otherwise the Workflow Sequence that is triggered is the one associated via elseContains.




Extends
=======
:ref:`ConditionalControlConstruct`


Relationships
=============

============  ================  ===========  ================
Name          Type              Cardinality  allways external
============  ================  ===========  ================
elseContains  WorkflowSequence  0..n           no
hasElseIf     ElseIf            1..1           no
============  ================  ===========  ================


elseContains
############
Optional sequence of Process Steps that are triggered when the condition is false.




hasElseIf
#########
Set of ElseIf that are evaluated if the condition is false. 






Graph
=====

.. graphviz:: /images/graph/Workflows/IfThenElse.dot