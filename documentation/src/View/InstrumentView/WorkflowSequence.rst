.. _WorkflowSequence:


WorkflowSequence
****************
Sequencing of Workflow Steps that can be defined either by Temporal or Order Relations. 

The sequence can be typed to support local processing or classification flags and alternate sequencing instructions (such as randomize for each respondent).





Extends
=======
:ref:`ControlConstruct`


Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
constructSequence  SpecificSequence                   0..1
type               CollectionType                     0..1
typeOfSequence     ExternalControlledVocabularyEntry  0..n
=================  =================================  ===========


constructSequence
#################
Describes alternate ordering for different cases using the SpecificSequence structure. If you set the sequence to anything other than order of appearance the only allowable children are QuestionConstruct or Sequence. Contents must be randomizable.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.


typeOfSequence
##############
Provides the ability to "type" a sequence for classification or processing purposes. Supports the use of an external controlled vocabulary.

`


Relationships
=============

========  ==========  ===========  ================
Name      Type        Cardinality  allways external
========  ==========  ===========  ================
realizes  Collection  0..n           yes
========  ==========  ===========  ================


realizes
########
Class in the Collections pattern realized by this class. A Workflow Sequence can be viewed as a Collection of Workflow Steps.






Graph
=====

.. graphviz:: /images/graph/Workflows/WorkflowSequence.dot