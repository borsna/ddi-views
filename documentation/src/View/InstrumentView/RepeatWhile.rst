.. _RepeatWhile:


RepeatWhile
***********
Iterative control structure to be repeated while a specified condition is met. Before each iteration the condition is tested. If the condition is met, the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered. When the condition is not met, control passes back to the containing Workflow Step.



Extends
=======
:ref:`ConditionalControlConstruct


Graph
=====

.. graphviz:: /images/graph/Workflows/RepeatWhile.dot