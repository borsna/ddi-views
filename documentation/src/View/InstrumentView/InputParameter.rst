.. _InputParameter:


InputParameter
**************
Generic container for an input instance to a Process Step. 



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

====  =================================  ===========
Name  Type                               Cardinality
====  =================================  ===========
type  ExternalControlledVocabularyEntry  0..n
====  =================================  ===========


type
####
Type of object the parameter accepts (DDI object, Datatype, etc.). If not present the parameter is untyped.

`


Relationships
=============

========  ===========  ===========  ================
Name      Type         Cardinality  allways external
========  ===========  ===========  ================
realizes  InputOutput  0..n           yes
========  ===========  ===========  ================


realizes
########
Class in the Process Pattern to be realized by Input Parameter.






Graph
=====

.. graphviz:: /images/graph/Workflows/InputParameter.dot