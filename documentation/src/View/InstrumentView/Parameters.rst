.. _Parameters:


Parameters
**********
Collection of Input and Output Parameters of Workflow Steps.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

====  ==============  ===========
Name  Type            Cardinality
====  ==============  ===========
type  CollectionType  0..1
====  ==============  ===========


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========  ===============  ===========  ================
Name      Type             Cardinality  allways external
========  ===============  ===========  ================
input     InputParameter   0..n           no
output    OutputParameter  0..n           no
realizes  Interface        0..n           yes
========  ===============  ===========  ================


input
#####
Realization of contains in Interface for Input Parameters.




output
######
Realization of contains in Interface for Output Parameters.




realizes
########
Class in the Process Pattern realized by Parameters.






Graph
=====

.. graphviz:: /images/graph/Workflows/Parameters.dot