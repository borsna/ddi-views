.. _PrecedesIntervalRelation:


PrecedesIntervalRelation
************************
Representation of the precedes relation in Allen's interval algebra.

We say that an interval A precedes another interval B if and only if A finishes before B begins.

More precisely, A.end < B.start.

Instead of saying that A precedes B we can also say that B is preceded by A (converse).




Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Temporal-Precedes


symmetry
########
Fixed to Anti-Symmetric


transitivity
############
Fixed to Transitive


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  ===================  ===========  ================
Name        Type                 Cardinality  allways external
==========  ===================  ===========  ================
contains    OrderedIntervalPair  0..n           no
realizes    StrictOrderRelation  0..n           yes
structures  WorkflowSequence     0..n           no
==========  ===================  ===========  ================


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class.





structures
##########
Sequence to which the Temporal Interval Relation is applied.






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/PrecedesIntervalRelation.dot