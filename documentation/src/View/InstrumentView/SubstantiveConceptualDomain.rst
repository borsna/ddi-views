.. _SubstantiveConceptualDomain:


SubstantiveConceptualDomain
***************************
Set of valid Concepts. The Concepts can be described by either enumeration or by an expression.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ============  ===========
Name          Type          Cardinality
============  ============  ===========
displayLabel  DisplayLabel  0..n
============  ============  ===========


displayLabel
############
A display label for the Conceptual Domain. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.

`


Relationships
=============

==========================  ==========================  ===========  ================
Name                        Type                        Cardinality  allways external
==========================  ==========================  ===========  ================
describedConceptualDomain   ValueAndConceptDescription  0..n           no
enumeratedConceptualDomain  CategorySet                 0..n           no
==========================  ==========================  ===========  ================


describedConceptualDomain
#########################
A description of the concepts in the domain. A numeric domain might use a logical expression to be machine actionable a text domain might use a regular expression to describe strings that describe the concepts.




enumeratedConceptualDomain
##########################
The CategorySet containing the concepts in the domain.






Graph
=====

.. graphviz:: /images/graph/Conceptual/SubstantiveConceptualDomain.dot