.. _OrderedIntervalPair:


OrderedIntervalPair
*******************
Ordered pair of Process Steps in a Temporal Interval Relation.




Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ============  ===========  ================
Name      Type          Cardinality  allways external
========  ============  ===========  ================
realizes  OrderedPair   0..n           yes
source    WorkflowStep  0..n           no
target    WorkflowStep  0..n           no
========  ============  ===========  ================


realizes
########
Class of the Collection pattern realized by this class.





source
######
First Process Step in the Ordered Pair. Process Step that either starts or ends before the Process Step in the target.




target
######
Second Process Step in the Ordered Pair. Process Step that either starts or ends after the Process Step in the source.






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/OrderedIntervalPair.dot