**************
InstrumentView
**************
Purpose:
The purpose of the Instrument Functional View is to describe the data collection instrument and procedures involved in fielding a simple survey questionnaire. It allows only basic Domains (Text, Numeric, and CodeList), but supports the full questionnaire flow (Sequence, IfThenElse, ElseIf, Loop, RepeatUntil, and RepeatWhile). The specific flow of the Instrument is described using PrecedesIntervalRelation. Support has also been provided for binding the output of one capture to the input of another, for example, the number of persons in the household output used as the condition of a Loop. 

Target Audience:
Creator's of simple questionnaires.

Included Classes: 
Binding , Category, CategorySet, CodeItem, CodeList, Concept, ConceptualInstrument , ConceptualVariable, ElseIf , ExternalAid , ExternalMaterial, IfThenElse , ImplementedInstrument, InputParameter, InstanceQuestion , Instruction , InstrumentCode , Level, Loop , OrderedIntervalPair, OutputParameter, Parameters , PrededesIntervalRelation, RepeatUntil , RepeatWhile , RepresentedQuestion, RepresentedVariable, ResponseDomain , SentinelConceptualDomain, SentinelValueDomain, Statement , SubstantiveConceptualDomain, SubstantiveValueDomain, UnitType, Universe, ValueAndConceptDescription, WorkflowSequence 

Restricted Classes:
The following classes have been restricted. 
The relationships with target class "WorkflowService" must be external references, they cannot be included in an implementation of the view. These were restricted as no specific type of WorkflowService relating to an Instrument has been created. 
Conceptualinstrument can only include Design, Algorithm, Result, and Precondition only by use of an external reference.

General Documentation
The general top-level entry points are ImplementedInstrument or ConceptualInstrument for content and PrecedesIntervalRelation for the ordering of Sequences and other control construct types.


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Binding
   Category
   CategorySet
   CodeItem
   CodeList
   Concept
   ConceptualInstrument
   ConceptualVariable
   ElseIf
   ExternalAid
   ExternalMaterial
   IfThenElse
   ImplementedInstrument
   InputParameter
   InstanceQuestion
   Instruction
   InstrumentCode
   Level
   Loop
   OrderedIntervalPair
   OutputParameter
   Parameters
   PrecedesIntervalRelation
   RepeatUntil
   RepeatWhile
   RepresentedQuestion
   RepresentedVariable
   ResponseDomain
   SentinelConceptualDomain
   SentinelValueDomain
   Statement
   SubstantiveConceptualDomain
   SubstantiveValueDomain
   UnitType
   Universe
   ValueAndConceptDescription
   WorkflowSequence



Graph
=====

.. graphviz:: /images/graph/InstrumentView/InstrumentView.dot