.. _ConceptualVariable:


ConceptualVariable
******************
The use of a Concept as a characteristic of a Universe intended to be measured [GSIM 1.1]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
descriptiveText  StructuredString  0..1
displayLabel     DisplayLabel      0..n
name             Name              0..n
===============  ================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

============================  ===========================  ===========  ================
Name                          Type                         Cardinality  allways external
============================  ===========================  ===========  ================
realizes                      Member                       0..n           yes
takesSentinelConceptsFrom     SentinelConceptualDomain     0..n           no
takesSubstantiveConceptsFrom  SubstantiveConceptualDomain  1..n           no
usesConcept                   Concept                      0..n           no
usesUnitType                  UnitType                     0..n           no
============================  ===========================  ===========  ================


realizes
########
Class can play the role of Member within a Collection




takesSentinelConceptsFrom
#########################
Identifies the ConceptualDomain containing the set of sentinel concepts used to describe the  ConceptualVariable.




takesSubstantiveConceptsFrom
############################
Identifies the ConceptualDomain containing the set of substantive concepts used to describe the  ConceptualVariable.




usesConcept
###########
Reference to a Concept that is being used




usesUnitType
############
Identifies the UnitType associated with the ConceptualVariable






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptualVariable.dot