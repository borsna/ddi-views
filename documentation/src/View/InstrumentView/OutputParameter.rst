.. _OutputParameter:


OutputParameter
***************
Generic container for an output instance of a Process Step. 




Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

====  =================================  ===========
Name  Type                               Cardinality
====  =================================  ===========
type  ExternalControlledVocabularyEntry  0..n
====  =================================  ===========


type
####
Type of object the parameter accepts (DDI object, Datatype, etc.). If not present the parameter is untyped.

`


Relationships
=============

========  ===========  ===========  ================
Name      Type         Cardinality  allways external
========  ===========  ===========  ================
realizes  InputOutput  0..n           yes
========  ===========  ===========  ================


realizes
########
Class of the Process Pattern to be realized by Output Parameter.






Graph
=====

.. graphviz:: /images/graph/Workflows/OutputParameter.dot