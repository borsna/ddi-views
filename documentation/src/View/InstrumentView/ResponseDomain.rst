.. _ResponseDomain:


ResponseDomain
**************
The possible list of values that are allowed by a Capture.



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

=============================  ======================  ===========  ================
Name                           Type                    Cardinality  allways external
=============================  ======================  ===========  ================
takesSentinelResponsesFrom     SentinelValueDomain     0..n           no
takesSubstantiveResponsesFrom  SubstantiveValueDomain  0..n           no
=============================  ======================  ===========  ================


takesSentinelResponsesFrom
##########################
The sentinel values (e.g. missing codes) are drawn from this SubstantiveValueDomain




takesSubstantiveResponsesFrom
#############################
The values of interest are drawn from these SubstantiveValueDomains






Graph
=====

.. graphviz:: /images/graph/DataCapture/ResponseDomain.dot