.. _RepresentedVariable:


RepresentedVariable
*******************
A combination of a characteristic of a universe to be measured and how that measure will be represented.




Extends
=======
:ref:`ConceptualVariable`


Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
hasIntendedDataType  ExternalControlledVocabularyEntry  0..1
unitOfMeasurement    String                             0..1
===================  =================================  ===========


hasIntendedDataType
###################
The data type intended to be used by this variable. Supports the optional use of an external controlled vocabulary.


unitOfMeasurement
#################
The unit in which the data values are measured (kg, pound, euro).

`


Relationships
=============

==========================  ======================  ===========  ================
Name                        Type                    Cardinality  allways external
==========================  ======================  ===========  ================
basedOnConceptualVariable   ConceptualVariable      0..n           no
measures                    Universe                0..n           no
takesSubstantiveValuesFrom  SubstantiveValueDomain  0..n           no
==========================  ======================  ===========  ================


basedOnConceptualVariable
#########################
The ConceptualVariable that can be shared by a set of multiple RepresentedVariables. Indicates comparability in  ConceptualDomain and UnitType. 




measures
########
The defined class of people, entities, events, or objects to be measured.




takesSubstantiveValuesFrom
##########################
The substantive representation (SubstantiveValueDomain) of the variable. This is equivalent to the relationship "Measures" in GSIM although GSIM makes no distinction between substantive and sentinel values






Graph
=====

.. graphviz:: /images/graph/Conceptual/RepresentedVariable.dot