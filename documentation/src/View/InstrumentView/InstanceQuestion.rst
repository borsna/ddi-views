.. _InstanceQuestion:


InstanceQuestion
****************
An instance question is an instantiation of a represented question,to be used as an Act in the process steps that define a survey questionnaire.



Extends
=======
:ref:`InstrumentComponent`


Properties
==========

====  ====  ===========
Name  Type  Cardinality
====  ====  ===========
name  Name  0..n
====  ====  ===========


name
####
The name of a question as used in an Instrument. Redefined to provide a more useful description. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.

`


Relationships
=============

============  ===================  ===========  ================
Name          Type                 Cardinality  allways external
============  ===================  ===========  ================
instantiates  RepresentedQuestion  0..n           no
============  ===================  ===========  ================


instantiates
############
The question being used.






Graph
=====

.. graphviz:: /images/graph/DataCapture/InstanceQuestion.dot