.. _InstrumentCode:


InstrumentCode
**************



Extends
=======
:ref:`InstrumentComponent`


Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
commandCode    CommandCode                        0..1
purposeOfCode  ExternalControlledVocabularyEntry  0..1
=============  =================================  ===========


commandCode
###########
Describes the code used to execute the command using the options of inline textual description, inline code, and/or an external file.


purposeOfCode
#############
The purpose of the code (e.g., quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument)




Graph
=====

.. graphviz:: /images/graph/DataCapture/InstrumentCode.dot