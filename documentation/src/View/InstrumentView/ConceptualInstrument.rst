.. _ConceptualInstrument:


ConceptualInstrument
********************
Design plan for creating a data capture tool. 



Extends
=======
:ref:`Workflow`


Properties
==========

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
description    StructuredString  0..1
displayLabel   DisplayLabel      0..n
hasAnnotation  Annotation        0..1
name           Name              0..n
usage          StructuredString  0..1
=============  ================  ===========


description
###########
A description of the purpose or use of the Member. May be expressed in multiple languages and supports the use of structured content. 


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text. 


hasAnnotation
#############
Allows for complete annotation of the Conceptual Instrument


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

=========  =======  ===========  ================
Name       Type     Cardinality  allways external
=========  =======  ===========  ================
organizes  Capture  0..1           no
=========  =======  ===========  ================


organizes
#########
Refers to the RepresentedMeasures and RepresentedQuestions used by the ConceptualInstrument.






Graph
=====

.. graphviz:: /images/graph/DataCapture/ConceptualInstrument.dot