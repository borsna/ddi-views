.. _CollectionItem:


CollectionItem
**************
A collection item is designed to provide a bibliographic, coverage and related description of an item in a collection.



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

===============  ==================  ===========  ================
Name             Type                Cardinality  allways external
===============  ==================  ===========  ================
belongsToSeries  SeriesStatement     0..n           no
describesAccess  Access              0..n           no
hasCoverage      Coverage            0..n           no
hasFunding       FundingInformation  0..n           no
realizes         Member              0..n           yes
===============  ==================  ===========  ================


belongsToSeries
###############
Collection item belongs to this series




describesAccess
###############
Information on how to access the collection item




hasCoverage
###########
Describes the temporal, topical, and spatial coverof the colleciton item




hasFunding
##########
Information concerning the funding source for the colleciton item




realizes
########
Collection Item can act as the member of a collection






Graph
=====

.. graphviz:: /images/graph/Discovery/CollectionItem.dot