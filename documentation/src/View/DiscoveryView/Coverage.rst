.. _Coverage:


Coverage
********
Coverage information for an annotated object. Includes coverage information for temporal, topical, and spatial coverage. 



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
A generic description including temporal, topical, and spatial coverage that is the equivalent of dc:coverage (the refinement base of dcterms:spatial and dcterms:temporal. Use specific coverage content for detailed information. Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.

`


Relationships
=============

===================  ================  ===========  ================
Name                 Type              Cardinality  allways external
===================  ================  ===========  ================
hasSpatialCoverage   SpatialCoverage   0..n           no
hasTemporalCoverage  TemporalCoverage  0..n           no
hasTopicalCoverage   TopicalCoverage   0..n           no
===================  ================  ===========  ================


hasSpatialCoverage
##################
Description of the spatial (geographic) coverage of the contents of the annotated object.




hasTemporalCoverage
###################
The dates and time periods described by the contents of the annotated object.




hasTopicalCoverage
##################
The topics covered by the contents of the annotated object. These may be expressed by subject classification systems and structured or unstructured keywords.






Graph
=====

.. graphviz:: /images/graph/Discovery/Coverage.dot