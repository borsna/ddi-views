*************
DiscoveryView
*************
Purpose:
The Discovery View provides basic information on Annotation, Coverage, and Access. The current view is limited to this basic set of objects and is provided to test out these structures. The Discovery View will modify as more content is available. The future intent is to make this view closer to DISCO as development continues. Currently allows the creation of catalogs of collection items.

Use Cases:
Archive collection of bibliographic records and related information for items in the collection.

Target Audience:
Distributors

Included Classes:
Access, BoundingBox, CatalogOfItems, CollectionItem, Coverage, FundingInformation, SeriesStatement, SpatialCoverage, TemporalCoverage, TopicalCoverage

General Documentation:
Point of entry is CatalogOfItems.

A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access
   BoundingBox
   CatalogOfItems
   CollectionItem
   Coverage
   FundingInformation
   SeriesStatement
   SpatialCoverage
   TemporalCoverage
   TopicalCoverage



Graph
=====

.. graphviz:: /images/graph/DiscoveryView/DiscoveryView.dot