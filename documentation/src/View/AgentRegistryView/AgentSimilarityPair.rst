.. _AgentSimilarityPair:


AgentSimilarityPair
*******************
Describes simple pair of similar agents of any type (i.e. Organization, Individual, Machine, etc.). Uses the pattern for an UnorderedPair.



Extends
=======
:ref:`Identifiable`


Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
effectiveDates  DateRange  0..1
==============  =========  ===========


effectiveDates
##############
The effective dates of the relation. A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.

`


Relationships
=============

========  =============  ===========  ================
Name      Type           Cardinality  allways external
========  =============  ===========  ================
maps      Agent          0..n           no
realizes  UnorderedPair  0..n           yes
========  =============  ===========  ================


maps
####
Two Agents that are similar within the context of the type of relationship.




realizes
########
Reflects the pattern of an UnorderedPair






Graph
=====

.. graphviz:: /images/graph/Agents/AgentSimilarityPair.dot