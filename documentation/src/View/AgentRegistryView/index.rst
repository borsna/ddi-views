*****************
AgentRegistryView
*****************
Purpose:
The Agent Registry View supports the creation of a listing of Agents (Organization, Individual, Machine) for the purpose of maintenance and reuse. The listing of Agents can be organized in pair-wise descriptions within Agent Relationships. These may be typed and provided with effective date ranges either as a set of relationships of a specified type or as individual pairs.

Use Cases:
Maintaining a listing of all Organizations and Individuals related to a project or statistical activities, i.e. Individuals and Organizations related to the IPUMS projects at the Minnesota Population Center or the Departments and Individuals involved in the production of the Australian Census within the Australian Bureau of Statistics.

Target Audience:
Organizations working with a number of Individuals and Organizations whose descriptive information and interrelationships change over time.

Included Classes: Agent Registry, Agent Listing, Agent Relationships, Organization, Individual, Machine, Agent Similarity, Agent Similarity Pair, Agent Hierarchy, Agent Hierarchy Pair, Access, Funding Information, External Material, Coverage, Bounding Box. 

Restricted Classes:
All classes are included in their entirety, there are no restrictions.

General Documentation:
The primary class is the Agent Registry which contains Agent Listings and Agent Relationships. Agents may be of any type supported by DDI at the time of this release. Agent Relationships are described by Agent Similarity specified with Agent Similarity Pair (unordered pair of agents) or Agent Hierarchy specified with Agent Hierarchy Pair (ordered pair reflecting a hierarchical relationship between the agents). The Functional View includes the standard document description and Access, Coverage,  Funding Information, External Materials, and Bounding Box classes have been added to support a full description of the instance to support discovery and access.

A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access
   AgentHierarchy
   AgentHierarchyPair
   AgentListing
   AgentRegistry
   AgentRelationships
   AgentSimilarity
   AgentSimilarityPair
   BoundingBox
   Coverage
   ExternalMaterial
   FundingInformation
   Individual
   Machine
   Organization



Graph
=====

.. graphviz:: /images/graph/AgentRegistryView/AgentRegistryView.dot