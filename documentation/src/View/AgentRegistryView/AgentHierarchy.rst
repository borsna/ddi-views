.. _AgentHierarchy:


AgentHierarchy
**************
A set of relationships between pairs of Agents that is hierarchical (Parent/Child) in nature. Uses the pattern for Immediate Precedence Relation where the relation over members in the Agent Registry is characterized by the following: Reflexivity=Anti_Reflexive; Symmetry=Anti_Symmedtric; Transitivity=Anti_Transitive. It must contain like items (Agents).



Extends
=======
:ref:`AgentBinary`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti_Reflexive


semantics
#########
Controlled vocabulary for the ImmediatePrecedenceRelation semantics. It should contain, at least, the following: Parent_Of, Child_Of, Next, Previous, Instance_Of, Temporal_Meets. 


symmetry
########
Fixed to Anti_Symmetric


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


transitivity
############
Fixed to Anti_Transitive

`


Relationships
=============

==========  ===========================  ===========  ================
Name        Type                         Cardinality  allways external
==========  ===========================  ===========  ================
contains    AgentHierarchyPair           0..n           no
realizes    ImmediatePrecedenceRelation  0..n           yes
structures  AgentListing                 0..n           no
==========  ===========================  ===========  ================


contains
########
Pair of Agents whose relationship is hierarchical in nature.




realizes
########
Uses the pattern of Immediate Precedence Relation




structures
##########
The Agent Listing or Listings whose Agents are grouped by pairs to support a variety of structures






Graph
=====

.. graphviz:: /images/graph/Agents/AgentHierarchy.dot