.. _AgentRelationships:


AgentRelationships
******************
A collection of relationships between pairs of Agents. 

`


Properties
==========

==========  ================  ===========
Name        Type              Cardinality
==========  ================  ===========
maintainer  AgentAssociation  0..1
name        Name              0..n
purpose     StructuredString  0..1
type        CollectionType    0..1
==========  ================  ===========


maintainer
##########
The agent who maintains the relationship information


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of the collection of Agent Relationships. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========  ===========  ===========  ================
Name      Type         Cardinality  allways external
========  ===========  ===========  ================
contains  AgentBinary  0..n           no
realizes  Collection   0..n           yes
========  ===========  ===========  ================


contains
########
Agent Hierarchy or Agent Similarity sets described in the Agent Relationship




realizes
########
Uses the pattern for a Collection






Graph
=====

.. graphviz:: /images/graph/Agents/AgentRelationships.dot