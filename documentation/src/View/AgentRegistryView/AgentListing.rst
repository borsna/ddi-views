.. _AgentListing:


AgentListing
************
A listing of Agents of any type



Extends
=======
:ref:`Identifiable`


Properties
==========

==========  ================  ===========
Name        Type              Cardinality
==========  ================  ===========
maintainer  AgentAssociation  0..1
name        Name              0..n
purpose     StructuredString  0..1
type        CollectionType    0..1
==========  ================  ===========


maintainer
##########
The agent responsible for maintaining the Agent Listing


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of the Agent Listing. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========  ==========  ===========  ================
Name      Type        Cardinality  allways external
========  ==========  ===========  ================
contains  Agent       0..n           no
realizes  Collection  0..n           yes
========  ==========  ===========  ================


contains
########
The Agents contained in the Listing




realizes
########
Uses the pattern Collection






Graph
=====

.. graphviz:: /images/graph/Agents/AgentListing.dot