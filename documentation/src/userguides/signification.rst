Using the Signification pattern
================================

A Sign links a Signified with a Signifier that denotes it. A Signifier is a concept whose extension consists of tokens (perceivable objects).

Signifier, Sign and Signified become part of the Signification Pattern.

**Figure 1. The Signification Pattern**

.. |figure1| image:: ../images/ddi4_signification01.png

|figure1|

A Designation is simply a Sign where the Signified is a Concept. Therefore Designation and Sign realize Sign and Signified, respectively. Signifier becomes the Data Type of the representation property of Sign.

The reason for making Signifier, Sign and Signified into a pattern to be realized as opposed to classes to be extended is that Concepts are not always Signifieds, which is what a specialization would imply. In fact, a Concept is a Signified only if there is a Designation that denotes it. The realization means that the Concept is going to behave like a Signified only in the context described.

Codes enter into the picture as Designations. A Code then is a type of Designation that has Non-Linguistic Signifiers and where the Signified is a Category (Concept).

**Figure 2. Code Item**

.. |figure2| image:: ../images/ddi4_signification02.png

|figure2|

A Node takes meaning from a single Category and has an optional set of Designations. In the case of the Code Item subtype, at least one Designation is required, i.e. a Code. It�s important to note that a Code Item can have only one Category, thus if there are multiple Codes associated with a Code Item, all of them will have to denote the same Category (see constraint in diagram).
