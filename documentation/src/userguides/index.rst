************
User Guides
************

DDI-Views introduces a new feature for those developing the model. There are �pattern� packages that consist entirely of abstract classes that can be �realized� by other classes. 
The pattern classes do not appear in the bindings (e.g. XML or RDF) that users of DDI will see. The realizing classes must mirror all of the properties and relationships of the pattern class 
although the properties of relationships may be renamed to make the semantics clearer. These pages describe several of the pattern classes and their use.

.. toctree::
   :maxdepth: 2

   collectionpattern.rst
   processpattern.rst
   variablecascade.rst
   signification.rst
   binaryrelations.rst
