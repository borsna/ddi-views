A Guide to Binary Relations
============================

.. glossary::

    Object 
      Anything perceivable or conceivable (from ISO 1087-1)     

    Perceivable 
      Selectable through the senses or an instrument e.g. An apple, A thunder clap, A voltage
  
    Conceivable 
      Abstract or imagined e.g. A law, A standard, A unicorn, A hippogriff

    Class 
      Collection of objects sharing specified characteristics and behavior

    Tuple
      Ordered set of objects, one from each of a given set of classes

    Relationship
      Collection of tuples

    Relation
      Meaning of a relationship
      *Note* We define relation and relationship as above because the same set of tuples may have more than one meaning attached. 
      For example, take the set of two married cousins named John and Mary – call the set P. 
      The elements, or tuples, of PxP (the cross product, or all possible pairs of elements of P) are <John, John>, <Mary, Mary>, <John, Mary>, and <Mary, John>. 
      Now, consider two relations on this set of people: cousins and married. Both relations are anti-reflexive, symmetric, and intransitive (see below). 
      So, we only need to consider the tuple <Mary, John>. This tuple makes up the relationship corresponding to both relations.

    Binary relation
      A binary relation is one that takes 2 arguments. So, more formally, a binary relation applied to (more commonly “on”) some set A is a subset of AxA. 
      For example, if A = {1, 2, 3}, then the binary relation “=” (equality) on A is the following subset of AxA: {<1, 1>, <2, 2>, <3, 3>}. 
      For instance, the pair <1, 2> is not in the subset defined by “=”.

    Total relation
      A total relation on a set A is the case for which all a, b in A, then either aRb, bRa, or both. 
      This means every pair of elements are comparable. The “less than”, or “<”, relation on the set of integers is total.

    Reflexive relation
      A reflexive relation on a set A is the case for which all a in A, aRa. For example, “equality”, or “=”, on the set of integers is reflexive. For example, 5 = 5.

    Anti-reflexive
      An anti-reflexive, or irreflexive, relation on a set A is the case for which all a in A, then it is never the case that aRa. 
      The strict “less than”, or “<”, relation on the set of integers is anti-reflexive. For example, 17 is not less than 17.

    Symmetric relation
      A symmetric relation on a set A is the case for which all a, b in A, then if aRb implies bRa. The “equality” relation on the set of integers is symmetric.

    Anti-symmetric
      An anti-symmetric relation on a set A is the case for which all a, b in A, then aRb and bRa implies a=b. 
      An anti-symmetric relation is one where symmetry never holds. The strict “less than”, or “<”, relation on the set of integers is anti-symmetric. 
      For example, since 4 < 5, then it is not the case that 5 < 4.

    Transitive relation
      A transitive relation on a set A is the case for which all a, b, and c in A, then if aRb and bRc, then aRc. The strict “less than”, or “<”, relation on the set of 
      integers is transitive. For example, since 42 < 69 and 69 < 1024, then 42 < 1024.

    Anti-transitive
      An anti-transitive relation is one which for all a, b, and c in A, if aRb and bRc, then not aRc . Let A = {rock, paper, scissors}. We define a relation where rock beats scissors, scissors beats paper, and paper beats rock. But, given rBs and sBp, it is not the case that rBp. It is easy to see that no other transitivity case is true either. This relation is also anti-reflexive and anti-symmetric.

    Intransitive relation
      An intransitive relation is one which for some but not all a, b, c in A, if aRb and bRc, then aRc. Let R be “distrusts” applied to countries. 
      The US distrusts Iran, and Iran distrusts the UK, yet the US and the UK do not distrust each other. The US distrusts Iran, and Iran distrusts Iraq. 
      Yet, the US distrusts Iraq also.

    Partial order
      A partial order is reflexive, anti-symmetric, and transitive. This applies to the usual “less than or equal” relation on numbers. 
      The strict “less than” relation is anti-reflexive.
      A partial order that is total is a total order, linear order, or chain.
      A strict partial order is anti-reflexive.

    Graph
      A graph is a non-empty set of nodes and a set of arcs linking pairs of nodes.
      It is often useful to think of the nodes of a graph as representing some objects, which are the elements of some set, and the arcs of the graph as the 
      relationships between those elements. We call this a represented graph.

    Path
      A path is a sequence of nodes and arcs connecting two distinct nodes, where no nodes in the sequence are repeated. 
      More precisely, let N.0 and N.n be two distinct nodes in a graph, then a path from N.0 to N.n is a sequence {N.0, a.1, N.1, a.2, …, N.n} of nodes and arcs, 
      where nodes N.i ≠ N.j for all i and j, and a.i is an arc connecting N.i-1 to N.i.

    Connected graph
      A connected graph is one for which there is a path between any pair of distinct nodes.
      A graph is acyclic if for nodes A, B, C and paths P and Q from nodes A to B and from nodes A to C, respectively, then there does not exist a path from B to C.

    Tree
      A tree is a connected, acyclic graph with one node identified as the root.
      A hierarchy is a represented tree.
      A relation is hierarchical, a hierarchical relation, if when it is applied to some set of objects it can be represented in a tree.

Explanation of Relations
------------------------

Less Than
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Transitive
Meaning: Relation between numbers comparing cardinalities
Example – 7 is less than 12 (or 7 < 12)

Less Than or Equal
  * Hierarchical
  * Reflexive
  * Anti-symmetric
  * Transitive
Meaning: Relation between numbers comparing cardinalities
Example – 7 is less than or equal to 12 (or 7 ≤ 12), 10 is less than or equal to 10 (or 10 ≤ 10)

Generic
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Transitive
Meaning: Sub-type; Specialization; 
Relation between classes whereby the characteristics and behaviors of one (generic) class are a proper subset of those for the other (specialized) class
Example – ball (generic) and tennis ball (specialized)

Partitive
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Transitive
Meaning: Part/Whole, 
Relation between classes whereby one class constitutes the whole and the other class constitutes a part of that whole
Example – car (whole) and engine (part)

Instantiation
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Intransitive
Meaning: Instance of 
Relation between classes whereby one class represents a class of objects and the other class represents individual instances of the first class
Example – planet (class) and Saturn (instance)

Parent-Child
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Anti-transitive
Meaning: offspring of 
Relation between parents and their offspring. Mostly applies to people, but it can apply to other animals as well. Can apply to other situations, such as spawned processes in the UNIX operating system for computers
Example – parent (Queen Elizabeth) and child (Prince Charles)

Sequential
  * Can be cyclical (consider time as seen on a wall clock)
  * Anti-reflexive
  * Anti-symmetric
  * Intransitive
Meaning: Relation between classes based on temporal or spatial proximity
Example – production and consumption
Note – A sequence can either be based on precedes and follows or it can be based on previous and next. This description is the general case. Either kind is possible. The next description is the more specific (previous and next) case.

Immediate Sequential
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Anti-transitive
Meaning: Relation between classes based on immediate temporal or spatial proximity
Example – US and Canada

Temporal
  * Hierarchical (if date is included, not if time is only measured by a 12 or 24 hour clock)
  * Anti-reflexive
  * Anti-symmetric
  * Intransitive
Meaning: Sequential relation between classes involving events in time
Example – spring and summer
Note – A temporal sequence can either be based on precedes and follows or it can be based on previous and next. This description is the general case. Either kind is possible. The next description is the more specific (previous and next) case.

Immediate Temporal
  * Hierarchical
  * Anti-reflexive
  * Anti-symmetric
  * Anti-transitive
Meaning: Relation between classes based on immediate temporal proximity
Example – 1 o’clock and 2 o’clock

Causal
  * Can be cyclical
  * Anti-reflexive
  * Neither symmetric nor anti-symmetric
  * Intransitive
Relation between classes involving cause and its effect
Example – lightning and thunder; groups of teenaged boys punching each other

