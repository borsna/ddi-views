Using the Process pattern
==========================

The Process pattern introduced in DDI-Views consists of a basic set of classes to describe process steps and information flows between them. Some of its classes are extensions of classes in the Collections Pattern. Figure 1 illustrates a high level view of the Process pattern.

A Process Step performs one or more business functions at any granularity. Each Process Step can be performed by a Service. Process Steps and Services can have Interfaces with input, outputs and other interface definitions. Process Steps can be nested and thus describe processes at multiple levels of detail. The Process Control Step handles the execution flow of the steps in its scope. The Information Flow specifies how information objects move between Process Steps by mapping inputs and outputs. 

**Figure 1. The Process Pattern**

.. |figure1| image:: ../images/ddi4_process01.png

|figure1|

The Process Pattern can be realized in multiple ways. DDI-Views include a well-known realization called Workflow (Figure 2), which can be mapped to process execution languages like BPEL or BPMN. 

A Workflow is Sequence of Workflow Steps that performs one or more business functions. Each Workflow Step can be performed by a Workflow Service. 

**Figure 2. Workflow**

.. |figure2| image:: ../images/ddi4_process02.png

|figure2|

There are two types of Workflow Steps: Acts and Control Constructs. 

Acts represent actions and are atomic Process Steps, i.e. they cannot be composed of other Process Steps. An Act is similar to a terminal in the production rules of a formal grammar and an instruction in a programming language. 

A Control Construct describes logical execution flows between Process Steps. Control Constructs can be nested via its sub-classes and thus describe workflows at multiple levels of detail. The nesting of Workflow Steps always terminates in an Act. All nesting of Workflow Steps occurs via Workflow Sequences and Conditional Control Constructs, both specializations of Control Constructs. The former models linear execution of Workflow Steps whereas the latter includes three types of iterative constructs: repeatWhile, repeatUntil and Loop. 
The Workflow Sequence at the end of the *contains* association represents the body of the Conditional Control Construct, which is executed depending on the result of the *condition* evaluation. The specialized sub-classes determine whether the Sequence is executed in each iteration before the condition is evaluated (RepeatUntil), or after (RepeatWhile, Loop). The Loop also provides a counter with *initialValue* and *stepValue* that can be used to specify in the condition how many times the Sequence in the body is executed. 

**Figure 3. Control Constructs**

.. |figure3| image:: ../images/ddi4_process03.png

|figure3|

In addition to the iterative constructs, Conditional Control Constructs includes IfThenElse, which provides a means to specify branching control flows. 
It contains the *condition* inherited from the parent class and two associations: *contains* (also inherited from the parent class), to the Sequence of Process Steps that is executed when the condition is *true*, and *containsElse*, to an optional Sequence to be executed if the condition is evaluated to *false*. Optionally, IfThenElse can also have an associated ElseIf construct to model switch statements.

A Workflow Sequence can be viewed as a Collection whose Members are Workflow Steps that can be ordered in two different ways: with one or more Temporal Interval Relations, i.e. a design-time temporal constraint, or with a Rule (constructor) to determine ordering at run-time. Let’s begin discussing temporal constraints.

Temporal Interval Relations provide a mechanism for capturing Allen’s interval relations, one of the best established formalisms for temporal reasoning. 
Temporal Interval Relations can be used to define *temporal constraints* between pairs of Workflow Steps, e.g. whether the execution of two Workflow Steps can overlap in time or not, or one has to finish before the other one starts, etc. Note that this also supports parallel processing, which is also implicit in the model: the definition of Conditional Control Constructs can contain multiple Workflow Sequences that could be executed in parallel.

There are thirteen Temporal Interval Relations: twelve asymmetric ones, i.e. *precedes*, *meets*, *overlaps*, *finishes*, *contains*, *starts* and their converses, plus *equals*, which is the only one that has no converse or, rather, it is the same as its converse. Together these relations are distinct (any pair of definite intervals are described by one and only one of the relations), exhaustive (any pair of definite intervals are described by one of the relations), and qualitative (no numeric time spans are considered).

Following Allen’s, Temporal Interval Relations are defined as follows.

**Figure 4. Allen's Temporal Interval Relations**

.. |figure4| image:: ../images/ddi4_process04.png

|figure4|

In DDI-Views, each of the asymmetric Allen’s interval relations is a Temporal Interval Relation that realizes different Binary Relations with specific temporal semantics. All asymmetric Temporal Interval Relations contain Ordered Interval Pairs whereas the only symmetric one, i.e. Equals, contains Unordered Interval Pairs. For instance, the Precedes Interval Relation realizes the pattern as shown in the diagram below.

**Figure 5. Precedes Interval Relation**

.. |figure5| image:: ../images/ddi4_process05.png

|figure5|

Precedes Interval Relation and Ordered Interval Pair realize Strict Order Relation and Ordered Pair, respectively. 
If we look back to the Binary Relations Specialization diagram (in Using the Collections Pattern - Figure 4) we notice that Strict Order Relation has the TEMPORAL_PRECEDES semantics, among others, which means that the Process Step at the end of the *source* association in the Ordered Interval Pair has to finish before the one at the end of *target* starts. 

The Equals Interval Relation is a slightly different case because it is an equivalence relation rather than an asymmetric one and therefore it contains Unordered Interval Pairs. 

**Figure 6. Equals Interval Relation**

.. |figure6| image:: ../images/ddi4_process06.png

|figure6|

Equals Interval Relation and Unordered Interval Pair realize Equivalence Relation and Unordered Pair, respectively. Equivalence Relation is simply a Symmetric Binary Relation that is REFLEXIVE and TRANSITIVE with some additional semantics, among which we find TEMPORAL_EQUALS, the one required by the Equals Interval Relation. This means that the execution of the two Process Steps at the end of the maps association in Unordered Interval Pair begin and end at the same time.
Let’s see how Temporal Interval Relations work with an example.

Consider a questionnaire with set of questions and a control flow logic defined between them. The flow between questions can be modeled with a Workflow Sequence where each question is a realization of a Workflow Step. Now, let’s assume a couple of conditions: (i) question Q3 requires the answer of both Q1 and Q2, and (ii) question Q4 is triggered by answering question Q2. Note the difference between requiring an answer and being triggered by an answer. The former means that there is no necessary immediate execution, i.e. Q3 can be executed long after both Q1 and Q2 have been answered. In other words, the exact moment for executing Q3 will depend on other parts of the control flow logic of the questionnaire, i.e. other constraints defined in the questionnaire flow. However, when a question triggers another it means that the latter is executed right after the former is answered. 

With that in mind, the precedence constraint between Q1, Q2 and Q3 can be modeled with a Precedes Interval Relation whereas the one between Q2 and Q4 can be represented by a Meets Interval Relation. Remember that the fact that Q1 precedes Q3 means that Q1 finishes before Q3 starts whereas Q2 meets Q4 means that Q4 starts exactly when Q2 finishes, which is exactly what the two Temporal Interval Relations mentioned above can express.
The following (Figure 7) is the resulting model.

**Figure 7. Temporal Interval Relations Example**

.. |figure7| image:: ../images/ddi4_process07.png

|figure7|

So far we described the control flow logic that is specified by Control Constructs. How does information flow between the steps in the control flow logic? We mentioned that the pattern has a class for that purpose, i.e. Information Flow. Binding is the realization of Information Flow for Workflows. A Binding is a design-time class that maps Input and Output Parameters of different steps in a Workflow Sequence or between a step and its sub-steps. 

**Figure 8. Binding**

.. |figure8| image:: ../images/ddi4_process08.png

|figure8|

Let’s illustrate the use of Bindings with an example.

Consider a Workflow that implements a fragment of the GSBPM Process phase. This is a specific implementation of GSBPM within the context of an organization in which some sub-steps are not implemented.

**Figure 9. Bindings Example**

.. |figure9| image:: ../images/ddi4_process09.png

|figure9|

The step at the top, Process Data, has four Parameters, three Input and one Output, each with its type between brackets, i.e. Classification, Instance Variable and Edit Rules for Input and Instance Variable for Output. This means that each parameter can hold at runtime only objects of the specified type. 

There are also two sub-steps organized in a Workflow Sequence. The first sub-step, Code, has an Instance Variable and a Classification as Inputs and an Instance Variable as Output. The second sub-step, Edit and Impute, has the coded Instance Variable and Edit Rules as Input and an Instance Variable as Output.

How do we put all the pieces together? The top step gets a collected Instance Variable and produces a coded, edited and imputed Instance Variable based on a Classification and some Edit Rules. This is achieved by invoking the two sub-steps just described in sequence.

In order for this to work, the Input and Output Parameters of each step have to map so that the Instance Variable the top step receives moves thru the two sub-steps and is returned back, transformed, to the top step. This is made possible by the Bindings. 

The first Binding on the left maps the Classification Input Parameter of Process Variable to that of the Code sub-step. This is an example of input-to-input Binding between two levels, i.e. a container step and its sub-steps. The bottom Binding maps the Instance Variable Output Parameter of Code to the Instance Variable Input Parameter of Edit and Impute. This is an example of an output-to-input Binding between steps at the same level, i.e. within the same Control Construct container. The other Bindings make sure that all Parameters are mapped.
