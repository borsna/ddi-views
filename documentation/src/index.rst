==========================================
DDI-Views (Q2 Release 2016) Documentation
==========================================

.. image:: ../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   About/index.rst
   Preface/index.rst
   AssociatedInfo/index.rst
   Introduction/index.rst
   userguides/index.rst
   Package/index.rst
   View/index.rst
   About/glossary.rst
..   About/documentationSyntax.rst
..   usecases/index.rst
