.. _Process:


Process
*******
Process describes how a design is used to implement a goal. It is the series of steps taken as a whole. It is decomposable into ProcessSteps, but this decomposition is not necessary. 



Extends
=======
:ref:`Identifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
contains             ProcessStep       0..n
hasDesign            Design            0..1
hasPrecondition      Precondition      0..n
hasResults           Result            0..n
implementsAlgorithm  Algorithm         0..n
isDiscussedIn        ExternalMaterial  0..n
===================  ================  ===========


contains
########
The process step that initiates the detailed process.




hasDesign
#########
The design informs the specific process by providing the underlying rationale, rules and general preconditions for the process. The design is the reusable part of the implemented process.




hasPrecondition
###############
Any condition that must be met prior to the process being implemented.




hasResults
##########
The end result of the process which can contain a binding to specific outcome collections as well as usage instructions for implementing results




implementsAlgorithm
###################
An implementation and/or execution of an algorithm which is associated with the design of the process.




isDiscussedIn
#############
Identifies material discussing the process. The material may be in DDI or other format.






Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/Process.dot