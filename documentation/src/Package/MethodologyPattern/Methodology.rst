.. _Methodology:


Methodology
***********
Methodology may be used to define the design and process used within data collection, management, and use to achieve overall studies or specific activities such as sampling or weighting. A methodology in normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work.





Extends
=======
:ref:`Identifiable`


Properties
==========

=========  ================  ===========
Name       Type              Cardinality
=========  ================  ===========
name       Name              0..n
overview   StructuredString  0..1
rationale  StructuredString  0..1
usage      StructuredString  0..1
=========  ================  ===========


name
####
A linguistic signifier. Human  understandable name (word, phrase, or  mnemonic) that reflects the ISO/IEC  11179-5 naming principles. If more than  one name is provided provide a context to  differentiate usage. 


overview
########
Short natural language account of the  information obtained from the  combination of properties and  relationships associated with an object.  Supports the use of multiple languages  and structured text. 


rationale
#########
Explanation of the reasons some decision  was made or some object exists. Supports  the use of multiple languages and  structured text.  


usage
#####
Explanation of the ways in which some  decision or object is employed. Supports  the use of multiple languages and  structured text.  

`


Relationships
=============

====================  ===========  ===========
Name                  Type         Cardinality
====================  ===========  ===========
componentMethodology  Methodology  0..n
hasDesign             Design       0..n
hasProcess            Process      0..n
isExpressedBy         Algorithm    0..n
====================  ===========  ===========


componentMethodology
####################
A methodology which is a component part of this Methodology




hasDesign
#########
Design used to form the Methodology




hasProcess
##########
Process used to implement the Methodology




isExpressedBy
#############
An algorithm that defines the Methodology in a generic way.






Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/Methodology.dot