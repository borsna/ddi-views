.. _Algorithm:


Algorithm
*********
An algorithm is an effective method that can be expressed within a finite amount of space and time and in a well-defined formal language for calculating a function. Starting from an initial state and initial input (perhaps empty), the instructions describe a computation that, when executed, proceeds through a finite number of well-defined successive states, eventually producing "output" and terminating at a final ending state. The transition from one state to the next is not necessarily deterministic; some algorithms, known as randomized algorithms, incorporate random input. [from www.wikipedia.org 15 April 2016]

The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.



Extends
=======
:ref:`Identifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
isDiscussedIn  ExternalMaterial  0..n
=============  ================  ===========


isDiscussedIn
#############
Identifies material discussing the algorithm. The material may be in DDI or other format.






Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/Algorithm.dot