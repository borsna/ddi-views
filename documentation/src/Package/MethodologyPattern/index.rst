******************
MethodologyPattern
******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Algorithm
   Design
   Methodology
   Process



Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/MethodologyPattern.dot