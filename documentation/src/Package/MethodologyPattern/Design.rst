.. _Design:


Design
******
The design pattern class may be used to specify or, again, defines how a process will be performed in general. The design informs a specific or implemented process as to its general parameters.



Extends
=======
:ref:`Identifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
assumesPrecondition  Precondition      0..n
expressesDesign      Algorithm         0..n
isDiscussedIn        ExternalMaterial  0..n
specifiesGoal        Goal              0..n
===================  ================  ===========


assumesPrecondition
###################
A precondition that must exist prior to the applied use of this design.




expressesDesign
###############
An algorithm that defines the process used by the design in a generic way.




isDiscussedIn
#############
External materials that describe or discuss the design 




specifiesGoal
#############
The generic goal of a design.






Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/Design.dot