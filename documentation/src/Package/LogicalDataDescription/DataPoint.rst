.. _DataPoint:


DataPoint
*********
A DataPoint is a container for a Datum.



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
has            Datum             0..n
isDescribedBy  InstanceVariable  0..n
realizes       Member            0..n
=============  ================  ===========


has
###
The Datum populating the DataPoint.




isDescribedBy
#############
The InstanceVariable delimits the values which can populate a DataPoint.




realizes
########
Classes which realize the class Member fufill the role of Member e.g. they may have relations defined on them, for example one DataPoint follows another in a  Record.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/DataPoint.dot