.. _RecordRelation:


RecordRelation
**************
The RecordRelation object is used to indicate relationships among record types within and between datasets. For InstanceVariables existing in a data set with multiple record layouts, pairs of InstanceVariables may function as paired keys to permit the expression of hierarchical links between records of different types. These links between keys in different record types could also be used to link records in a relational structure.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ================  ===========
Name          Type              Cardinality
============  ================  ===========
displayLabel  DisplayLabel      0..n
purpose       StructuredString  0..1
usage         StructuredString  0..1
============  ================  ===========


displayLabel
############
A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

========  =======================  ===========
Name      Type                     Cardinality
========  =======================  ===========
contains  InstanceVariableMapping  0..n
maps      LogicalRecordLayout      0..n
realizes  SymmetricRelation        0..n
========  =======================  ===========


contains
########
Correspondences between Instance Variables of different Logical Record Layout. Realization of contains in Symmetric Relation.




maps
####
Map related record types, which are Collections of Instance Variables. Realization of structures in Symmetric Relation.





realizes
########
Class in the Collections Pattern realized by this class. 






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/RecordRelation.dot