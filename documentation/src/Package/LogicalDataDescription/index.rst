**********************
LogicalDataDescription
**********************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AttributeRole
   DataPoint
   DataRecord
   DataStore
   DatastoreLibrary
   Datum
   IdentifierRole
   InstanceVariableMapping
   LayoutOrderedPair
   LogicalRecordLayout
   LogicalRecordLayoutOrder
   MeasureRole
   Observation
   RecordRelation
   Viewpoint
   ViewpointRole



Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/LogicalDataDescription.dot