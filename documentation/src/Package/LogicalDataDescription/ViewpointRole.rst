.. _ViewpointRole:


ViewpointRole
*************
A ViewpointRole designates the function an InstanceVariable performs in the context of the ViewPoint. (IdentifierRole, AttributeRole, or MeasureRole of interest).




Extends
=======
:ref:`AnnotatedIdentifiable


Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/ViewpointRole.dot