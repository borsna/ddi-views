.. _Viewpoint:


Viewpoint
*********
The assignment of measure, identifier and attribute roles to InstanceVariables



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

=================  ==============  ===========
Name               Type            Cardinality
=================  ==============  ===========
hasAttributeRole   AttributeRole   1..n
hasIdentifierRole  IdentifierRole  1..1
hasMeasureRole     MeasureRole     1..1
=================  ==============  ===========


hasAttributeRole
################
The InstanceVaribles functioning as attributes




hasIdentifierRole
#################
The InstanceVaribles functioning as identifiers




hasMeasureRole
##############
The InstanceVaribles functioning as measures






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/Viewpoint.dot