.. _LogicalRecordLayoutOrder:


LogicalRecordLayoutOrder
************************
A realization of OrderRelation that is used to describe the sequence of Instance Variables in a record layout.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
displayLabel  DisplayLabel                       0..n
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


displayLabel
############
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


reflexivity
###########
Fixed to Reflexive


semantics
#########
Controlled vocabulary for the order relation semantics. It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.


symmetry
########
Fixed to Anti_Symmetric


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


transitivity
############
Fixed to Transitive


usage
#####
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.

`


Relationships
=============

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
contains    LayoutOrderedPair    0..n
realizes    OrderRelation        0..n
structures  LogicalRecordLayout  0..n
==========  ===================  ===========


contains
########
Layout ordered pairs describing the Instance Variable sequence.




realizes
########
Class of the Collection pattern realized by this class.




structures
##########
The Instance Variables in a record layout to be sequenced.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/LogicalRecordLayoutOrder.dot