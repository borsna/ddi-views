.. _DataRecord:


DataRecord
**********
A Record is a Collection of DataPoints with an optional OrderRelation.

[Name of the class needs to be changed to Record].



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  1..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

================  ===================  ===========
Name              Type                 Cardinality
================  ===================  ===========
contains          DataPoint            1..1
hasLogicalLayout  LogicalRecordLayout  0..n
isViewedFrom      Viewpoint            1..n
realizes          Collection           0..n
================  ===================  ===========


contains
########
The set of DataPoints composing the DataRecord




hasLogicalLayout
################
Structure (type) of the Data Record.




isViewedFrom
############
A DataRecord may have multiple ViewPoints, each of which assigns among roles identifier, measure, and attribute to the InstanceVariables associated with each DataPoint.




realizes
########
Allows the DataStucture to function as a Collection, enabling, for example, ordering of its components.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/DataRecord.dot