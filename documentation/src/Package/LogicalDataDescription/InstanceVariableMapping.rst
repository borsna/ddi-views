.. _InstanceVariableMapping:


InstanceVariableMapping
***********************
Relation between Instance Variables in different Logical Record Layouts.



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
maps      InstanceVariable  0..n
realizes  UnorderedTuple    0..n
========  ================  ===========


maps
####
Mapping between Instance Variables in different Logical Record Layouts. 




realizes
########
Class in the Collections Pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/InstanceVariableMapping.dot