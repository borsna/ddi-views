.. _LayoutOrderedPair:


LayoutOrderedPair
*****************
The pair of Instance Variables in a Record Layout which are being placed in a sequence.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
realizes  OrderedPair       0..n
source    InstanceVariable  0..n
target    InstanceVariable  0..n
========  ================  ===========


realizes
########
Class of the Collection pattern realized by this class.




source
######
Specialization of source in OrderedPair for Instance Variables in a Logical Record Layout.




target
######
Specialization of target in OrderedPair for Instance Variables in a Logical Record Layout.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/LayoutOrderedPair.dot