.. _Observation:


Observation
***********
The result of applying a particular Capture in an Instrument to some Unit.

`


Relationships
=============

=============================  ===================  ===========
Name                           Type                 Cardinality
=============================  ===================  ===========
isDesignatedBy                 Datum                1..1
isMadeOn                       Unit                 0..n
producedByInstrumentComponent  InstrumentComponent  1..1
=============================  ===================  ===========


isDesignatedBy
##############
A datum designates an observation




isMadeOn
########
The Unit of which the Observation is made.




producedByInstrumentComponent
#############################
The InstanceMeasure, InstanceQuestion and/or other InstrumentComponents used to create the Observation.






Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/Observation.dot