.. _BinaryRelation:


BinaryRelation
**************
Set of ordered pairs of Members of the Collection the Binary Relation structures.



Extends
=======
:ref:`Identifiable`


Properties
==========

============  ================  ===========
Name          Type              Cardinality
============  ================  ===========
reflexivity   ReflexivityType   1..1
symmetry      SymmetryType      1..1
totality      TotalityType      1..1
transitivity  TransitivityType  1..1
============  ================  ===========


reflexivity
###########
Controlled Vocabulary to specify whether the relation is reflexive, anti-reflexive, neither or unknown.


symmetry
########
Controlled Vocabulary to specify whether the relation is symmetric, anti-symmetric, asymmetric or unknown.


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


transitivity
############
Controlled Vocabulary to specify whether the relation is transitive, anti-transitive, neither or unknown.

`


Relationships
=============

==========  ==========  ===========
Name        Type        Cardinality
==========  ==========  ===========
structures  Collection  0..n
==========  ==========  ===========


structures
##########
Collection whose Members are grouped by pairs to support a variety of structures.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/BinaryRelation.dot