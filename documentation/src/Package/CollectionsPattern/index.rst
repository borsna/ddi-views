******************
CollectionsPattern
******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AcyclicPrecedenceRelation
   AsymmetricBinaryRelation
   AsymmetricRelation
   BinaryRelation
   Collection
   EquivalenceRelation
   ImmediatePrecedenceRelation
   Member
   NaryRelation
   OrderRelation
   OrderedPair
   OrderedTuple
   StrictOrderRelation
   SymmetricBinaryRelation
   SymmetricRelation
   UnorderedPair
   UnorderedTuple



Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/CollectionsPattern.dot