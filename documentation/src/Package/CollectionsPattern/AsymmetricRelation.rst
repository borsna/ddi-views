.. _AsymmetricRelation:


AsymmetricRelation
******************
Relation whose n-ary tuples are ordered. It could be the result of a join between one or more Binary Relations.



Extends
=======
:ref:`NaryRelation`


Relationships
=============

========  ============  ===========
Name      Type          Cardinality
========  ============  ===========
contains  OrderedTuple  0..n
========  ============  ===========


contains
########
Ordered tuples of Members.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/AsymmetricRelation.dot