.. _UnorderedPair:


UnorderedPair
*************
Unordered pair of Members of the Collection structured by the Symmetric Binary Relation to which the pair belongs.



Extends
=======
:ref:`Identifiable`


Relationships
=============

====  ======  ===========
Name  Type    Cardinality
====  ======  ===========
maps  Member  0..n
====  ======  ===========


maps
####
Members in the Unordered Pair






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/UnorderedPair.dot