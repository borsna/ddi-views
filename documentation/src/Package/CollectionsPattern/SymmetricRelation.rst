.. _SymmetricRelation:


SymmetricRelation
*****************
Relation whose n-ary tuples are not ordered. It could be the result of a join between one or more Binary Relations.



Extends
=======
:ref:`NaryRelation`


Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
contains  UnorderedTuple  0..n
========  ==============  ===========


contains
########
Unordered tuples of Members.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/SymmetricRelation.dot