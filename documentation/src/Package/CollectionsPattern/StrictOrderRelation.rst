.. _StrictOrderRelation:


StrictOrderRelation
*******************
Relation over members in a collection (set or bag) characterized by the following assignments to the types inherited from Asymmetric Binary Relation:
- Reflexivity = Anti_Reflexive;
- Symmetry = Anti_Symmetric (more restrictive than parent class) ;
- Transitivity = Transitive.

It must contain like items.




Extends
=======
:ref:`AsymmetricBinaryRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


reflexivity
###########
Fixed to Anti_Reflexive


semantics
#########
Controlled vocabulary for the strict order relation semantics. It should contain, at least, the following: Descendant_Of, Strict_Part_Of, Less_Than, Strict_Subtype_Of, Predecessor_Of, Successor_Of, Temporal_Precedes, Temporal_Finishes, Temporal_Contains and Temporal_Starts


symmetry
########
Fixed to Anti_Symmetric


transitivity
############
Fixed to Transitive




Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/StrictOrderRelation.dot