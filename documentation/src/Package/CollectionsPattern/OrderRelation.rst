.. _OrderRelation:


OrderRelation
*************
Relation over members in a collection (set or bag) characterized by the following assignments to the types inherited from Asymmetric Binary Relation:
- Reflexivity = Reflexive;
- Symmetry  = Anti_Symmetric (more restrictive than parent class) ;
- Transitivity = Transitive.

It must contain like items.



Extends
=======
:ref:`AsymmetricBinaryRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


criteria
########
Intentional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


reflexivity
###########
Fixed to Reflexive


semantics
#########
Controlled vocabulary for the order relation semantics.  It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.


symmetry
########
Fixed to Anti_Symmetric


transitivity
############
Fixed to Transitive




Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/OrderRelation.dot