.. _Member:


Member
******
Generic class representing members of a collection. 




Extends
=======
:ref:`Identifiable


Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/Member.dot