.. _AcyclicPrecedenceRelation:


AcyclicPrecedenceRelation
*************************
Relation over members in a collection (set or bag) characterized by the following assignments to the types inherited from Asymmetric Binary Relation:
- Reflexivity = Anti_Reflexive;
- Symmetry = Anti_Symmetric (more restrictive than parent class) ;
- Transitivity = Neither.

It must contain like items.




Extends
=======
:ref:`AsymmetricBinaryRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti_Reflexive


semantics
#########
Controlled vocabulary for the AcyclicPrecedenceRelationsemantics. It should contain, at least, the following: Tangential_Proper_Part and Temporal_Overlaps


symmetry
########
Fixed to Anti_Symmetric


transitivity
############
Fixed to Neither




Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/AcyclicPrecedenceRelation.dot