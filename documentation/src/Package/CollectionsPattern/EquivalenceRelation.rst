.. _EquivalenceRelation:


EquivalenceRelation
*******************
Relation over members in a collection (set or bag) characterized by the following assignments to the types inherited from Symmetric Binary Relation:
- Reflexivity = Reflexive;
- Symmetry = Symmetric (same as parent class);
- Transitivity = Transitive.

It must contain like items.



Extends
=======
:ref:`SymmetricBinaryRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Reflexive


semantics
#########
Controlled vocabulary for the equivalence relation semantics. It should contain, at least, the following: Same_As, Similar_To, Congruent_To, Compatible_With, Equidistant_From, Temporal_Equals.


transitivity
############
Fixed to Transitive




Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/EquivalenceRelation.dot