.. _OrderedPair:


OrderedPair
***********
Ordered pair of Members of the Collection structured by the Asymmetric Binary Relation to which the pair belongs.



Extends
=======
:ref:`Identifiable`


Relationships
=============

======  ======  ===========
Name    Type    Cardinality
======  ======  ===========
source  Member  0..n
target  Member  0..n
======  ======  ===========


source
######
First member in the Ordered Pair.




target
######
Second member in the Ordered Pair.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/OrderedPair.dot