.. _Collection:


Collection
**********
Collection container (set or bag). It could have an optional order relation (total or partial) associated to it to model linear order, hierarchies and nesting. A Collection is also a subtype of Member to allow for nested collections.



Extends
=======
:ref:`Member`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========  ======  ===========
Name      Type    Cardinality
========  ======  ===========
contains  Member  0..n
========  ======  ===========


contains
########
Members of the collection.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/Collection.dot