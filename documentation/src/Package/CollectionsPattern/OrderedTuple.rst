.. _OrderedTuple:


OrderedTuple
************
Ordered n-ary tuple of Members of the Collection structured by the Asymmetric Relation to which the tuple belongs. Members are separated into two groups: source and target.



Extends
=======
:ref:`Identifiable`


Relationships
=============

======  ======  ===========
Name    Type    Cardinality
======  ======  ===========
source  Member  0..n
target  Member  0..n
======  ======  ===========


source
######
Set of Members in the n-ary tuple identified as source. If the Ordered Tuple is the result of joining one or more Ordered Pairs, then it is the union of the source Members in the joined Ordered Pairs.




target
######
Set of Members in the n-ary tuple identified as target. If the Ordered Tuple is the result of joining one or more Ordered Pairs, then it is the union of the target Members in the joined Ordered Pairs.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/OrderedTuple.dot