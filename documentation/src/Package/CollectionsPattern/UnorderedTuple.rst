.. _UnorderedTuple:


UnorderedTuple
**************
Unordered n-ary tuple of Members of the Collection structured by the Symmetric Relation to which the tuple belongs.



Extends
=======
:ref:`Identifiable`


Relationships
=============

====  ======  ===========
Name  Type    Cardinality
====  ======  ===========
maps  Member  0..n
====  ======  ===========


maps
####
Set of Members in the n-ary tuple. If the Unordered Tuple is the result of joining one or more Unordered Pairs, then it is the union of the Members in the joined Unordered Pairs.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/UnorderedTuple.dot