.. _SymmetricBinaryRelation:


SymmetricBinaryRelation
***********************
Relation over members in a Collection (set or bag) characterized by the following assignments to the types inherited from Binary Relation:
- Symmetry = Symmetric.





Extends
=======
:ref:`BinaryRelation`


Properties
==========

========  ============  ===========
Name      Type          Cardinality
========  ============  ===========
symmetry  SymmetryType  1..1
========  ============  ===========


symmetry
########
Fixed to Symmetric

`


Relationships
=============

========  =============  ===========
Name      Type           Cardinality
========  =============  ===========
contains  UnorderedPair  0..n
========  =============  ===========


contains
########
Unordered pair of Members






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/SymmetricBinaryRelation.dot