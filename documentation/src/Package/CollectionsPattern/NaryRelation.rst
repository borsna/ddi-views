.. _NaryRelation:


NaryRelation
************
Set of ordered, n-ary tuples of Members of the Collection the Relation structures. 



Extends
=======
:ref:`Identifiable`


Relationships
=============

==========  ==========  ===========
Name        Type        Cardinality
==========  ==========  ===========
structures  Collection  0..n
==========  ==========  ===========


structures
##########
Collection(s) whose Members are grouped by n-ary tuples to support a variety of structures.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/NaryRelation.dot