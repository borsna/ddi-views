.. _AsymmetricBinaryRelation:


AsymmetricBinaryRelation
************************
Relation over members in a Collection (set or bag) characterized by the following assignments to the types inherited from Binary Relation:
- Symmetry = Asymmetric.



Extends
=======
:ref:`BinaryRelation`


Properties
==========

========  ============  ===========
Name      Type          Cardinality
========  ============  ===========
symmetry  SymmetryType  1..1
========  ============  ===========


symmetry
########
Fixed to Asymmetric

`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
contains  OrderedPair  0..n
========  ===========  ===========


contains
########
Ordered pair of Members.






Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/AsymmetricBinaryRelation.dot