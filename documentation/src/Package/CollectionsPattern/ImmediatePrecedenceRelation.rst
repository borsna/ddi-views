.. _ImmediatePrecedenceRelation:


ImmediatePrecedenceRelation
***************************
Relation over members in a collection (set or bag) characterized by the following assignments to the types inherited from Asymmetric Binary Relation:
- Reflexivity = Anti_Reflexive;
- Symmetry = Anti_Symmetric (more restrictive than parent class) ;
- Transitivity = Anti_Transitive.

It must contain like items.




Extends
=======
:ref:`AsymmetricBinaryRelation`


Properties
==========

===========  =================================  ===========
Name         Type                               Cardinality
===========  =================================  ===========
reflexivity  ReflexivityType                    1..1
semantics    ExternalControlledVocabularyEntry  0..1
symmetry     SymmetryType                       1..1
trastivity   TransitivityType                   1..1
===========  =================================  ===========


reflexivity
###########
Fixed to Anti_Reflexive


semantics
#########
Controlled vocabulary for the ImmediatePrecedenceRelation semantics. It should contain, at least, the following: Parent_Of, Child_Of, Next, Previous, Instance_Of, Temporal_Meets.


symmetry
########
Fixed to Anti_Symmetric


trastivity
##########
Fixed to Anti_Transitive




Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/ImmediatePrecedenceRelation.dot