.. _NodeHierarchyPair:


NodeHierarchyPair
*****************
Parent-child pair of nodes in a Node Hierarchy.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
child     Node         0..1
parent    Node         0..n
realizes  OrderedPair  0..n
========  ===========  ===========


child
#####
Specialization of target in OrderedPair for children of Nodes.




parent
######
Specialization of source in OrderedPair for parents of Nodes.




realizes
########
Class of the Collection pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/Representations/NodeHierarchyPair.dot