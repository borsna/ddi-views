.. _IndexEntrySequence:


IndexEntrySequence
******************
Sequence of Classification Index Entries in a Classification Index.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Next


symmetry
########
Fixed to Anti-Symmetric


totality
########
Fixed to Total


transitivity
############
Fixed to Anti-Transitive

`


Relationships
=============

==========  ===========================  ===========
Name        Type                         Cardinality
==========  ===========================  ===========
contains    IndexEntryPair               1..n
realizes    ImmediatePrecedenceRelation  0..n
structures  ClassificationIndex          0..n
==========  ===========================  ===========


contains
########
Preceding-following pair of entries in the sequence.




realizes
########
Class of the Collection pattern realized by this class.




structures
##########
Classification Index ordered by the Index Entry Sequence.






Graph
=====

.. graphviz:: /images/graph/Representations/IndexEntrySequence.dot