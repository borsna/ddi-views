.. _Code:


Code
****
A Designation for a Category.



Extends
=======
:ref:`Designation`


Relationships
=============

=======  ========  ===========
Name     Type      Cardinality
=======  ========  ===========
denotes  Category  0..n
=======  ========  ===========


denotes
#######
A definition for the code. Specialization of denotes for Categories.






Graph
=====

.. graphviz:: /images/graph/Representations/Code.dot