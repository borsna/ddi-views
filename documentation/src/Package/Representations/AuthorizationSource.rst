.. _AuthorizationSource:


AuthorizationSource
*******************
Identifies the authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========================  ===================  ===========
Name                      Type                 Cardinality
========================  ===================  ===========
authorizationDate         Date                 0..1
legalMandate              InternationalString  0..1
purpose                   StructuredString     0..1
statementOfAuthorization  StructuredString     0..1
========================  ===================  ===========


authorizationDate
#################
Identifies the date of Authorization.


legalMandate
############
Provide a legal citation to a law authorizing the study/data collection. For example, a legal citation for a law authorizing a country's census.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


statementOfAuthorization
########################
Text of the authorization (law, mandate, approved business case).

`


Relationships
=============

================  =====  ===========
Name              Type   Cardinality
================  =====  ===========
authorizingAgent  Agent  0..n
================  =====  ===========


authorizingAgent
################
References the authorizing agent generally described as an organization or individual






Graph
=====

.. graphviz:: /images/graph/Representations/AuthorizationSource.dot