.. _NodePartitiveRelation:


NodePartitiveRelation
*********************
Part-whole relation of Nodes in a Node Set.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
displayLabel  DisplayLabel                       0..n
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


criteria
########
Intentional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


displayLabel
############
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


reflexivity
###########
Fixed to Reflexive


semantics
#########
Fixed to Part-Of


symmetry
########
Fixed to Anti-Symmetric


totality
########
Fixed to Partial


transitivity
############
Fixed to Transitive


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  =================  ===========
Name        Type               Cardinality
==========  =================  ===========
contains    NodePartitivePair  1..n
realizes    OrderRelation      0..n
structures  NodeSet            0..n
==========  =================  ===========


contains
########
Part-whole pair of nodes in the relation.




realizes
########
Class of the Collection pattern realized by this class.




structures
##########
NodeSet structured by the NodePartitiveRelation.






Graph
=====

.. graphviz:: /images/graph/Representations/NodePartitiveRelation.dot