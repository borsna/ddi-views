.. _NodePartitivePair:


NodePartitivePair
*****************
Part-whole pair of nodes in a Node Partitive Relation



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
part      Node         0..1
realizes  OrderedPair  0..n
whole     Node         0..n
========  ===========  ===========


part
####
Specialization of source in OrderedPair for representing the part in the partitive relationship.




realizes
########
Class of the Collection pattern realized by this class.




whole
#####
Specialization of target in OrderedPair for representing the whole in the partitive relationship.






Graph
=====

.. graphviz:: /images/graph/Representations/NodePartitivePair.dot