***************
Representations
***************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AuthorizationSource
   CategorySet
   ClassificationFamily
   ClassificationIndex
   ClassificationIndexEntry
   ClassificationItem
   ClassificationSeries
   Code
   CodeItem
   CodeList
   CorrespondenceTable
   Designation
   IndexEntryPair
   IndexEntrySequence
   Level
   Map
   Node
   NodeHierarchy
   NodeHierarchyPair
   NodePartitivePair
   NodePartitiveRelation
   NodeSet
   SentinelValueDomain
   StatisticalClassification
   SubstantiveValueDomain
   ValueAndConceptDescription
   ValueDomain
   Vocabulary



Graph
=====

.. graphviz:: /images/graph/Representations/Representations.dot