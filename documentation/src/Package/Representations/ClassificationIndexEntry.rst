.. _ClassificationIndexEntry:


ClassificationIndexEntry
************************
A Classification Index Entry is a word or a short text (e.g. the name of a locality, an economic activity or an occupational title) describing a type of object/unit or object property to which a Classification Item applies, together with the code of the corresponding Classification Item. Each Classification Index Entry typically refers to one item of the Statistical Classification. Although a Classification Index Entry may be associated with a Classification Item at any Level of a Statistical Classification, Classification Index Entries are normally associated with items at the lowest Level.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==================  ===================  ===========
Name                Type                 Cardinality
==================  ===================  ===========
codingInstructions  CommandCode          0..1
content             InternationalString  0..1
validDates          DateRange            0..1
==================  ===================  ===========


codingInstructions
##################
Additional information which drives the coding process. Required when coding is dependent upon one or many other factors.


content
#######
Text describing the type of object/unit or object property.


validDates
##########
Date from which the Classification Index Entry became valid (startDate). The date must be defined if the Classification Index Entry belongs to a floating Classification Index. Date at which the Classification Index Entry became invalid (endDate). The date must be defined if the Classification Index Entry belongs to a floating Classification Index and is no longer valid. 

`


Relationships
=============

========  ======  ===========
Name      Type    Cardinality
========  ======  ===========
realizes  Member  0..n
========  ======  ===========


realizes
########
Class of the Collection pattern realized by this class. 






Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationIndexEntry.dot