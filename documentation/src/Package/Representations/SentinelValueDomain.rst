.. _SentinelValueDomain:


SentinelValueDomain
*******************
The Value Domain for a sentinel conceptual domain. Sentinel values are defined in ISO 11404 as
"element of a value space that is not completely consistent with a datatype's properties and characterizing operations...". A common example would be codes for missing values.



Extends
=======
:ref:`ValueDomain`


Relationships
=============

=====================  ==========================  ===========
Name                   Type                        Cardinality
=====================  ==========================  ===========
describedValueDomain   ValueAndConceptDescription  0..n
enumeratedValueDomain  CodeList                    0..n
takesConceptsFrom      SentinelConceptualDomain    0..n
=====================  ==========================  ===========


describedValueDomain
####################
A formal description of the set of valid values - for described value domains.




enumeratedValueDomain
#####################
A CodeList enumerating the set of valid values.




takesConceptsFrom
#################
Corresponding conceptual definition given by a SentinelConceptualDomain.






Graph
=====

.. graphviz:: /images/graph/Representations/SentinelValueDomain.dot