.. _NodeSet:


NodeSet
*******
A NodeSet is a set of Nodes, which could be organized into a hierarchy of Levels.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

=========  ==========  ===========
Name       Type        Cardinality
=========  ==========  ===========
contains   Node        1..n
hasLevel   Level       1..n
isBasedOn  Concept     0..n
realizes   Collection  0..n
=========  ==========  ===========


contains
########
Specialization of contains in Collection for Nodes.




hasLevel
########
Specialization of contains in Collection for Levels.




isBasedOn
#########
Associated concept within a system.




realizes
########
Indicates that this class implements part of the Collection pattern






Graph
=====

.. graphviz:: /images/graph/Representations/NodeSet.dot