.. _Level:


Level
*****
The Level describes the nesting structure of a hierarchical collection.





Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

===========  ==========  ===========
Name         Type        Cardinality
===========  ==========  ===========
groups       Node        1..1
isDefinedBy  Concept     0..n
realizes     Collection  0..n
===========  ==========  ===========


groups
######
Realization of contains in Collection for Nodes.




isDefinedBy
###########
Associated concept that provides the conceptual definition of the level.




realizes
########
Class of the Collection pattern realized by this class.







Graph
=====

.. graphviz:: /images/graph/Representations/Level.dot