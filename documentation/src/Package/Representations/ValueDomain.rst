.. _ValueDomain:


ValueDomain
***********
The permitted range of values for a characteristic of a variable. [GSIM 1.1]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
displayLabel         DisplayLabel                       0..n
recommendedDataType  ExternalControlledVocabularyEntry  0..n
===================  =================================  ===========


displayLabel
############
A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


recommendedDataType
###################
The data types that are recommended for use with this domain




Graph
=====

.. graphviz:: /images/graph/Representations/ValueDomain.dot