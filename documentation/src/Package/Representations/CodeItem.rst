.. _CodeItem:


CodeItem
********
A type of Node contained in a CodeList that has a Code associated to a Category. In addition, a CodeItem can have a number of optional Designations. All Designations associated to the Code Item, including the Code, are synonyms, i.e. are associated with the same Concept.



Extends
=======
:ref:`Node


Graph
=====

.. graphviz:: /images/graph/Representations/CodeItem.dot