.. _ClassificationIndex:


ClassificationIndex
*******************
A Classification Index is an ordered list (alphabetical, in code order etc) of Classification Index Entries. A Classification Index can relate to one particular or to several Statistical Classifications. [GSIM Statistical Classification Model]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==================  ===================  ===========
Name                Type                 Cardinality
==================  ===================  ===========
availableLanguage                        0..n
codingInstructions  CommandCode          0..n
contactPersons      AgentAssociation     0..n
corrections         InternationalString  0..n
maintenanceUnit     AgentAssociation     0..1
name                Name                 0..n
purpose             StructuredString     0..1
releaseDate         Date                 0..1
type                CollectionType       0..1
==================  ===================  ===========


availableLanguage
#################
A Classification Index can exist in several languages. Indicates the languages available. If a Classification Index exists in several languages, the number of entries in each language may be different, as the number of terms describing the same phenomenon can change from one language to another. However, the same phenomena should be described in each language.


codingInstructions
##################
Additional information which drives the coding process for all entries in a Classification Index.


contactPersons
##############
Person(s) who may be contacted for additional information about the Classification Index.


corrections
###########
Verbal summary description of corrections, which have occurred within the Classification Index. Corrections include changing the item code associated with an Classification Index Entry.


maintenanceUnit
###############
The unit or group of persons within the organisation responsible for the Classification Index, i.e. for adding, changing or deleting Classification Index Entries.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


releaseDate
###########
Date when the current version of the Classification Index was released.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

===============  ========================  ===========
Name             Type                      Cardinality
===============  ========================  ===========
groups           ClassificationIndexEntry  1..n
hasPublications  ExternalMaterial          0..n
realizes         Collection                0..n
===============  ========================  ===========


groups
######
Realization of contains in Collection




hasPublications
###############
A list of the publications in which the Classification Index has been published.




realizes
########
Class of the Collection pattern realized by this class. 






Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationIndex.dot