.. _NodeHierarchy:


NodeHierarchy
*************
Parent-child hierarchy of Nodes in a Node Set.




Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Parent-Of


symmetry
########
Fixed to Anti-Symmetric


totality
########
Fixed to Partial


transitivity
############
Fixed to Anti-Transitive

`


Relationships
=============

==========  ===========================  ===========
Name        Type                         Cardinality
==========  ===========================  ===========
contains    NodeHierarchyPair            1..n
realizes    ImmediatePrecedenceRelation  0..n
structures  NodeSet                      0..n
==========  ===========================  ===========


contains
########
Parent-child pair of Nodes in the hierarchy.




realizes
########
Class of the Collection pattern realized by this class.




structures
##########
NodeSet structured by the NodeHierachy.






Graph
=====

.. graphviz:: /images/graph/Representations/NodeHierarchy.dot