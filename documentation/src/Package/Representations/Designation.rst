.. _Designation:


Designation
***********
A sign denoting a concept.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
representation  Signifier  1..1
==============  =========  ===========


representation
##############
A perceivable object used to denote a signified, i.e. a concept in this case.

`


Relationships
=============

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
denotes   Concept  0..n
realizes  Sign     0..n
========  =======  ===========


denotes
#######
The concept denoted by the designation. 




realizes
########
Class in the Signification Pattern realized by Designation. A Designation is a type of Sign.






Graph
=====

.. graphviz:: /images/graph/Representations/Designation.dot