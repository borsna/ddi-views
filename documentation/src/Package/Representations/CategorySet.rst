.. _CategorySet:


CategorySet
***********
A Category Set is a type of Node Set which groups Categories.




Extends
=======
:ref:`NodeSet`


Relationships
=============

===========  ========  ===========
Name         Type      Cardinality
===========  ========  ===========
hasCategory  Category  1..n
===========  ========  ===========


hasCategory
###########
Specialization of contains in NodeSet for Categories.






Graph
=====

.. graphviz:: /images/graph/Representations/CategorySet.dot