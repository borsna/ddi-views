.. _IndexEntryPair:


IndexEntryPair
**************
Indexing order, defined either by predecessor-successor pairs or by a criteria (e.g. alphabetical, in code order, etc.)



Extends
=======
:ref:`Identifiable`


Relationships
=============

==============  ========================  ===========
Name            Type                      Cardinality
==============  ========================  ===========
followingEntry  ClassificationIndexEntry  0..1
precedingEntry  ClassificationIndexEntry  0..1
realizes        OrderedPair               0..n
==============  ========================  ===========


followingEntry
##############
Realization of target in OrderedPair for ClassificationEntries.




precedingEntry
##############
Realization of source in OrderedPair for ClassificationEntries.




realizes
########
Class of the Collection pattern realized by this class. 






Graph
=====

.. graphviz:: /images/graph/Representations/IndexEntryPair.dot