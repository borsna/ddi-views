.. _StatisticalClassification:


StatisticalClassification
*************************
A Statistical Classification is a set of Categories which may be assigned to one or more variables registered in statistical surveys or administrative files, and used in the production and dissemination of statistics. The Categories at each Level of the classification structure must be mutually exclusive and jointly exhaustive of all objects/units in the population of interest. (Source: GSIM StatisticalClassification)



Extends
=======
:ref:`NodeSet`


Properties
==========

=================  ===================  ===========
Name               Type                 Cardinality
=================  ===================  ===========
availableLanguage                       0..n
changeFromBase     StructuredString     0..1
copyright          InternationalString  0..n
displayLabel       DisplayLabel         0..n
isCurrent          Boolean              0..1
isFloating         Boolean              0..1
purposeOfVariant   StructuredString     0..1
rationale          StructuredString     0..1
releaseDate        Date                 0..1
updateChanges      StructuredString     0..n
usage              StructuredString     0..1
validDates         DateRange            0..1
=================  ===================  ===========


availableLanguage
#################
A list of languages in which the Statistical Classification is available. Repeat for each langauge.


changeFromBase
##############
Describes the relationship between the variant and its base Statistical Classification, including regroupings, aggregations added and extensions. (Source: GSIM StatisticalClassification/Changes from base Statistical Classification)


copyright
#########
Copyright of the statistical classification.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


isCurrent
#########
Indicates if the Statistical Classification is currently valid.


isFloating
##########
Indicates if the Statistical Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating


purposeOfVariant
################
If the Statistical Classification is a variant, notes the specific purpose for which it was developed. (Source: GSIM StatisticalClassification/Purpose of variant)


rationale
#########
Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.


releaseDate
###########
Date the Statistical Classification was released


updateChanges
#############
Summary description of changes which have occurred since the most recent classification version or classification update came into force.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.


validDates
##########
The date the statistical classification enters production use and the date on which the Statistical Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)

`


Relationships
=============

===============  =========================  ===========
Name             Type                       Cardinality
===============  =========================  ===========
contains         ClassificationItem         1..n
hasDistribution  ExternalMaterial           0..n
has              ClassificationIndex        1..n
isMaintainedBy   Organization               0..n
predecessor      StatisticalClassification  0..1
successor        StatisticalClassification  0..1
variantOf        StatisticalClassification  0..n
===============  =========================  ===========


contains
########
Specialization of contains in NodeSet for ClassificationItems.




hasDistribution
###############
Description and link to a publication, including print, PDF, HTML and other electronic formats, in which the Statistical Classification has been published. This is similar to dcat:Distribution.




has
###
ClassificationIndex(es) related to the StatisticalClassification.




isMaintainedBy
##############
Organization, agency, or group within an agency responsible for the maintenance and upkeep of the statistical classification.




predecessor
###########
Statistical Classification superseded by the actual Statistical Classification (for those Statistical Classifications that are versions or updates).






successor
#########
Statistical Classification that supersedes the actual Statistical Classification (for those Statistical Classifications that are versions or updates), 




variantOf
#########
Statistical Classification on which the current variant is based, and any subsequent versions of that Statistical Classification to which it is also applicable.







Graph
=====

.. graphviz:: /images/graph/Representations/StatisticalClassification.dot