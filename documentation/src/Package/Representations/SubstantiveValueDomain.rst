.. _SubstantiveValueDomain:


SubstantiveValueDomain
**********************
The Value Domain for a substantive conceptual domain. 



Extends
=======
:ref:`ValueDomain`


Relationships
=============

=====================  ===========================  ===========
Name                   Type                         Cardinality
=====================  ===========================  ===========
describedValueDomain   ValueAndConceptDescription   0..n
enumeratedValueDomain  CodeList                     0..n
takesConceptsFrom      SubstantiveConceptualDomain  0..n
=====================  ===========================  ===========


describedValueDomain
####################
A formal description of the set of valid values - for described value domains.




enumeratedValueDomain
#####################
A CodeList enumerating the set of valid values.




takesConceptsFrom
#################
Corresponding conceptual definition given by an SubstantiveConceptualDomain.






Graph
=====

.. graphviz:: /images/graph/Representations/SubstantiveValueDomain.dot