.. _Node:


Node
****
A combination of a category and its designations.




Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

================  ===========  ===========
Name              Type         Cardinality
================  ===========  ===========
contains          Designation  0..n
realizes          Member       0..n
takesMeaningFrom  Category     0..n
================  ===========  ===========


contains
########
Representation(s), i.e. sign(s), of the related Category.




realizes
########
Class can play the role of a Member in a Collection




takesMeaningFrom
################
Category denoted by the Code. It providing meaning to the Node.






Graph
=====

.. graphviz:: /images/graph/Representations/Node.dot