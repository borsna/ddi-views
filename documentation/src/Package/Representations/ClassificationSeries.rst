.. _ClassificationSeries:


ClassificationSeries
********************
A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name (for example, ISIC or ISCO).



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
context                   StructuredString                   0..1
keywords                  ExternalControlledVocabularyEntry  0..n
name                      Name                               0..n
objectsOrUnitsClassified  ExternalControlledVocabularyEntry  0..1
owners                    AgentAssociation                   0..n
purpose                   StructuredString                   0..1
subjectAreas              ExternalControlledVocabularyEntry  0..n
type                      CollectionType                     0..1
========================  =================================  ===========


context
#######
ClassificationSeries can be designed in a specific context. 


keywords
########
A ClassificationSeries can be associated with one or a number of keywords. 


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


objectsOrUnitsClassified
########################
A ClassificationSeries is designed to classify a specific type of object/unit according to a specific attribute.


owners
######
The statistical office or other authority, which created and maintains the StatisticalClassification (s) related to the ClassificationSeries. A ClassificationSeries may have several owners.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


subjectAreas
############
Areas of statistics in which the ClassificationSeries is implemented.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========  =========================  ===========
Name      Type                       Cardinality
========  =========================  ===========
groups    StatisticalClassification  0..n
realizes  Collection                 0..n
========  =========================  ===========


groups
######
Realization of contains in Collection for StatisticalClassifications.




realizes
########
Class of the Collection pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationSeries.dot