.. _Map:


Map
***
Relationship between Nodes in NodeSets.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ==================  ===========
Name          Type                Cardinality
============  ==================  ===========
displayLabel  DisplayLabel        0..1
type          CorrespondenceType  0..1
usage         StructuredString    0..1
validDates    DateRange           0..1
============  ==================  ===========


displayLabel
############
A display label for the OrderedMemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


type
####
Type of correspondence in terms of commonalities and differences between two members.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.


validDates
##########
Date from which the Map became valid. The date must be defined if the Map belongs to a floating CorrespondenceTable. Date at which the Map became invalid. The date must be defined if the Map belongs to a floating Correspondence Table and is no longer valid.

`


Relationships
=============

========  ============  ===========
Name      Type          Cardinality
========  ============  ===========
realizes  OrderedTuple  0..n
source    Node          0..n
target    Node          0..n
========  ============  ===========


realizes
########
Class of the Collection pattern realized by this class. 




source
######
Realization of source in Ordered Tuple for Nodes.




target
######
Realization of target in Ordered Tuple for Nodes.






Graph
=====

.. graphviz:: /images/graph/Representations/Map.dot