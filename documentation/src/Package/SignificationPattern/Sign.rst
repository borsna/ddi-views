.. _Sign:


Sign
****
Something that suggests the presence or existence of a fact, condition, or quality.



Extends
=======
:ref:`Identifiable`


Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
representation  Signifier  1..1
==============  =========  ===========


representation
##############
A perceivable object used to denote a signified.

`


Relationships
=============

=======  =========  ===========
Name     Type       Cardinality
=======  =========  ===========
denotes  Signified  0..n
=======  =========  ===========


denotes
#######
Concept or object denoted by the signifier associated to the sign.






Graph
=====

.. graphviz:: /images/graph/SignificationPattern/Sign.dot