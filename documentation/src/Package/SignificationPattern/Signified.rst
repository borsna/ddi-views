.. _Signified:


Signified
*********
Concept or object denoted by the signifier associated to a sign.



Extends
=======
:ref:`Identifiable


Graph
=====

.. graphviz:: /images/graph/SignificationPattern/Signified.dot