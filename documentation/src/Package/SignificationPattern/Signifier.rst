.. _Signifier:


Signifier
*********
Concept whose extension includes perceivable objects. 

`


Properties
==========

=====  =====  ===========
Name   Type   Cardinality
=====  =====  ===========
token  Value  1..1
=====  =====  ===========


token
#####
Perceivable object in the extension of the signifier.




Graph
=====

.. graphviz:: /images/graph/SignificationPattern/Signifier.dot