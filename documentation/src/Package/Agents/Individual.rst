.. _Individual:


Individual
**********
A person who acts, or is designated to act towards a specific purpose.



Extends
=======
:ref:`Agent`


Properties
==========

==================  ==================  ===========
Name                Type                Cardinality
==================  ==================  ===========
contactInformation  ContactInformation  0..1
ddiId               String              0..n
individualName      IndividualName      0..n
==================  ==================  ===========


contactInformation
##################
Contact information for the individual including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.


ddiId
#####
The agency identifier of the individual according to the DDI Alliance agent registry.


individualName
##############
The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix.




Graph
=====

.. graphviz:: /images/graph/Agents/Individual.dot