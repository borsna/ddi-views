******
Agents
******

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Agent
   AgentBinary
   AgentHierarchy
   AgentHierarchyPair
   AgentListing
   AgentRegistry
   AgentRelationships
   AgentSimilarity
   AgentSimilarityPair
   Individual
   Machine
   Organization



Graph
=====

.. graphviz:: /images/graph/Agents/Agents.dot