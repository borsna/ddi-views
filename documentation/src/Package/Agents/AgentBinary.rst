.. _AgentBinary:


AgentBinary
***********
This abstract structure is used as the extension base for all Binary Relations between two agents. It allows new Binary Relations to be created without changing the collection Agent Relationships in order to accommodate them. As an extension base it provides a consistent set of properties to all Binary Relations used between Agents. 



Extends
=======
:ref:`Identifiable`


Properties
==========

==================  =================================  ===========
Name                Type                               Cardinality
==================  =================================  ===========
effectiveDates      DateRange                          0..1
privacy             ExternalControlledVocabularyEntry  0..1
purpose             StructuredString                   0..1
typeOfRelationship  ExternalControlledVocabularyEntry  0..1
==================  =================================  ===========


effectiveDates
##############
The effective start and end date of the relationship


privacy
#######
Define the level of privacy regarding this relationship. Supports the use of a controlled vocabulary.


purpose
#######
Explanation of the intent of creating the relation. Supports the use of multiple languages and structured text.


typeOfRelationship
##################
Define the specific nature of the relationship if not captured within the semantics of the specific relation type such as simple similarity or Parent/Child relationship. Supports the use of a controlled vocabulary




Graph
=====

.. graphviz:: /images/graph/Agents/AgentBinary.dot