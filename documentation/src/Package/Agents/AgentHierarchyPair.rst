.. _AgentHierarchyPair:


AgentHierarchyPair
******************
Used to define a pair of agents in a hierarchical structure where the source is the parent and the target is the child. 



Extends
=======
:ref:`Identifiable`


Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
effectiveDates  DateRange  0..1
==============  =========  ===========


effectiveDates
##############
The effective dates of the relation. A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.

`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
realizes  OrderedPair  0..n
source    Agent        0..n
target    Agent        0..n
========  ===========  ===========


realizes
########
Uses the pattern of an OrderedPair




source
######
The Parent agent in the hierarchical relationship




target
######
The Child agent in the hierarchical relationship






Graph
=====

.. graphviz:: /images/graph/Agents/AgentHierarchyPair.dot