.. _AgentRegistry:


AgentRegistry
*************
A collection of Agent Listings and/or AgentRelationships describing individual agents and their relationships to each other over time.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==========  ================  ===========
Name        Type              Cardinality
==========  ================  ===========
purpose     StructuredString  0..1
usage       StructuredString  0..1
validDates  DateRange         0..1
==========  ================  ===========


purpose
#######
Explanation of the intent of creating and maintaining this registry. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which the contents of the registry, Agent Listing and Agent Relationships can be employed. Supports the use of multiple languages and structured text.  


validDates
##########
A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date. 

`


Relationships
=============

================  ==================  ===========
Name              Type                Cardinality
================  ==================  ===========
hasListing        AgentListing        0..n
hasRelationships  AgentRelationships  0..n
realizes          Collection          0..n
================  ==================  ===========


hasListing
##########
Specialization of the contains relation in Collection. Identifies the specific Agent Listings defined in the Registry.




hasRelationships
################
Specialization of the contains relation in Collection. Identifies the specific Agent Relationships defined in the Registry.




realizes
########
Uses the pattern of Collection






Graph
=====

.. graphviz:: /images/graph/Agents/AgentRegistry.dot