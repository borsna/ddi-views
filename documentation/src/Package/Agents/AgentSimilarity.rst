.. _AgentSimilarity:


AgentSimilarity
***************
A specific type of Agent Binary relationship that is used when the intent is to express the similarity between a pair of agents. Uses the pattern for EquivalenceRelation. Each pair must contain like items. The relation of the members in the collection is characterized by the following assignments: Reflexivity=Reflexive; Symmetry=Semetric; Transitivity=Transitive.



Extends
=======
:ref:`AgentBinary`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Reflexive


semantics
#########
Controlled vocabulary for the equivalence relation semantics. It should contain, at least, the following: Same_As, Similar_To, Congruent_To, Compatible_With, Equidistant_From, Temporal_Equals.


symmetry
########
Fixed to Symmetric


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown


transitivity
############
Fixed to Transitive

`


Relationships
=============

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
contains    AgentSimilarityPair  0..n
realizes    EquivalenceRelation  0..n
structures  AgentListing         0..n
==========  ===================  ===========


contains
########
Unordered pairs of Agents




realizes
########
Reflects the pattern of EquivalenceRelation




structures
##########
AgentListing or Listings whose Agents are grouped by pairs to support a variety of structures.






Graph
=====

.. graphviz:: /images/graph/Agents/AgentSimilarity.dot