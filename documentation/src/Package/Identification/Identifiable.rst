.. _Identifiable:


Identifiable
************
Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned and provide administrative metadata properties. Use for First Order Classes whose content does not need to be discoverable in its own right but needs to be related to multiple classes. 

`


Properties
==========

=====================  =============  ===========
Name                   Type           Cardinality
=====================  =============  ===========
agency                 String         1..1
basedOnObject          BasedOnObject  0..1
id                     String         1..1
isPersistent           Boolean        1..1
isUniversallyUnique    Boolean        1..1
localId                LocalId        0..n
version                String         1..1
versionDate            IsoDate        0..1
versionRationale       String         0..1
versionResponsibility  String         0..1
=====================  =============  ===========


agency
######
This is the registered agency code with optional sub-agencies separated by dots. For example, diw.soep, ucl.qss, abs.essg.


basedOnObject
#############
The object/version that this object version is based on.


id
##
The ID of the object. This must conform to the allowed structure of the DDI Identifier and must be unique within the Agency.


isPersistent
############
Default value is false. Usually the content of the current version is allowed to change, for example as the contributor is working on the object contents. However, when isPersistent is true, it indicates the there will be no more changes to the current version.


isUniversallyUnique
###################
Default value is false. Usually the combination of agency and id (ignoring different versions) is unique. If isUniversallyUnique is set to true, it indicates that the id itself is universally unique (unique across systems and/or agencies) and therefore the agency part is not required to ensure uniqueness. 


localId
#######
This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id. For example, localId could be a variable name in a dataset. 


version
#######
The version number of the object. The version number is incremented whenever the non-administrative metadata contained by the object changes.


versionDate
###########
The date and time the object was changed. Supports standard ISO date and datetime formats.


versionRationale
################
The reason for making this version of the object.


versionResponsibility
#####################
Contributor who has the ownership and responsibility for the current version. 




Graph
=====

.. graphviz:: /images/graph/Identification/Identifiable.dot