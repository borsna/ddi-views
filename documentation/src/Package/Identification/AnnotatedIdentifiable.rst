.. _AnnotatedIdentifiable:


AnnotatedIdentifiable
*********************
Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned. Provides identification and administrative metadata about the object. Adds optional annotation. Use this as the extension base for First Order Classes that contain intellectual content that needs to be discoverable in its own right.



Extends
=======
:ref:`Identifiable`


Properties
==========

=============  ==========  ===========
Name           Type        Cardinality
=============  ==========  ===========
hasAnnotation  Annotation  0..1
=============  ==========  ===========


hasAnnotation
#############
Provides annotation information on the object to support citation and crediting of the creator(s) of the object.




Graph
=====

.. graphviz:: /images/graph/Identification/AnnotatedIdentifiable.dot