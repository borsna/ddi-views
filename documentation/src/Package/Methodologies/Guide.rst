.. _Guide:


Guide
*****
Provides a guide for the usage of a result within a specified application



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
isDiscussedIn  ExternalMaterial  0..n
=============  ================  ===========


isDiscussedIn
#############
Identifies material discussing the precondition. The material may be in DDI or other format.






Graph
=====

.. graphviz:: /images/graph/Methodologies/Guide.dot