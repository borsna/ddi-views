.. _Goal:


Goal
****
Goals are the "things" a method aims to achieve. A goal may be a business function (GSIM) corresponding to a function in a catalog of functions such as GSBPM or GLBMN. However, goals may be specified more broadly. For example, conducting a clinical trial might be the goal of a method. Machine learning might be the goal of a method. 





Extends
=======
:ref:`BusinessFunction`


Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
isDiscussedIn  ExternalMaterial  0..n
=============  ================  ===========


isDiscussedIn
#############
Identifies material discussing the goal. The material may be in DDI or other format.






Graph
=====

.. graphviz:: /images/graph/Methodologies/Goal.dot