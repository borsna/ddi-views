*************
Methodologies
*************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AppliedUse
   BusinessFunction
   Goal
   Guide
   Precondition
   Result



Graph
=====

.. graphviz:: /images/graph/Methodologies/Methodologies.dot