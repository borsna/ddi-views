.. _Precondition:


Precondition
************
A precondition is a state. The state includes one or more goals that were previously achieved. This state is the necessary condition before a process can begin.



Extends
=======
:ref:`BusinessFunction`


Relationships
=============

==================  ================  ===========
Name                Type              Cardinality
==================  ================  ===========
basedOnPriorResult  Result            0..n
isDiscussedIn       ExternalMaterial  0..n
==================  ================  ===========


basedOnPriorResult
##################
The Precondition may be based on the Results of earlier Processes, indicated with this relationship.




isDiscussedIn
#############
Identifies material discussing the precondition. The material may be in DDI or other format.






Graph
=====

.. graphviz:: /images/graph/Methodologies/Precondition.dot