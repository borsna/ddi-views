.. _Result:


Result
******
Describes the results of a process for the purpose of linking these results to guidance for future usage in specified situations. Result is abstract and serves as a substitution base for the specified result of a specific instantiation of a methodology process.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

===================  ===========  ===========
Name                 Type         Cardinality
===================  ===========  ===========
evaluateAgainstGoal  Goal         0..1
hasAppliedUse        AppliedUse   0..n
hasBinding           Binding      0..n
realizes             InputOutput  0..n
===================  ===========  ===========


evaluateAgainstGoal
###################
Comparison of Result of the Process against the Goal to inform further activities.




hasAppliedUse
#############
The result may have one or more specific usages which provides guidance for the use of the result with a specific unit type.




hasBinding
##########
Defines the binding of process outputs to applied usages of these results.




realizes
########
Result can serve as an input or output of a process






Graph
=====

.. graphviz:: /images/graph/Methodologies/Result.dot