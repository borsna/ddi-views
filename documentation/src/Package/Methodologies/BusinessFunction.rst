.. _BusinessFunction:


BusinessFunction
****************
Something an enterprise does, or needs to do, in order to achieve its objectives.

A Business Function delivers added value from a business point of view. It is delivered by bringing together people, processes and technology (resources), for a specific business purpose. 

Business Functions answer in a generic sense "What business purpose does this Business Service or Process Step serve?" Through identifying the Business Function associated with each Business Service or Process Step it increases the documentation of the use of the associated Business Services and Process Steps, to enable future reuse.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.




Graph
=====

.. graphviz:: /images/graph/Methodologies/BusinessFunction.dot