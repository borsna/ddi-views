.. _EventLayout:


EventLayout
***********
The EventLayout exists to permit the description of data stored in a "tall" dataset format. For event data typically each row in the dataset will contain a single data point's value, a unit identifier, a reference to a variable, a start time, and end time, and a datatype (that of the referenced variable).



Extends
=======
:ref:`PhysicalLayout


Graph
=====

.. graphviz:: /images/graph/FormatDescription/EventLayout.dot