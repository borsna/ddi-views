.. _CubeLayout:


CubeLayout
**********
The CubeLayout supports the description of the structure of aggregate data, and how it may be linked to the microdata of which it is a  cross-tabulation.



Extends
=======
:ref:`PhysicalLayout


Graph
=====

.. graphviz:: /images/graph/FormatDescription/CubeLayout.dot