.. _StructureDescription:


StructureDescription
********************
The information needed for understanding the physical structure of data coming from a file or other source. Multiple styles of structural description are supported: including describing files as unit-record (rectangular) files; describing cubes; and describing event-history (spell) data. A StructureDescription also points to the Data Store it physically represents. StructureDescription is reusable.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.

`


Relationships
=============

======================  ==============  ===========
Name                    Type            Cardinality
======================  ==============  ===========
containsPhysicalLayout  PhysicalLayout  0..n
formatsDataStore        DataStore       0..n
======================  ==============  ===========


containsPhysicalLayout
######################
Physical description (EventLayout, RectangularLayout, or CubeLayout) of the associated Logical Record Layout consisting of a Collection of Value Mappings describing the physical representation of each related Instance Variable.




formatsDataStore
################
Data Store physically represented by the Structure Description. 






Graph
=====

.. graphviz:: /images/graph/FormatDescription/StructureDescription.dot