.. _PhysicalLayoutOrder:


PhysicalLayoutOrder
*******************
A realization of OrderRelation that is used to describe the sequence of Value Mappings in a Physical Layout.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


reflexivity
###########
Fixed to Reflexive


semantics
#########
Controlled vocabulary for the order relation semantics. It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.


symmetry
########
Fixed to Anti_Symmetric


transitivity
############
Fixed to Transitive

`


Relationships
=============

==========  =========================  ===========
Name        Type                       Cardinality
==========  =========================  ===========
contains    PhysicalLayoutOrderedPair  0..n
realizes    OrderRelation              0..n
structures  PhysicalLayout             0..n
==========  =========================  ===========


contains
########
Physical layout ordered pairs describing the Value Mappings sequence.




realizes
########
Class of the Collection pattern realized by this class.




structures
##########
The Value Mappings in a Physical Layout to be sequenced.






Graph
=====

.. graphviz:: /images/graph/FormatDescription/PhysicalLayoutOrder.dot