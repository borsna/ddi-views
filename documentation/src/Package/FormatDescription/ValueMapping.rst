.. _ValueMapping:


ValueMapping
************
Provides physical characteristics for an InstanceVariable as part of a PhysicalLayout



Extends
=======
:ref:`Identifiable`


Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
decimalPositions            Integer                            0..1
defaultDecimalSeparator     String                             0..1
defaultDigitGroupSeparator  String                             0..1
defaultValue                String                             0..1
format                      ExternalControlledVocabularyEntry  0..1
length                      Integer                            0..1
maximumLength               Integer                            0..1
minimumLength               Integer                            0..1
nullSequence                String                             0..1
numberPattern               String                             0..1
physicalDatatype            ExternalControlledVocabularyEntry  0..1
required                    Boolean                            0..1
scale                       Integer                            0..1
==========================  =================================  ===========


decimalPositions
################
The number of decimal positions expressed as an integer. Used when the decimal position is implied (no decimal separator is present) See DDI 3.2 ManagedNumericRepresentation@decimalPositions


defaultDecimalSeparator
#######################
The string separating the integer part from the fractional part of a decimal or real number. In W3C part of the datatype format From https://www.w3.org/TR/tabular-metadata/ tabular 6.4.2 decimalChar: “A string whose value is used to represent a decimal point within the number. The default value is ".". If the supplied value is not a string, implementations MUST issue a warning and proceed as if the property had not been specified.” 


defaultDigitGroupSeparator
##########################
A string separating groups of digits (for readability). In W3C part of the datatype format From https://www.w3.org/TR/tabular-metadata/ tabular 6.4.2 groupChar: “A string whose value is used to group digits within the number. The default value is null. If the supplied value is not a string, implementations MUST issue a warning and proceed as if the property had not been specified.” 


defaultValue
############
A default string indicating the value to substitute for an empty string. From https://www.w3.org/TR/tabular-metadata/ Inherited 5.7  default “An atomic property holding a single string that is used to create a default value for the cell in cases where the original string value is an empty string. See Parsing Cells in [tabular-data-model] for more details. If not specified, the default for the default property is the empty string, "". The value of this property becomes the default annotation for the described column.”


format
######
This defines the format of the physical representation of the value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 format: “An atomic property that contains either a single string or an object that defines the format of a value of this type, used when parsing a string value as described in Parsing Cells in [tabular-data-model]. The value of this property becomes the format annotation for the described datatype.” See https://www.w3.org/TR/tabular-metadata/ Tabular 6.4.2 ‘Formats for numeric datatypes’  this may include decimalChar, groupChar, pattern  “By default, numeric values must be in the formats defined in [xmlschema11-2]. It is not uncommon for numbers within tabular data to be formatted for human consumption, which may involve using commas for decimal points, grouping digits in the number using commas, or adding percent signs to the number.” See https://www.w3.org/TR/tabular-metadata/ Tabular 6.4. Formats for Booleans   ” Boolean values may be represented in many ways aside from the standard 1 and 0 or true and false.” See https://www.w3.org/TR/tabular-metadata/ 6.4.4. Formats for dates and times “By default, dates and times are assumed to be in the format defined in [xmlschema11-2]. However dates and times are commonly represented in tabular data in other formats.” See https://www.w3.org/TR/tabular-metadata/ 6.4.5 Formats for durations “Durations MUST be formatted and interpreted as defined in [xmlschema11-2], using the [ISO8601] format -?PnYnMnDTnHnMnS. For example, the duration P1Y1D is used for a year and a day; the duration PT2H30M for 2 hours and 30 minutes.” See  https://www.w3.org/TR/tabular-metadata/ 6.4.6 Formats for other types “If the datatype base is not numeric, boolean, a date/time type, or a duration type, the datatype format annotation provides a regular expression for the string values, with syntax and processing defined by [ECMASCRIPT]. If the supplied value is not a valid regular expression, implementations MUST issue a warning and proceed as if no format had been provided.” From DDI3.2 ManagedNumericRepresentation@format “A format for number expressed as a string.” From DDI3.2 ManagedDateTimeRepresentation_DateFieldFormat “Describes the format of the date field, in formats such as YYYY/MM or MM-DD-YY, etc. If this element is omitted, then the format is assumed to be the XML Schema format corresponding to the type attribute value.” 


length
######
The length of the physical representation of the value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 length: “A numeric atomic property that contains a single integer that is the exact length of the value. The value of this property becomes the length annotation for the described datatype. See Length Constraints in [tabular-data-model] for details.” Corresponds to DDI2.5 var/location/width and DDI 3.2 PhysicalLocation/Width


maximumLength
#############
The largest possible value of the length of the physical representation of the value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 maxLength: “A numeric atomic property that contains a single integer that is the maximum length of the value. The value of this property becomes the maximum length annotation for the described datatype. See Length Constraints in [tabular-data-model] for details.”


minimumLength
#############
The smallest possible value for the length of the physical representation of the value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 minLength: “An atomic property that contains a single integer that is the minimum length of the value. The value of this property becomes the minimum length annotation for the described datatype. See Length Constraints in [tabular-data-model] for details.”


nullSequence
############
A string indicating a null value. From https://www.w3.org/TR/tabular-metadata/ 4.3 null — “the string or strings which cause the value of cells having string value matching any of these values to be null.” From Inherited 5.7 null: “An atomic property giving the string or strings used for null values within the data. If the string value of the cell is equal to any one of these values, the cell value is null. See Parsing Cells in [tabular-data-model] for more details. If not specified, the default for the null property is the empty string "". The value of this property becomes the null annotation for the described column.” 


numberPattern
#############
A pattern description of the format of a numeric value. In W3C part of the datatype format From https://www.w3.org/TR/tabular-metadata/ tabular 6.4.2 pattern: “A number format pattern as defined in [UAX35] http://www.unicode.org/reports/tr35/tr35-31/tr35-numbers.html#Number_Format_Patterns . Implementations MUST recognise number format patterns containing the symbols 0, #, the specified decimalChar (or "." if unspecified), the specified groupChar (or "," if unspecified), E, +, % and ‰. Implementations MAY additionally recognise number format patterns containing other special pattern characters defined in [UAX35]. If the supplied value is not a string, or if it contains an invalid number format pattern or uses special pattern characters that the implementation does not recognise, implementations MUST issue a warning and proceed as if the property had not been specified.   f the datatype format annotation is a single string, this is interpreted in the same way as if it were an object with a pattern property whose value is that string. If the groupChar is specified, but no pattern is supplied, when parsing the string value of a cell against this format specification, implementations MUST recognise and parse numbers that consist of:  an optional + or - sign, …  Implementations MAY also recognise numeric values that are in any of the standard-decimal, standard-percent or standard-scientific formats listed in the Unicode Common Locale Data Repository. …” 


physicalDatatype
################
The base datatype of the physical representation. An integer InstanceVariable might, for example, be stored as a floating point number. From https://www.w3.org/TR/tabular-metadata/ Inherited 5.7 datatype: “An atomic property that contains either a single string that is the main datatype of the values of the cell or a datatype description object. If the value of this property is a string, it MUST be the name of one of the built-in datatypes defined in section 5.11.1 Built-in Datatypes and this value is normalized to an object whose base property is the original string value. If it is an object then it describes a more specialized datatype. If a cell contains a sequence (i.e. the separator property is specified and not null) then this property specifies the datatype of each value within that sequence. See 5.11 Datatypes and Parsing Cells in [tabular-data-model] for more details.  The normalized value of this property becomes the datatype annotation for the described column. ” 


required
########
If True a value is required for this variable. NOTE: this might be better at the InstanceVariable or higher in the variable cascade. From https://www.w3.org/TR/tabular-metadata/ Inherited 5.7 required: “A boolean atomic property taking a single value which indicates whether the cell value can be null. See Parsing Cells in [tabular-data-model] for more details. The default is false, which means cells can have null values. The value of this property becomes the required annotation for the described column.”


scale
#####
The scale of the number expressed as an integer (for example a number expressed in 100's, 5 = 500 would have a scale of 100).   From DDI 3.2 ManagedNumericRepresentation@scale: 

`


Relationships
=============

========================  =======================  ===========
Name                      Type                     Cardinality
========================  =======================  ===========
describesSegmentLocation  PhysicalSegmentLocation  0..n
formatsInstanceVariable   InstanceVariable         0..n
realizes                  Member                   0..n
========================  =======================  ===========


describesSegmentLocation
########################
Uses a PhysicalSegmentLocation to describe where in the physical record a segment representing the InstanceVarible is. This could be, for example, described as a start position and end position value for characters in a text record via the SegmentByText extension of PhysicalSegmentLocation.




formatsInstanceVariable
#######################
Describes the physical representation of the InstanceVariable 




realizes
########
ValueMappings are members of the PhysicalLayout. 






Graph
=====

.. graphviz:: /images/graph/FormatDescription/ValueMapping.dot