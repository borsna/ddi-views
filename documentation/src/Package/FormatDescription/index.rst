*****************
FormatDescription
*****************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   TableDirectionValues
   TextDirectionValues
   TrimValues
   CubeLayout
   EventLayout
   PhysicalLayout
   PhysicalLayoutOrder
   PhysicalLayoutOrderedPair
   RectangularLayout
   StructureDescription
   ValueMapping



Graph
=====

.. graphviz:: /images/graph/FormatDescription/FormatDescription.dot