.. _InputParameter:


InputParameter
**************
Generic container for an input instance to a Process Step. 



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

====  =================================  ===========
Name  Type                               Cardinality
====  =================================  ===========
type  ExternalControlledVocabularyEntry  0..n
====  =================================  ===========


type
####
Type of object the parameter accepts (DDI object, Datatype, etc.). If not present the parameter is untyped.

`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
realizes  InputOutput  0..n
========  ===========  ===========


realizes
########
Class in the Process Pattern to be realized by Input Parameter.






Graph
=====

.. graphviz:: /images/graph/Workflows/InputParameter.dot