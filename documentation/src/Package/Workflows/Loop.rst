.. _Loop:


Loop
****
Iterative control structure to be repeated a specified number of times based on one or more conditions. Inside the loop, one or more Workflow Steps are evaluated and processed in the order they appear.



Extends
=======
:ref:`ConditionalControlConstruct`


Properties
==========

============  ===========  ===========
Name          Type         Cardinality
============  ===========  ===========
initialValue  CommandCode  0..1
stepValue     CommandCode  0..1
============  ===========  ===========


initialValue
############
The command used to set the initial value for the process. Could be a simple value.


stepValue
#########
The command used to set the incremental or step value for the process. Could be a simple value.




Graph
=====

.. graphviz:: /images/graph/Workflows/Loop.dot