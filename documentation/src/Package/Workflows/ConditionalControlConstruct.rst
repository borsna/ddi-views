.. _ConditionalControlConstruct:


ConditionalControlConstruct
***************************
Type of Control Construct in which the execution flow is determined by one or more conditions.




Extends
=======
:ref:`ControlConstruct`


Properties
==========

=========  ===========  ===========
Name       Type         Cardinality
=========  ===========  ===========
condition  CommandCode  0..1
=========  ===========  ===========


condition
#########
Condition to be evaluated to determine whether or not to execute the Workflow Sequence in contains. It can be an expression and/or programmatic code. The specialized sub-classes determine whether the Sequence is executed when the condition is true or false

`


Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
contains  WorkflowSequence  0..1
========  ================  ===========


contains
########
The Workflow Sequence is executed depending on the result of the condition evaluation. The specialized sub-classes determine whether the Sequence is executed when the condition is true or false. Specialization of contains in Control Construct.






Graph
=====

.. graphviz:: /images/graph/Workflows/ConditionalControlConstruct.dot