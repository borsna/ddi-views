.. _WorkflowService:


WorkflowService
***************
A means of performing a Workflow Step as part of a concrete implementation of a Business Function  (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve).



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
estimatedDuration  Date                               0..1
interface          ExternalControlledVocabularyEntry  0..1
location           ExternalControlledVocabularyEntry  0..1
=================  =================================  ===========


estimatedDuration
#################
The estimated time period associated with the operation of the Service. This may be expressed as a time, date-time, or duration.


interface
#########
Specifies how to communicate with the service.


location
########
Specifies where the service can be accessed.

`


Relationships
=============

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
hasAgent  Agent    0..n
realizes  Service  0..n
========  =======  ===========


hasAgent
########
Actor that performs a role in the service,




realizes
########
Class in the Process Pattern realized by Workflow Service.






Graph
=====

.. graphviz:: /images/graph/Workflows/WorkflowService.dot