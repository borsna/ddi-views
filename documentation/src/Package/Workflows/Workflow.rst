.. _Workflow:


Workflow
********
A Workflow is an realized Process which identifies the WorkflowSequence which contains the WorkflowSteps and their order. Describes the design, preconditions, results, algorithm, and workflow sequence of a workflow. This is an abstract class which is instantiated for specific processes.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
overview  StructuredString  0..1
========  ================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

`


Relationships
=============

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
contains         WorkflowSequence  0..n
hasAlgorithm     Algorithm         0..n
hasDesign        Design            0..1
hasPrecondition  Precondition      0..n
hasResults       Result            0..n
isDiscussedIn    ExternalMaterial  0..n
realizes         Process           0..n
===============  ================  ===========


contains
########
Restriction of contains in Process to identify the Workflow Sequence




hasAlgorithm
############
An implementation and/or execution of an algorithm which is associated with the design of the process.




hasDesign
#########
The design informs the specific process by providing the underlying rationale, rules and general preconditions for the process. The design is the reusable part of the implemented process.




hasPrecondition
###############
Any condition that must be met prior to the process being implemented.




hasResults
##########
The end result of the process which can contain a binding to specific outcome collections as well as usage instructions for implementing results




isDiscussedIn
#############
Identifies material discussing the process. The material may be in DDI or other format.




realizes
########
Can play the role of a Process in a Workflow






Graph
=====

.. graphviz:: /images/graph/Workflows/Workflow.dot