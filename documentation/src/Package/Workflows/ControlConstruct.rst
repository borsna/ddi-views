.. _ControlConstruct:


ControlConstruct
****************
A Workflow Step that controls the execution flow of the Workflow by determining the next Workflow Step in its scope.



Extends
=======
:ref:`WorkflowStep`


Relationships
=============

========  ==================  ===========
Name      Type                Cardinality
========  ==================  ===========
contains  WorkflowStep        0..n
defines   Binding             0..n
realizes  ProcessControlStep  0..n
========  ==================  ===========


contains
########
Workflow Steps in scope of the Control Construct. Realization of contains in Process Control Step.




defines
#######
Mappings between Inputs and Outputs of Workflow Steps in scope of the Control Construct. Realization of defines in Process Step.




realizes
########
Class in the Process Pattern realized by Control Construct.






Graph
=====

.. graphviz:: /images/graph/Workflows/ControlConstruct.dot