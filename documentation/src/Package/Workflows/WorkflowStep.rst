.. _WorkflowStep:


WorkflowStep
************
One of the constituents of a Workflow. It can be a composition or atomic and might be performed by a Service.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ================  ===========
Name          Type              Cardinality
============  ================  ===========
displayLabel  DisplayLabel      0..n
name          Name              0..n
overview      StructuredString  0..1
purpose       StructuredString  0..1
usage         StructuredString  0..n
============  ================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
hasParameters        Parameters        0..n
hasProcessFramework  ExternalMaterial  0..n
isPerformedBy        WorkflowService   0..n
realizes             ProcessStep       0..n
===================  ================  ===========


hasParameters
#############
Input and Output Parameters of the Workflow Step. Specialization of hasInterface in Process Step for Parameters of Workflow Steps.




hasProcessFramework
###################
Reference to a standard process framework or step within the process famework such as GSBPM




isPerformedBy
#############
Identifies the Service Implementation which performs the Workflow Step. Specialization of isPerformedBy in Process Step for Service Implementations.




realizes
########
Class in the ProcessPattern realized by WorkflowStep.






Graph
=====

.. graphviz:: /images/graph/Workflows/WorkflowStep.dot