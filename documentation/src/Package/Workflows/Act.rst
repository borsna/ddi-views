.. _Act:


Act
***
An Act is an indivisible, atomic step, i.e. not composed of other steps. An Act can also be viewed as a terminal node in a hierarchy of Workflow Steps. 



Extends
=======
:ref:`WorkflowStep`


Properties
==========

====================  ===========  ===========
Name                  Type         Cardinality
====================  ===========  ===========
instructionalCommand  CommandCode  0..1
====================  ===========  ===========


instructionalCommand
####################
Optionally an act has an instructionalCommand expressed as Command Code. Acts are abstract so, in addition to an instructionalCommand, we can expect other specializations or instructions to be provided by the class which extends it.




Graph
=====

.. graphviz:: /images/graph/Workflows/Act.dot