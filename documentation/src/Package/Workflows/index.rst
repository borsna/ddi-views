*********
Workflows
*********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Act
   Binding
   ConditionalControlConstruct
   ControlConstruct
   ElseIf
   IfThenElse
   InputParameter
   Loop
   OutputParameter
   Parameters
   RepeatUntil
   RepeatWhile
   Workflow
   WorkflowSequence
   WorkflowService
   WorkflowStep



Graph
=====

.. graphviz:: /images/graph/Workflows/Workflows.dot