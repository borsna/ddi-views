.. _Binding:


Binding
*******
Mapping between Input and Output Parameters of Workflow Steps representing the flow of information within a Workflow.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ==================  ===========
Name          Type                Cardinality
============  ==================  ===========
displayLabel  DisplayLabel        0..n
type          CorrespondenceType  0..1
============  ==================  ===========


displayLabel
############
A display label for the MemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


type
####
Type of correspondence in terms of commonalities and differences between two members.

`


Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
input     InputParameter   0..n
ouput     OutputParameter  0..n
realizes  InformationFlow  0..n
========  ===============  ===========


input
#####
Specialization of maps in Information Flow for Input Parameters.




ouput
#####
Specialization of maps in Information Flow for Output Parameters.




realizes
########
Class in the Process Pattern realized by Binding.






Graph
=====

.. graphviz:: /images/graph/Workflows/Binding.dot