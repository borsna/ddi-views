*********
Discovery
*********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access
   BoundingBox
   CatalogOfItems
   CollectionItem
   Coverage
   SeriesStatement
   SpatialCoverage
   TemporalCoverage
   TopicalCoverage



Graph
=====

.. graphviz:: /images/graph/Discovery/Discovery.dot