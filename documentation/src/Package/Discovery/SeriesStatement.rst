.. _SeriesStatement:


SeriesStatement
***************
Describes a series by providing citation information and coverage information. 



Extends
=======
:ref:`Identifiable`


Properties
==========

================  ==========  ===========
Name              Type        Cardinality
================  ==========  ===========
citationOfSeries  Annotation  0..n
================  ==========  ===========


citationOfSeries
################
Bibliographic citation of the Series

`


Relationships
=============

================  ========  ===========
Name              Type      Cardinality
================  ========  ===========
coverageOfSeries  Coverage  0..n
================  ========  ===========


coverageOfSeries
################
Temporal, topical, and spatial coverage of the series






Graph
=====

.. graphviz:: /images/graph/Discovery/SeriesStatement.dot