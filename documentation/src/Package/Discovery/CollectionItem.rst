.. _CollectionItem:


CollectionItem
**************
A collection item is designed to provide a bibliographic, coverage and related description of an item in a collection.



Extends
=======
:ref:`AnnotatedIdentifiable`


Relationships
=============

===============  ==================  ===========
Name             Type                Cardinality
===============  ==================  ===========
belongsToSeries  SeriesStatement     0..n
describesAccess  Access              0..n
hasCoverage      Coverage            0..n
hasFunding       FundingInformation  0..n
realizes         Member              0..n
===============  ==================  ===========


belongsToSeries
###############
Collection item belongs to this series




describesAccess
###############
Information on how to access the collection item




hasCoverage
###########
Describes the temporal, topical, and spatial coverof the colleciton item




hasFunding
##########
Information concerning the funding source for the colleciton item




realizes
########
Collection Item can act as the member of a collection






Graph
=====

.. graphviz:: /images/graph/Discovery/CollectionItem.dot