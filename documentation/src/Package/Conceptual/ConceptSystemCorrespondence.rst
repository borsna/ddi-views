.. _ConceptSystemCorrespondence:


ConceptSystemCorrespondence
***************************
Relationship between Concept Systems used to group similarity mappings between their Concepts.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ================  ===========
Name          Type              Cardinality
============  ================  ===========
displayLabel  DisplayLabel      0..n
purpose       StructuredString  0..1
usage         StructuredString  0..1
============  ================  ===========


displayLabel
############
A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

========  =================  ===========
Name      Type               Cardinality
========  =================  ===========
contains  SimilarConcept     1..n
maps      ConceptSystem      0..n
realizes  SymmetricRelation  0..n
========  =================  ===========


contains
########
Realization of contains in Symmetric Relation for mapping similar concepts.




maps
####
Realization of structures in Symmetric Relation. When Concepts of a single ConceptSystem are mapped, the Concept has to appear twice as target.




realizes
########
Class in the Collections Pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptSystemCorrespondence.dot