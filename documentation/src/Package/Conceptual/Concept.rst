.. _Concept:


Concept
*******
Unit of thought differentiated by characteristics [GSIM 1.1]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==========  ================  ===========
Name        Type              Cardinality
==========  ================  ===========
definition  StructuredString  0..1
name        Name              0..n
==========  ================  ===========


definition
##########
Natural language statement conveying the meaning of a concept, differentiating it from other concepts. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

========  =========  ===========
Name      Type       Cardinality
========  =========  ===========
realizes  Signified  0..n
========  =========  ===========


realizes
########
A concept can play the role of a signified when there is a designation that denotes it.






Graph
=====

.. graphviz:: /images/graph/Conceptual/Concept.dot