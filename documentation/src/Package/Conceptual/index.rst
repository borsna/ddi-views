**********
Conceptual
**********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Category
   Concept
   ConceptParentChild
   ConceptParentChildPair
   ConceptPartWhole
   ConceptPartWholePair
   ConceptSystem
   ConceptSystemCorrespondence
   ConceptualVariable
   InstanceVariable
   Population
   RepresentedVariable
   SentinelConceptualDomain
   SimilarConcept
   SubstantiveConceptualDomain
   Unit
   UnitType
   Universe



Graph
=====

.. graphviz:: /images/graph/Conceptual/Conceptual.dot