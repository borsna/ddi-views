.. _ConceptPartWholePair:


ConceptPartWholePair
********************
Identifies the concept defining the Whole and the concept defining the Part.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
part      Concept      0..n
realizes  OrderedPair  0..n
whole     Concept      0..n
========  ===========  ===========


part
####
Specialization of "target" in OrderedPair




realizes
########
realizes pattern OrderedPair




whole
#####
Specialization of "source" in OrderedPair.






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptPartWholePair.dot