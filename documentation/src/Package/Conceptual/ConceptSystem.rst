.. _ConceptSystem:


ConceptSystem
*************
A set of Concepts structured by the relations among them. [GSIM 1.1]



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

=====================  ==================  ===========
Name                   Type                Cardinality
=====================  ==================  ===========
contains               Concept             0..n
hasConceptParentChild  ConceptParentChild  1..n
hasConceptPartWhole    ConceptPartWhole    1..n
realizes               Collection          0..n
=====================  ==================  ===========


contains
########
The relationship to the concepts contained in the concept system




hasConceptParentChild
#####################
Specialization of isOrderedBy in Collection.




hasConceptPartWhole
###################
Specialization of isOrderedBy in Collection.




realizes
########
realizes the collection pattern






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptSystem.dot