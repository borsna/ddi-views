.. _ConceptParentChildPair:


ConceptParentChildPair
**********************
Specifies the Parent Concept and Child Concept in the Concept Parent Child Relationship.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
child     Concept      0..n
parent    Concept      0..n
realizes  OrderedPair  0..n
========  ===========  ===========


child
#####
Specialization of "target" in OrderedPair




parent
######
Specialization of "source" in OrderedPair




realizes
########
realizes the pattern OrderedPair






Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptParentChildPair.dot