.. _SimilarConcept:


SimilarConcept
**************
A reference to a concept with similar meaning and a description of their differences. The similar concept structure allows specification of similar concepts to address cases where confusion may affect the appropriate use of the concept.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  ==================  ===========
Name          Type                Cardinality
============  ==================  ===========
displayLabel  DisplayLabel        0..n
type          CorrespondenceType  0..1
usage         StructuredString    0..1
============  ==================  ===========


displayLabel
############
A display label for the MemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


type
####
Type of correspondence in terms of commonalities and differences between two members.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
maps      Concept         0..n
realizes  UnorderedTuple  0..n
========  ==============  ===========


maps
####
Realization of maps in Unordered Tuple for Concepts that are similar. Used to assist in disambiguation of concepts.




realizes
########
Class in the Collections Pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/Conceptual/SimilarConcept.dot