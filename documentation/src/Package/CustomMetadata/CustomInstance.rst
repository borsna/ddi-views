.. _CustomInstance:


CustomInstance
**************
A set of CustomValues to be attached to some object.



Extends
=======
:ref:`Identifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
see Collection


purpose
#######
see Collection


type
####
see Collection

`


Relationships
=============

====================  =======================  ===========
Name                  Type                     Cardinality
====================  =======================  ===========
contains              CustomValue              1..1
correspondsTo         CustomStructure          0..n
hasParentChildOrder   CustomValueParentChild   1..n
hasRelationshipOrder  CustomValueRelationship  1..n
hasSequenceOrder      CustomValueSequence      1..n
realizes              Collection               0..n
====================  =======================  ===========


contains
########
the set of CustomItems grouped.




correspondsTo
#############
The Set of CustomItems allowed.




hasParentChildOrder
###################
Associates a specification of a parent-child ordering among the CustomValues in this structure




hasRelationshipOrder
####################
Associates a specification of a source target predicate ordering among the CustomValues in this structure




hasSequenceOrder
################
Associates a specification of a sequential ordering among the CustomValues in this structure




realizes
########
This is a collection of key-value pairs






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomInstance.dot