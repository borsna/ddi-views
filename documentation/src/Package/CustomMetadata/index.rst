**************
CustomMetadata
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   ControlledVocabulary
   CustomInstance
   CustomItem
   CustomItemParentChild
   CustomItemParentChildPair
   CustomItemRelationship
   CustomItemRelationshipPair
   CustomItemSequence
   CustomItemSequencePair
   CustomStructure
   CustomValue
   CustomValueParentChild
   CustomValueParentChildPair
   CustomValueRelationship
   CustomValueRelationshipPair
   CustomValueSequence
   CustomValueSequencePair
   VocabularyEntry
   VocabularyParentChild
   VocabularyParentChildPair
   VocabularySequence
   VocabularySequencePair



Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomMetadata.dot