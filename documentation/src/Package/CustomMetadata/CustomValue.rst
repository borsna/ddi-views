.. _CustomValue:


CustomValue
***********
An instance of a  key, value pair for a  particular  key.



Extends
=======
:ref:`Identifiable`


Properties
==========

=====  ======  ===========
Name   Type    Cardinality
=====  ======  ===========
key    String  0..1
value  Value   0..1
=====  ======  ===========


key
###
The unique String identifying the value


value
#####
the value associated with a particular key expressed as a string. The ultimate value representation is defined by the corresponding CustomValue.

`


Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
correspondsTo  CustomItem        0..n
relatesTo      InstanceVariable  0..n
=============  ================  ===========


correspondsTo
#############
The CustomItem which defines the key for this key,value pair, It also defines a ValueRepresentation




relatesTo
#########
This optional relationship indicates an InstanceVariable comparable to this key. Custom metadata might be reused as data and data might be used as metadata. This mechanism could enable these transformations. The related InstanceVariable could also carry information about units of measurement, missing values etc.






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomValue.dot