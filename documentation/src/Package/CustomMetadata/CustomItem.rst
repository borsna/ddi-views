.. _CustomItem:


CustomItem
**********
A custom item description. This allows the definition of an item which is a member of a CustomStructure.
It defines the minimum and maximum number of occurrences and representation of a CustomValue.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=========  =======  ===========
Name       Type     Cardinality
=========  =======  ===========
key        String   0..1
maxOccurs  Integer  0..1
minOccurs  Integer  0..1
=========  =======  ===========


key
###
The key to be used by a key,value pair


maxOccurs
#########
The maximum number of occurrences of an associated CustomValue


minOccurs
#########
The minimum number of occurrences of an associated CustomValue

`


Relationships
=============

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
hasConcept  Concept              0..n
realizes    Member               0..n
relatesTo   RepresentedVariable  0..n
uses        ValueDomain          0..n
==========  ===================  ===========


hasConcept
##########
A Concept associated with the key of a CustomValue




realizes
########
Class can play the role of a Member in a Collection




relatesTo
#########
This optional relationship indicates a RepresentedVariable comparable to this key. Custom metadata might be reused as data and data might be used as metadata. This mechanism could enable these transformations.




uses
####
The type of value for the key.value pair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomItem.dot