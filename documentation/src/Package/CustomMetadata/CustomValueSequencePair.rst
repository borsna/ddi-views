.. _CustomValueSequencePair:


CustomValueSequencePair
***********************
Defines a simple ordering between two CustomValuess - one precedes the other.



Extends
=======
:ref:`Identifiable`


Relationships
=============

===========  ===========  ===========
Name         Type         Cardinality
===========  ===========  ===========
predecessor  CustomValue  0..n
realizes     OrderedPair  0..n
successor    CustomValue  0..n
===========  ===========  ===========


predecessor
###########
The key,value pair which comes first. Specialization of 'source' in OrderedPair




realizes
########
realizes pattern OrderedPair




successor
#########
The key,value pair which follows. Specialization of 'target' in OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomValueSequencePair.dot