.. _CustomStructure:


CustomStructure
***************
A Collection containing a set of item descriptions defining a structure for a set of key,value pairs



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

====================  ======================  ===========
Name                  Type                    Cardinality
====================  ======================  ===========
contains              CustomItem              0..n
hasParentChildOrder   CustomItemParentChild   1..n
hasRelationshipOrder  CustomItemRelationship  1..n
hasSequenceOrder      CustomItemSequence      1..n
realizes              Collection              0..n
====================  ======================  ===========


contains
########
A CustomStructure contains a set of CustomItems. This defines a particular set of key,value pairs that are usable in a particular application.




hasParentChildOrder
###################
Associates a specification of a parent-child ordering among the CustomItems in this structure




hasRelationshipOrder
####################
Associates a specification of a source target predicate ordering among the CustomItems in this structure




hasSequenceOrder
################
Associates a specification of a sequential ordering among the CustomItems in this structure




realizes
########
realizes a collection pattern






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomStructure.dot