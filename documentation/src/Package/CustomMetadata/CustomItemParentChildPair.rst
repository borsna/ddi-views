.. _CustomItemParentChildPair:


CustomItemParentChildPair
*************************
A parent-child relationship between a pair of CustomItems.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
child     CustomItem   0..n
parent    CustomItem   0..n
realizes  OrderedPair  0..n
========  ===========  ===========


child
#####
The child in the pair. This item descends from the parent in some sense. Specialization of "target" in OrderedPair




parent
######
The parent in the pair. Specialization of "source" in OrderedPair




realizes
########
realizes pattern OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomItemParentChildPair.dot