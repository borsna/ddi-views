.. _VocabularyParentChildPair:


VocabularyParentChildPair
*************************
a parent-child relationship between two vocabularyEntries



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
child     VocabularyEntry  0..n
parent    VocabularyEntry  0..n
realizes  OrderedPair      0..n
========  ===============  ===========


child
#####
the entry descending from the parent, the child. Specializes 'target' in OrderedPair.




parent
######
The entry from which the child entry descends, the parent. Specializes 'source' in OrderedPair.




realizes
########
realizes pattern OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/VocabularyParentChildPair.dot