.. _ControlledVocabulary:


ControlledVocabulary
********************
The specification of a controlled vocabulary defines a set of values and their definitions together with the order relationships among those entries.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

=======  ================  ===========
Name     Type              Cardinality
=======  ================  ===========
name     Name              0..n
purpose  StructuredString  0..1
type     CollectionType    0..1
=======  ================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.

`


Relationships
=============

========================  =====================  ===========
Name                      Type                   Cardinality
========================  =====================  ===========
contains                  VocabularyEntry        1..n
hasVocabularyParentChild  VocabularyParentChild  1..n
hasVocabularySequence     VocabularySequence     1..n
realizes                  Collection             0..n
========================  =====================  ===========


contains
########
The entries in the vocabulary.




hasVocabularyParentChild
########################
The OrderRelation defining the hierarchy in the vocabulary




hasVocabularySequence
#####################
The OrderRelation defining the sequential order in the vocabulary




realizes
########
realizes the collection pattern






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/ControlledVocabulary.dot