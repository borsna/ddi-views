.. _CustomItemSequencePair:


CustomItemSequencePair
**********************
Defines a simple ordering between two CustomItems - one precedes the other.



Extends
=======
:ref:`Identifiable`


Relationships
=============

===========  ===========  ===========
Name         Type         Cardinality
===========  ===========  ===========
predecessor  CustomItem   0..n
realizes     OrderedPair  0..n
successor    CustomItem   0..n
===========  ===========  ===========


predecessor
###########
The CustomItem coming first in the order. Specializes source in OrderedPair




realizes
########
realizes pattern OrderedPair




successor
#########
The CustomItem coming second in the order. Specializes target in OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomItemSequencePair.dot