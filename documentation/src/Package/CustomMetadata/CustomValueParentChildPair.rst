.. _CustomValueParentChildPair:


CustomValueParentChildPair
**************************
A parent-child relationship between a pair of CustomValues.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
child     CustomValue  0..n
parent    CustomValue  0..n
realizes  OrderedPair  0..n
========  ===========  ===========


child
#####
The child in the pair. This item descends from the parent in some sense. Specializes 'target' in OrderedPair




parent
######
The Parent in the pair. Specializes 'source' in OrderedPair




realizes
########
realizes pattern OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomValueParentChildPair.dot