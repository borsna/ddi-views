.. _CustomItemParentChild:


CustomItemParentChild
*********************
Contains the set of CustomItemParentChildPairs that define the parent child relationships in a CustomStructure.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
criteria      StructuredString                   0..1
displayLabel  DisplayLabel                       0..n
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  0..1
symmetry      SymmetryType                       1..1
totality      TotalityType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


criteria
########
Intentional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


displayLabel
############
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Controlled vocabulary for the order relation semantics. It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.


symmetry
########
Fixed to Anti_Symmetric


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


transitivity
############
Fixed to Anti-Transitive


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  ===========================  ===========
Name        Type                         Cardinality
==========  ===========================  ===========
contains    CustomItemParentChildPair    0..n
realizes    ImmediatePrecedenceRelation  0..n
structures  CustomStructure              0..n
==========  ===========================  ===========


contains
########
Each CustomItemParentChipdPair defines the ordering between two CustomItems. 




realizes
########
realizes the pattern ImmediatePrecedenceRelation. (An object is not a parent to a grandchild)




structures
##########
Collection whose Members are grouped by pairs to support a variety of structures.






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomItemParentChild.dot