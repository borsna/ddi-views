.. _VocabularySequencePair:


VocabularySequencePair
**********************
Defines a simple ordering between two VocabularyEntry(s) one precedes the other.



Extends
=======
:ref:`Identifiable`


Relationships
=============

===========  ===============  ===========
Name         Type             Cardinality
===========  ===============  ===========
predecessor  VocabularyEntry  0..n
realizes     OrderedPair      0..n
successor    VocabularyEntry  0..n
===========  ===============  ===========


predecessor
###########
The vocabulary entry coming first in the order. Specialization of 'source' in OrderedPair




realizes
########
realizes pattern OrderedPair




successor
#########
The vocabulary entry coming second in the order. Specialization of 'target' in OrderedPair






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/VocabularySequencePair.dot