.. _CustomValueRelationshipPair:


CustomValueRelationshipPair
***************************
Defines an 'order' relationship with a predicate between two CustomValues (a triple)



Extends
=======
:ref:`Identifiable`


Properties
==========

=========  ======  ===========
Name       Type    Cardinality
=========  ======  ===========
predicate  String  0..1
=========  ======  ===========


predicate
#########
The type of relationship between the two CustomValues. The verb in the sentence describing the relationship between the source (subject) and the target (object).

`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
realizes  OrderedPair  0..n
source    CustomValue  0..n
target    CustomValue  0..n
========  ===========  ===========


realizes
########
realizes pattern OrderedPair




source
######
The source (subject) of the relationship




target
######
The target (object) of the relationship defined by the predicate (verb)






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomValueRelationshipPair.dot