.. _VocabularyEntry:


VocabularyEntry
***************
One value and its definition in an ordered list comprising a controlled vocabulary.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==========  ================  ===========
Name        Type              Cardinality
==========  ================  ===========
definition  StructuredString  0..1
value       StructuredString  0..1
==========  ================  ===========


definition
##########
An explanation (definition) of the value


value
#####
the term being defined

`


Relationships
=============

========  ======  ===========
Name      Type    Cardinality
========  ======  ===========
realizes  Member  0..n
========  ======  ===========


realizes
########
Class can play the role of a Member in a Collection






Graph
=====

.. graphviz:: /images/graph/CustomMetadata/VocabularyEntry.dot