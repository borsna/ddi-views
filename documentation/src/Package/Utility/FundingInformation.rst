.. _FundingInformation:


FundingInformation
******************
Provides information about the individual, agency and/or grant(s) which funded the described entity. Lists a reference to the agency or individual as described by a DDI Agent, the role of the funder, the grant number(s) and a description of the funding activity.



Extends
=======
:ref:`Identifiable`


Properties
==========

===========  =================================  ===========
Name         Type                               Cardinality
===========  =================================  ===========
funderRole   ExternalControlledVocabularyEntry  0..1
grantNumber  String                             0..n
purpose      StructuredString                   0..1
===========  =================================  ===========


funderRole
##########
Role of the funding organization or individual. Supports the use of a controlled vocabulary.


grantNumber
###########
The identification code of the grant or other monetary award which provided funding for the described object.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex

`


Relationships
=============

==========  =====  ===========
Name        Type   Cardinality
==========  =====  ===========
hasAFunder  Agent  0..n
==========  =====  ===========


hasAFunder
##########
Funding has been provided by






Graph
=====

.. graphviz:: /images/graph/Utility/FundingInformation.dot