.. _ExternalMaterial:


ExternalMaterial
****************
ExternalMaterial describes the location, structure, and relationship to the DDI metadata instance for any material held external to that instance. This includes citations to such material, an external reference to a URL (or other URI), and a statement about the relationship between the cited ExternalMaterial the contents of the DDI instance. It should be used as follows: As an extension base for specific external materials found within DDI (such as an External Aid); as a target object from a relationship which clarifies its role within a class; or as the target of a relatedResource within an annotation.




Extends
=======
:ref:`Identifiable`


Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
citationOfExternalMaterial  Annotation                         0..1
descriptiveText             StructuredString                   0..1
externalURLReference        anyURI                             0..n
externalURNReference        anyURI                             0..1
mimeType                    ExternalControlledVocabularyEntry  0..1
relationshipDescription     StructuredString                   0..n
segment                     Segment                            0..n
typeOfMaterial              ExternalControlledVocabularyEntry  0..1
==========================  =================================  ===========


citationOfExternalMaterial
##########################
Bibliographic citation for the external resource.


descriptiveText
###############
A description of the referenced material. This field can map to a Dublin Core abstract. Note that Dublin Core does not support structure within the abstract element. Supports multiple languages and optional structured content.


externalURLReference
####################
Contains a URL which indicates the location of the cited external resource.


externalURNReference
####################
Contains a URN which identifies the cited external resource.


mimeType
########
Provides a standard Internet MIME type for use by processing applications.


relationshipDescription
#######################
Describes the reason the external material is being related to the DDI metadata instance.


segment
#######
Can describe a segment within a larger object such as a text or video segment.


typeOfMaterial
##############
Designation of the type of material being described. Supports the use of a controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/Utility/ExternalMaterial.dot