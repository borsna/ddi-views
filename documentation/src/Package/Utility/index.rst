*******
Utility
*******

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   DDI4Version
   DocumentInformation
   ExternalMaterial
   FundingInformation
   Note



Graph
=====

.. graphviz:: /images/graph/Utility/Utility.dot