.. _ProcessStep:


ProcessStep
***********
One of the constituents of a Process. It can be a composition or atomic and might be performed by a Service.



Extends
=======
:ref:`Identifiable`


Relationships
=============

=============  =========  ===========
Name           Type       Cardinality
=============  =========  ===========
hasInterface   Interface  0..n
isPerformedBy  Service    0..n
=============  =========  ===========


hasInterface
############
Collection of Inputs and Outputs of the Process Step.




isPerformedBy
#############
Service performing the Process Step.






Graph
=====

.. graphviz:: /images/graph/ProcessPattern/ProcessStep.dot