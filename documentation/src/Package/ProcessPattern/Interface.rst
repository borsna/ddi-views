.. _Interface:


Interface
*********
Collection of Inputs and Outputs of the Process Step.



Extends
=======
:ref:`Collection`


Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
contains  InputOutput  0..n
========  ===========  ===========


contains
########
Specialization of contains in Collection for Inputs and Outputs of Process Steps.






Graph
=====

.. graphviz:: /images/graph/ProcessPattern/Interface.dot