.. _ProcessControlStep:


ProcessControlStep
******************
A Process Step that controls the execution flow of the Process by determining the next Process Step in its scope.



Extends
=======
:ref:`ProcessStep`


Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
contains  ProcessStep      0..n
defines   InformationFlow  0..n
========  ===============  ===========


contains
########
Process Steps in scope of the Process Control Step




defines
#######
Mappings between Inputs and Outputs of Process Steps in scope of the Process Control Step.






Graph
=====

.. graphviz:: /images/graph/ProcessPattern/ProcessControlStep.dot