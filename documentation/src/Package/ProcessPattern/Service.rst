.. _Service:


Service
*******
A means of performing a Process Step as part of a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve). 





Extends
=======
:ref:`Identifiable`


Properties
==========

=========  =================================  ===========
Name       Type                               Cardinality
=========  =================================  ===========
interface  ExternalControlledVocabularyEntry  0..1
location   ExternalControlledVocabularyEntry  0..1
=========  =================================  ===========


interface
#########
Specifies how to communicate with the service.


location
########
Specifies where the service can be accessed.

`


Relationships
=============

========  =====  ===========
Name      Type   Cardinality
========  =====  ===========
hasAgent  Agent  0..n
========  =====  ===========


hasAgent
########
Actor that performs a role in the service,






Graph
=====

.. graphviz:: /images/graph/ProcessPattern/Service.dot