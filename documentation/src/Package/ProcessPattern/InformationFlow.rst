.. _InformationFlow:


InformationFlow
***************
Mapping between inputs and outputs of Process Steps representing the flow of information to, from and within a Process.



Extends
=======
:ref:`UnorderedTuple`


Relationships
=============

====  ===========  ===========
Name  Type         Cardinality
====  ===========  ===========
maps  InputOutput  0..n
====  ===========  ===========


maps
####
Specialization of maps in Unordered Tuple for Input/Outputs of Process Steps






Graph
=====

.. graphviz:: /images/graph/ProcessPattern/InformationFlow.dot