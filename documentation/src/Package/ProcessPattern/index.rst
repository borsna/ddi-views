**************
ProcessPattern
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   InformationFlow
   InputOutput
   Interface
   ProcessControlStep
   ProcessStep
   Service



Graph
=====

.. graphviz:: /images/graph/ProcessPattern/ProcessPattern.dot