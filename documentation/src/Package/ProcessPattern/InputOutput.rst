.. _InputOutput:


InputOutput
***********
An Input or Output to a Process Step. 



Extends
=======
:ref:`Member


Graph
=====

.. graphviz:: /images/graph/ProcessPattern/InputOutput.dot