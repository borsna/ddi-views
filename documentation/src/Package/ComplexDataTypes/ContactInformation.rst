.. _ContactInformation:


ContactInformation
******************
Contact information for the individual or organization including location specification, address, URL, phone numbers, and other means of communication access. Address, location, telephone, and other means of communication can be repeated to express multiple means of a single type or change over time. Each major piece of contact information (with the exception of URL) contains the element EffectiveDates in order to date stamp the period for which the information is valid.

`


Properties
==========

===================  =======================  ===========
Name                 Type                     Cardinality
===================  =======================  ===========
address              Address                  0..n
electronicMessaging  ElectronicMessageSystem  0..n
email                Email                    0..n
telephone            Telephone                0..n
website              URL                      0..n
===================  =======================  ===========


address
#######
The address for contact.


electronicMessaging
###################
Electronic messaging other than email


email
#####
Email contact information


telephone
#########
Telephone for contact


website
#######
The URL of the Agent's website




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ContactInformation.dot