.. _CorrespondenceType:


CorrespondenceType
******************
Describes the commonalities and differences between two members using a textual description of both commonalities and differences plus an optional coding of the type of commonality.

`


Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
commonality          StructuredString                   0..1
commonalityTypeCode  ExternalControlledVocabularyEntry  0..n
difference           StructuredString                   0..1
===================  =================================  ===========


commonality
###########
A description of the common features of the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.


commonalityTypeCode
###################
Commonality expressed as a term or code. Supports the use of an external controlled vocabulary. If repeated, clarify each external controlled vocabulary used.


difference
##########
A description of the differences between the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CorrespondenceType.dot