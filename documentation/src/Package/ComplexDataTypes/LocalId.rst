.. _LocalId:


LocalId
*******
This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id.

`


Properties
==========

==============  ======  ===========
Name            Type    Cardinality
==============  ======  ===========
localIdType     String  1..1
localIdValue    String  1..1
localIdVersion  String  0..1
==============  ======  ===========


localIdType
###########
Type of identifier, specifying the context of the identifier.


localIdValue
############
Value of the local ID.


localIdVersion
##############
Version of the Local ID.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/LocalId.dot