.. _Image:


Image
*****
A reference to an image, with a description of its properties and type.

`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
dpi              Integer                            0..1
imageLocation    anyURI                             0..1
languageOfImage                                     0..1
typeOfImage      ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


dpi
###
Provides the resolution of the image in dots per inch to assist in selecting the appropriate image for various uses.


imageLocation
#############
A reference to the location of the image using a URI.


languageOfImage
###############
Language of image.


typeOfImage
###########
Brief description of the image type. Supports the use of an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Image.dot