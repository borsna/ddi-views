.. _AgentId:


AgentId
*******
Persistent identifier for a researcher using a system like ORCID

`


Properties
==========

============  ======  ===========
Name          Type    Cardinality
============  ======  ===========
agentIdType   String  1..1
agentIdValue  String  1..1
============  ======  ===========


agentIdType
###########
The identifier system in use.


agentIdValue
############
The identifier for the agent.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AgentId.dot