.. _Command:


Command
*******
Provides the following information on the command The content of the command, the programming language used, the pieces of information (InParameters) used by the command, the pieces of information created by the command (OutParameters) and the source of the information used by the InParameters (Binding).

`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
commandContent   String                             0..1
programLanguage  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


commandContent
##############
Content of the command itself expressed in the language designated in Programming Language.


programLanguage
###############
Designates the programming language used for the command. Supports the use of a controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Command.dot