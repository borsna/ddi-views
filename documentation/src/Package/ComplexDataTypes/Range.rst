.. _Range:


Range
*****
Indicates the range of items expressed as a string, such as an alphabetic range.

`


Properties
==========

============  ==========  ===========
Name          Type        Cardinality
============  ==========  ===========
maximumValue  RangeValue  0..1
minimumValue  RangeValue  0..1
rangeUnit     String      0..1
============  ==========  ===========


maximumValue
############
Maximum value in the range.


minimumValue
############
Minimum value in the range.


rangeUnit
#########
Specifies the units in the range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Range.dot