.. _ElectronicMessageSystem:


ElectronicMessageSystem
***********************
Any non-email means of relaying a message electronically. This would include text messaging, Skype, Twitter, ICQ, or other emerging means of electronic message conveyance. 

`


Properties
==========

==============  =================================  ===========
Name            Type                               Cardinality
==============  =================================  ===========
contactAddress  String                             0..1
effectiveDates  DateRange                          0..1
isPreferred     Boolean                            0..1
privacy         ExternalControlledVocabularyEntry  0..1
typeOfService   ExternalControlledVocabularyEntry  0..1
==============  =================================  ===========


contactAddress
##############
 Account identification for contacting


effectiveDates
##############
Time period during which the account is valid.


isPreferred
###########
Set to "true" if this is the preferred address.


privacy
#######
Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary.


typeOfService
#############
Indicates the type of service used. Supports the use of a controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ElectronicMessageSystem.dot