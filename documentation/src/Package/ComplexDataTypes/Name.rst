.. _Name:


Name
****
A standard means of expressing a Name for a class object.  A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. In general the property name should be "name" as it is the name of the class object which contains it. Use a specific name (i.e. xxxName) only when naming something other than the class object which contains it.

`


Properties
==========

=======  =================================  ===========
Name     Type                               Cardinality
=======  =================================  ===========
content  String                             0..1
context  ExternalControlledVocabularyEntry  0..1
=======  =================================  ===========


content
#######
The expressed name of the object.


context
#######
A name may be specific to a particular context, i.e., a type of software, or a section of a registry. Identify the context related to the specified name. 




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Name.dot