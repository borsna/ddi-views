.. _CharacterOffset:


CharacterOffset
***************
Specification of the character offset for the beginning and end of the segment, or beginning and length.

`


Properties
==========

===============  =======  ===========
Name             Type     Cardinality
===============  =======  ===========
characterLength  Integer  0..1
endCharOffset    Integer  0..1
startCharOffset  Integer  0..1
===============  =======  ===========


characterLength
###############
can be used to describe a text segment as start and length


endCharOffset
#############
Number of characters from the beginning of the document, indicating the inclusive end of the text segment.


startCharOffset
###############
Number of characters from beginning of the document, indicating the inclusive start of the text range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CharacterOffset.dot