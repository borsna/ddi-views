.. _Software:


Software
********
Describes a specific software package, which may be commercially available or custom-made.

`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
function         ExternalControlledVocabularyEntry  0..n
issueDate        Date                               0..1
packageLanguage                                     0..1
purpose          StructuredString                   0..1
softwareName     Name                               0..n
softwarePackage  ExternalControlledVocabularyEntry  0..1
softwareVersion  String                             0..1
usage            StructuredString                   0..1
===============  =================================  ===========


function
########
Identifies the functions handled by this software. Repeat for multiple functions. It may be advisable to note only those functions used in the specific usage of the software.


issueDate
#########
Supported date of the software package with, at minimum, a release date if known.


packageLanguage
###############
Language (human language) of the software package. Note that language allows for a simple 2 or 3 character language code or a language code extended by a country code , for example en-au for English as used in Australia.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


softwareName
############
The name of the software package, including its producer.


softwarePackage
###############
A coded value from a controlled vocabulary, describing the software package.


softwareVersion
###############
The version of the software package. Defaults to '1.0'.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Software.dot