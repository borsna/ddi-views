.. _DisplayLabel:


DisplayLabel
************
A structured display label. Label provides display content of a fully human readable display for the identification of the object. 



Extends
=======
:ref:`StructuredString`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
locationVariant  ExternalControlledVocabularyEntry  0..1
maxLength        Integer                            0..1
validDates       DateRange                          0..1
===============  =================================  ===========


locationVariant
###############
Indicate the locality specification for content that is specific to a geographic area. May be a country code, sub-country code, or area name.


maxLength
#########
A positive integer indicating the maximum number of characters in the label.


validDates
##########
Allows for the specification of a starting date and ending date for the period that this label is valid. 




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/DisplayLabel.dot