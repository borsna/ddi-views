.. _XMLPrefixMap:


XMLPrefixMap
************
Maps a specified prefix to a namespace. For each XML namespace used in the profile's XPath expressions, the XML namespaces must have their prefix specified using this element.

`


Properties
==========

============  ======  ===========
Name          Type    Cardinality
============  ======  ===========
xmlNamespace  String  0..1
xmlPrefix     String  0..1
============  ======  ===========


xmlNamespace
############
Specify the namespace which the prefix represents.


xmlPrefix
#########
Specify the exact prefix used.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/XMLPrefixMap.dot