.. _Address:


Address
*******
Location address identifying each part of the address as separate elements, identifying the type of address, the level of privacy associated with the release of the address, and a flag to identify the preferred address for contact.

`


Properties
==========

================  =================================  ===========
Name              Type                               Cardinality
================  =================================  ===========
cityPlaceLocal    String                             0..1
countryCode       ExternalControlledVocabularyEntry  0..1
effectivePeriod   UnlimitedNatural                   0..1
geographicPoint   Point                              0..1
isPreferred       Boolean                            0..1
line              String                             0..n
locationName      Name                               0..1
postalCode        String                             0..1
privacy           ExternalControlledVocabularyEntry  0..1
regionalCoverage  ExternalControlledVocabularyEntry  0..1
stateProvince     String                             0..1
timeZone          ExternalControlledVocabularyEntry  0..1
typeOfAddress     ExternalControlledVocabularyEntry  0..1
typeOfLocation    ExternalControlledVocabularyEntry  0..1
================  =================================  ===========


cityPlaceLocal
##############
City, place, or local area used as part of an address.


countryCode
###########
Country of the location


effectivePeriod
###############
Clarifies when the identification information is accurate.


geographicPoint
###############
Geographic coordinates corresponding to the address.


isPreferred
###########
Set to "true" if this is the preferred location for contacting the organization or individual.


line
####
Number and street including office or suite number. May use multiple lines.


locationName
############
Name of the location if applicable.


postalCode
##########
Postal or ZIP Code


privacy
#######
Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary


regionalCoverage
################
The region covered by the agent at this address


stateProvince
#############
A major subnational division such as a state or province used to identify a major region within an address.


timeZone
########
Time zone of the location expressed as code.


typeOfAddress
#############
Indicates address type (i.e. home, office, mailing, etc.)


typeOfLocation
##############
The type or purpose of the location (i.e. regional office, distribution center, home)




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Address.dot