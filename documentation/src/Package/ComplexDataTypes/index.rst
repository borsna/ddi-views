****************
ComplexDataTypes
****************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   ArrayBaseCode
   CategoryRelationCode
   CollectionType
   ComputationBaseList
   ItemSequenceType
   OrderRelationshipType
   PointFormat
   ReflexivityType
   SexSpecificationType
   ShapeCoded
   SpatialObject
   SymmetryType
   TotalityType
   TransitivityType
   WhiteSpace
   AccessLocation
   Address
   AgentAssociation
   AgentId
   Annotation
   AnnotationDate
   AudioSegment
   BasedOnObject
   BibliographicName
   CharacterOffset
   Command
   CommandCode
   CommandFile
   ConditionalText
   ContactInformation
   Content
   ContentDateOffset
   CorrespondenceType
   Date
   DateRange
   DescribedRelationship
   DisplayLabel
   DynamicText
   ElectronicMessageSystem
   Email
   ExternalControlledVocabularyEntry
   Form
   Image
   ImageArea
   IndividualName
   InternationalIdentifier
   InternationalString
   LineParameter
   LiteralText
   LocalId
   LocationName
   Name
   NonIsoDateType
   OrganizationName
   PairedExternalControlledVocabularyEntry
   Point
   Polygon
   PrivateImage
   Range
   RangeValue
   ReferenceDate
   ResourceIdentifier
   Segment
   Software
   SpatialCoordinate
   SpecificSequence
   StandardKeyValuePair
   Statistic
   String
   StructuredCommand
   StructuredString
   TargetSample
   Telephone
   Text
   TextContent
   TextualSegment
   TypedDescriptiveText
   URI
   URL
   Value
   VideoSegment
   XMLPrefixMap



Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ComplexDataTypes.dot