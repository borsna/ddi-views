.. _DescribedRelationship:


DescribedRelationship
*********************
Relationship specification between this item and the item to which it is related. Provides a reference to any identifiable object and a description of the relationship.

`


Properties
==========

=========  ================  ===========
Name       Type              Cardinality
=========  ================  ===========
rationale  StructuredString  0..1
=========  ================  ===========


rationale
#########
Explanation of the reasons for relating the external material to the identified object. Supports the use of multiple languages and structured text.  

`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
relatedTo  Identifiable  0..n
=========  ============  ===========


relatedTo
#########
Reference to the item within the DDI Instance to which this item is related.






Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/DescribedRelationship.dot