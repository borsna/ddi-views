.. _Date:


Date
****
Provides the structure of a single Date expressed in an ISO date structure along with equivalent expression in any number of non-ISO formats. While it supports the use of the ISO time interval structure this should only be used when the exact date is unclear (i.e. occurring at some point in time between the two specified dates) or in specified applications. Ranges with specified start and end dates should use the DateRange as a datatype. Commonly uses property names include: eventDate, issueDate, and releaseDate.

`


Properties
==========

==========  ==============  ===========
Name        Type            Cardinality
==========  ==============  ===========
isoDate     IsoDate         0..1
nonIsoDate  NonIsoDateType  0..n
==========  ==============  ===========


isoDate
#######
Strongly recommend that ALL dates be expressed in an ISO format at a minimum. A single point in time expressed in an ISO standard structure. Note that while it supports an ISO date range structure this should be used in Date only when the single date is unclear i.e. occurring at some time between two dates. 


nonIsoDate
##########
A simple date expressed in a non-ISO date format, including a specification of the date format and calendar used.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Date.dot