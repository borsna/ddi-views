.. _SpecificSequence:


SpecificSequence
****************
Describes the ordering of items when not otherwise indicated. There are a set number of values for ItemSequenceType, but also a provision for describing an alternate ordering using a command language.

`


Properties
==========

=================  ================  ===========
Name               Type              Cardinality
=================  ================  ===========
alternateSequence  CommandCode       0..1
itemSequence       ItemSequenceType  0..1
=================  ================  ===========


alternateSequence
#################
Information on the command used to generate an alternative means of determining sequence changes. If used, the ItemSequenceType should be "Other".


itemSequence
############
Identifies the type of sequence to use. Values include InOrderOfAppearance, Random, Rotate, and Other.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/SpecificSequence.dot