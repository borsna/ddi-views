.. _ReferenceDate:


ReferenceDate
*************
The date covered by the annotated object. In addition to specifying a type of date (e.g. collection period, census year, etc.) the date or time span may be associated with a particular subject or keyword. This allows for the expression of a referent date associated with specific subjects or keywords. For example, a set of date items on income and labor force status may have a referent date for the year prior to the collection date. To express a duration the preference is for Start and End dates expressing two time points separated by a "/". Note that if needed you may use Start and Duration or Duration and End. These options may not be recognizable by all systems.



Extends
=======
:ref:`AnnotationDate`


Properties
==========

=======  =================================  ===========
Name     Type                               Cardinality
=======  =================================  ===========
keyword  ExternalControlledVocabularyEntry  0..n
subject  ExternalControlledVocabularyEntry  0..n
=======  =================================  ===========


keyword
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use keyword to specify the coverage of the data this date applies to. May be repeated to reflect multiple keywords.


subject
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use Subject to specify the coverage of the data this date applies to. May be repeated to reflect multiple subjects.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ReferenceDate.dot