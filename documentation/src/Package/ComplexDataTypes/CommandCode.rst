.. _CommandCode:


CommandCode
***********
Contains information on the command used for processing data. Contains a description of the command which should clarify for the user the purpose and process of the command, an in-line provision of the command itself, a reference to an external version of the command such as a coding script, and the option for attaching an extension to DDI to permit insertion of a command code in a foreign namespace. The definition of the InParameter, OutParameter, and Binding declared within CommandCode are available for use by all formats of the command.

`


Properties
==========

=================  =================  ===========
Name               Type               Cardinality
=================  =================  ===========
command            Command            0..n
commandFile        CommandFile        0..n
description        StructuredString   0..1
structuredCommand  StructuredCommand  0..1
=================  =================  ===========


command
#######
This is an in-line provision of the command itself. 


commandFile
###########
Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file, designates input and output parameters, binding information between input and output parameters, a description of the location of the file , and a URN or URL for the command file.


description
###########
A description of the purpose and use of the command code provided. Supports multiple languages.


structuredCommand
#################
The is an extension stub to allow for the insertion of command code using an external namespace.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CommandCode.dot