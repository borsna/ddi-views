.. _TextContent:


TextContent
***********
Abstract type existing as the head of a substitution group. May be replaced by any valid member of the substitution group TextContent. Provides the common property of purpose to all members using TextContent as an extension base.

`


Properties
==========

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
orderPosition  Integer           0..1
purpose        StructuredString  0..1
=============  ================  ===========


orderPosition
#############
Provides the relative order of TextContent objects in a dynamic text with more than one TextContent object. Uses integer value.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/TextContent.dot