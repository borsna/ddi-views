.. _PairedExternalControlledVocabularyEntry:


PairedExternalControlledVocabularyEntry
***************************************
A tightly bound pair of items from an external controlled vocabulary. The extent property describes the extent to which the parent term applies for the specific case. 



Extends
=======
:ref:`ExternalControlledVocabularyEntry`


Properties
==========

======  =================================  ===========
Name    Type                               Cardinality
======  =================================  ===========
extent  ExternalControlledVocabularyEntry  0..1
======  =================================  ===========


extent
######
Describes the extent to which the parent term applies for the specific case using an external controlled vocabulary. When associated with a role from the CASRAI Contributor Roles Taxonomy an appropriate vocabulary should be specified as either ‘lead’, ‘equal’, or ‘supporting’.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/PairedExternalControlledVocabularyEntry.dot