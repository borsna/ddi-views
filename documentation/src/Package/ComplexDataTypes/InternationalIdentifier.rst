.. _InternationalIdentifier:


InternationalIdentifier
***********************
An identifier whose scope of uniqueness is broader than the local archive. Common forms of an international identifier are ISBN, ISSN, DOI or similar designator. Provides both the value of the identifier and the agency who manages it.

`


Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
identifierContent  String                             0..1
isURI              Boolean                            0..1
managingAgency     ExternalControlledVocabularyEntry  0..1
=================  =================================  ===========


identifierContent
#################
An identifier as it should be listed for identification purposes.


isURI
#####
Set to "true" if Identifier is a URI


managingAgency
##############
The identification of the Agency which assigns and manages the identifier, i.e., ISBN, ISSN, DOI, etc.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/InternationalIdentifier.dot