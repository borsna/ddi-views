.. _RangeValue:


RangeValue
**********
Describes a bounding value of a string.



Extends
=======
:ref:`Value`


Properties
==========

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
included  Boolean  0..1
========  =======  ===========


included
########
Set to "true" if the value is included in the range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/RangeValue.dot