.. _ExternalControlledVocabularyEntry:


ExternalControlledVocabularyEntry
*********************************
Allows for unstructured content which may be taken from an externally maintained as an entry in a controlled vocabulary.If the content is from a controlled vocabulary provide the code value of the entry, as well as a reference to the controlled vocabulary from which the value is taken. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary. Note that DDI has published a number of controlled vocabularies applicable to several locations using the ExternalControlledVocabularyEntry structure. If the code portion of the controlled vocabulary entry is language specific (i.e. a list of keywords or subject headings) use language to specify that language. In most cases the code portion of an entry is not language specific although the description and usage may be managed in one or more languages. Use of shared controlled vocabularies helps support interoperability and machine actionability.

`


Properties
==========

==============================  ======  ===========
Name                            Type    Cardinality
==============================  ======  ===========
content                         String  0..1
controlledVocabularyAgencyName  String  0..1
controlledVocabularyID          String  0..1
controlledVocabularyName        String  0..1
controlledVocabularySchemeURN   String  0..1
controlledVocabularyURN         String  0..1
controlledVocabularyVersionID   String  0..1
language                                0..1
otherValue                      String  0..1
==============================  ======  ===========


content
#######
The value of the entry of the controlled vocabulary. If no controlled vocabulary is used the term is entered here and none of the properties defining the controlled vocabulary location are used.


controlledVocabularyAgencyName
##############################
The name of the agency maintaining the code list.


controlledVocabularyID
######################
The ID of the code list (controlled vocabulary) that the content was taken from.


controlledVocabularyName
########################
The name of the code list.


controlledVocabularySchemeURN
#############################
If maintained within a scheme, the URN of the scheme containing the codelist.


controlledVocabularyURN
#######################
The URN of the codelist.


controlledVocabularyVersionID
#############################
The version number of the code list (default is 1.0).


language
########
Language of the content value if applicable


otherValue
##########
If the value of the string is "Other" or the equivalent from the codelist, this attribute can provide a more specific value not found in the codelist.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ExternalControlledVocabularyEntry.dot