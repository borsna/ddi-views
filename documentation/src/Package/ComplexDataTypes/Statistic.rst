.. _Statistic:


Statistic
*********
The value of the statistics and whether it is weighted and/or includes missing values.

`


Properties
==========

===============  ===================  ===========
Name             Type                 Cardinality
===============  ===================  ===========
computationBase  ComputationBaseList  0..1
isWeighted       Boolean              0..1
value            Real                 0..n
===============  ===================  ===========


computationBase
###############
Defines the cases included in determining the statistic. The options are total=all cases, valid and missing (invalid); validOnly=Only valid values, missing (invalid) are not included in the calculation; missingOnly=Only missing (invalid) cases included in the calculation.


isWeighted
##########
Set to "true" if the statistic is weighted using the weight designated in VariableStatistics.


value
#####
The value of the statistic expressed as a decimal




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Statistic.dot