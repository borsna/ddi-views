.. _Annotation:


Annotation
**********
Provides annotation information on the object to support citation and crediting of the creator(s) of the object.

`


Properties
==========

======================  =================================  ===========
Name                    Type                               Cardinality
======================  =================================  ===========
abstract                InternationalString                0..1
alternativeTitle        InternationalString                0..n
contributor             AgentAssociation                   0..n
copyright               InternationalString                0..n
creator                 AgentAssociation                   0..n
date                    AnnotationDate                     0..n
identifier              InternationalIdentifier            0..n
informationSource       InternationalString                0..n
language                                                   0..n
provenance              InternationalString                0..n
publisher               AgentAssociation                   0..n
recordCreationDate      IsoDate                            0..1
recordLastRevisionDate  IsoDate                            0..1
relatedResource         ResourceIdentifier                 0..n
rights                  InternationalString                0..n
subTitle                InternationalString                0..n
title                   InternationalString                0..1
typeOfResource          ExternalControlledVocabularyEntry  0..n
versionIdentification   String                             0..1
versionResponsibility   AgentAssociation                   0..n
======================  =================================  ===========


abstract
########
An abstract (description) of the annotated object.


alternativeTitle
################
An alternative title by which a data collection is commonly referred, or an abbreviation  for the title.


contributor
###########
The name of a contributing author or creator, who worked in support of the primary creator given above.


copyright
#########
The copyright statement.


creator
#######
Person, corporate body, or agency responsible for the substantive and intellectual content of the described object.


date
####
A date associated with the annotated object (not the coverage period). Use typeOfDate to specify the type of date such as Version, Publication, Submitted, Copyrighted, Accepted, etc.


identifier
##########
An identifier or locator. Contains identifier and Managing agency (ISBN, ISSN, DOI, local archive). Indicates if it is a URI.


informationSource
#################
The name or identifier of source information for the annotated object.


language
########
Language of the intellectual content of the described object. Strongly recommend the use of language codes supported by xs:language which include the 2 and 3 character and extended structures defined by RFC4646 or its successors.


provenance
##########
A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.


publisher
#########
Person or organization responsible for making the resource available in its present form.


recordCreationDate
##################
Date the record was created


recordLastRevisionDate
######################
Date the record was last revised


relatedResource
###############
Provide the identifier, managing agency, and type of resource related to this object. 


rights
######
Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.


subTitle
########
Secondary or explanatory title.


title
#####
Full authoritative title. List any additional titles for this item as AlternativeTitle.


typeOfResource
##############
Provide the type of the resource. This supports the use of a controlled vocabulary. It should be appropriate to the level of the annotation.


versionIdentification
#####################
Means of identifying the current version of the annotated object. 


versionResponsibility
#####################
The agent responsible for the version. May have an associated role.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Annotation.dot