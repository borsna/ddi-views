.. _TypedDescriptiveText:


TypedDescriptiveText
********************
This Complex Data Type bundles a descriptiveText with an External Controlled Vocabulary Entry allowing structured content and a means of typing that content. For example specifying that the description provides a Table of Contents for a document.

`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
contentCoverage  ExternalControlledVocabularyEntry  0..1
descriptiveText  StructuredString                   0..1
===============  =================================  ===========


contentCoverage
###############
Uses a controlled vocabulary entry to classify the description provided.


descriptiveText
###############
A short natural language account of the characteristics of the object.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/TypedDescriptiveText.dot