.. _URI:


URI
***
A URN or URL for a file with a flag to indicate if it is a public copy.

`


Properties
==========

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
content   anyURI   0..1
isPublic  Boolean  0..1
========  =======  ===========


content
#######
The URI in valid format


isPublic
########
Set to "true" (default value) if this file is publicly available. This does not imply that there are not restrictions to access. Set to "false" if this is not publicly available, such as a backup copy, an internal processing data file, etc.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/URI.dot