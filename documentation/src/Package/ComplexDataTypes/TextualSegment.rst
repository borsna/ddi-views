.. _TextualSegment:


TextualSegment
**************
Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment.

`


Properties
==========

==================  ===============  ===========
Name                Type             Cardinality
==================  ===============  ===========
characterParameter  CharacterOffset  0..1
lineParamenter      LineParameter    0..1
==================  ===============  ===========


characterParameter
##################
Specification of the character offset for the beginning and end of the segment.


lineParamenter
##############
Specification of the line and offset for the beginning and end of the segment.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/TextualSegment.dot