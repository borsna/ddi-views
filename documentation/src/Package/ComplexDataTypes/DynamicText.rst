.. _DynamicText:


DynamicText
***********
Structure supporting the use of dynamic text, where portions of the textual content change depending on external information (pre-loaded data, response to an earlier query, environmental situations, etc.).

`


Properties
==========

===================  ===========  ===========
Name                 Type         Cardinality
===================  ===========  ===========
audienceLanguage                  0..1
content              TextContent  1..n
isStructureRequired  Boolean      0..1
===================  ===========  ===========


audienceLanguage
################
Specifies the language of the intended audience. This is particularly important for clarifying the primary language of a mixed language textual string, for example when language testing and using a foreign word withing the question text.


content
#######
This is the head of a substitution group and is never used directly as an element name. Instead it is replaced with either LiteralText or ConditionalText.


isStructureRequired
###################
If textual structure (e.g. size, color, font, etc.) is required to understand the meaning of the content change value to "true".




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/DynamicText.dot