.. _IndividualName:


IndividualName
**************
The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix. The preferred compilation of the name parts may also be provided. The legal or formal name of the individual should have the isFormal attribute set to true. The preferred name should be noted with the isPreferred attribute. The attribute sex provides information to assist in the appropriate use of pronouns.

`


Properties
==========

====================  =================================  ===========
Name                  Type                               Cardinality
====================  =================================  ===========
abbreviation          InternationalString                0..1
context               String                             0..1
effectiveDates        DateRange                          0..1
firstGiven            String                             0..1
fullName              InternationalString                0..1
isFormal              Boolean                            0..1
isPreferred           Boolean                            0..1
lastFamily            String                             0..1
middle                String                             0..n
prefix                String                             0..1
sex                   SexSpecificationType               0..1
suffix                String                             0..1
typeOfIndividualName  ExternalControlledVocabularyEntry  0..1
====================  =================================  ===========


abbreviation
############
An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.


context
#######
A name may be specific to a particular context, i.e. common usage, business, social, etc.. Identify the context related to the specified name.


effectiveDates
##############
Clarifies when the name information is accurate.


firstGiven
##########
First (given) name of the individual


fullName
########
This provides a means of providing a full name as a single object for display or print such as identification badges etc. For example a person with the name of William Grace for official use may prefer a display name of Bill Grace on a name tag or other informal publication.


isFormal
########
The legal or formal name of the individual should have the isFormal attribute set to true. To avoid confusion only one individual name should have the isFormal attribute set to true. Use the TypeOfIndividualName to further differentiate the type and applied usage when multiple names are provided.


isPreferred
###########
If more than one name for the object is provided, use the isPreferred attribute to indicate which is the preferred name content. All other names should be set to isPreferred="false".


lastFamily
##########
Last (family) name /surname of the individual


middle
######
Middle name or initial of the individual


prefix
######
Title that precedes the name of the individual, such as Ms., or Dr.


sex
###
Sex allows for the specification of male, female or neutral. The purpose of providing this information is to assist others in the appropriate use of pronouns when addressing the individual. Note that many countries/languages may offer a neutral pronoun form.


suffix
######
Title that follows the name of the individual, such as Esq.


typeOfIndividualName
####################
The type of individual name provided. the use of a controlled vocabulary is strongly recommended. At minimum his should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/IndividualName.dot