.. _NonIsoDateType:


NonIsoDateType
**************
Used to preserve an historical date, formatted in a non-ISO fashion.

`


Properties
==========

================  =================================  ===========
Name              Type                               Cardinality
================  =================================  ===========
calendar          ExternalControlledVocabularyEntry  0..1
dateContent       String                             1..1
nonIsoDateFormat  ExternalControlledVocabularyEntry  0..1
================  =================================  ===========


calendar
########
Specifies the type of calendar used (e.g., Gregorian, Julian, Jewish).


dateContent
###########
This is the date expressed in a non-ISO compliant structure. Primarily used to retain legacy content or to express non-Gregorian calender dates.


nonIsoDateFormat
################
Indicate the structure of the date provided in NonISODate. For example if the NonISODate contained 4/1/2000 the Historical Date Format would be mm/dd/yyyy. The use of a controlled vocabulary is strongly recommended to support interoperability.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/NonIsoDateType.dot