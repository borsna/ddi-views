.. _DateRange:


DateRange
*********
Expresses a date/time range using a start date and end date (both with the structure of Date and supporting the use of ISO and non-ISO date structures). Use in all locations where a range of dates is required, i.e. validFor, embargoPeriod, collectionPeriod, etc.

`


Properties
==========

=========  ====  ===========
Name       Type  Cardinality
=========  ====  ===========
endDate    Date  0..1
startDate  Date  0..1
=========  ====  ===========


endDate
#######
The date (time) designating the end of the period or range.


startDate
#########
The date (time) designating the beginning of the period or range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/DateRange.dot