.. _Form:


Form
****
A link to a form used by the metadata containing the form number, a statement regarding the contents of the form, a statement as to the mandatory nature of the form and a privacy level designation.

`


Properties
==========

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
formNumber  String               0..1
isRequired  Boolean              0..1
statement   InternationalString  0..1
uri         anyURI               0..1
==========  ===================  ===========


formNumber
##########
The number or other means of identifying the form.


isRequired
##########
Set to "true" if the form is required. Set to "false" if the form is optional.


statement
#########
A statement regarding the use, coverage, and purpose of the form.


uri
###
The URN or URL of the form.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Form.dot