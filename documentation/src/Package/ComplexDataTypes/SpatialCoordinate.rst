.. _SpatialCoordinate:


SpatialCoordinate
*****************
Lists the value and format type for the coordinate value. Note that this is a single value (X coordinate or Y coordinate) rather than a coordinate pair.

`


Properties
==========

===============  ===========  ===========
Name             Type         Cardinality
===============  ===========  ===========
coordinateType   PointFormat  0..1
coordinateValue  String       0..1
===============  ===========  ===========


coordinateType
##############
Identifies the type of point coordinate system using a controlled vocabulary. Point formats include decimal degree, degrees minutes seconds, decimal minutes, meters, and feet.


coordinateValue
###############
The value of the coordinate expressed as a string.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/SpatialCoordinate.dot