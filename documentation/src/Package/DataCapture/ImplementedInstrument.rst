.. _ImplementedInstrument:


ImplementedInstrument
*********************
ImplementedInstruments are mode and/or unit specific.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
displayLabel                DisplayLabel                       0..n
externalInstrumentLocation  anyURI                             0..n
name                        Name                               0..n
typeOfInstrument            ExternalControlledVocabularyEntry  0..1
usage                       StructuredString                   0..n
==========================  =================================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


externalInstrumentLocation
##########################
A reference to an external representation of the the data collection instrument, such as an image of a questionnaire or programming script.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


typeOfInstrument
################
Specification of the type of instrument according to the classification system of the documentor.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

=======  ====================  ===========
Name     Type                  Cardinality
=======  ====================  ===========
basedOn  ConceptualInstrument  0..n
=======  ====================  ===========


basedOn
#######
The ConceptualInstrument which informs the design of the ImplementedInstrument.






Graph
=====

.. graphviz:: /images/graph/DataCapture/ImplementedInstrument.dot