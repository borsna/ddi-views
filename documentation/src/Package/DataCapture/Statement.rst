.. _Statement:


Statement
*********
A Statement is human readable text or referred material.



Extends
=======
:ref:`InstrumentComponent`


Properties
==========

==================  =================================  ===========
Name                Type                               Cardinality
==================  =================================  ===========
purposeOfStatement  ExternalControlledVocabularyEntry  0..1
statementText       DynamicText                        0..n
==================  =================================  ===========


purposeOfStatement
##################
Describes the use of the statement within the instrument. For example, a section divider, welcome statement, or other text.


statementText
#############
Structured human-readable text that allows for dynamic content. For example, the insertion of a name or gender specific pronoun. Repeat ONLY for capturing the same content in multiple languages.




Graph
=====

.. graphviz:: /images/graph/DataCapture/Statement.dot