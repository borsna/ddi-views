.. _InstrumentComponent:


InstrumentComponent
*******************
InstrumentComponent is an abstract object which extends an Act (a type of Process Step). The purpose of InstrumentComponent is to provide a common parent for Capture (e.g., Question, Measure), Statement, and Instructions.





Extends
=======
:ref:`Act`


Relationships
=============

===============  ===========  ===========
Name             Type         Cardinality
===============  ===========  ===========
hasExternalAids  ExternalAid  0..1
hasInstructions  Instruction  0..1
===============  ===========  ===========


hasExternalAids
###############
Any external object used to clarify, inform, or support the role of the instrument component




hasInstructions
###############
An instrument compoment can have zero to many instructions






Graph
=====

.. graphviz:: /images/graph/DataCapture/InstrumentComponent.dot