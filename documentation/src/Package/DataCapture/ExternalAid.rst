.. _ExternalAid:


ExternalAid
***********
Any external material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made. 



Extends
=======
:ref:`ExternalMaterial`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
stimulusType  ExternalControlledVocabularyEntry  0..1
============  =================================  ===========


stimulusType
############





Graph
=====

.. graphviz:: /images/graph/DataCapture/ExternalAid.dot