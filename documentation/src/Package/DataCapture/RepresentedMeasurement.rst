.. _RepresentedMeasurement:


RepresentedMeasurement
**********************
The description of a reusable non-question measurement, that can be used as a template that describes the components of a measurement. A type code can be used to describe the type of measure that was used.



Extends
=======
:ref:`Capture`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
measurementType  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


measurementType
###############
The type of measurement performed

`


Relationships
=============

======================  ===================  ===========
Name                    Type                 Cardinality
======================  ===================  ===========
hasRepresentedVariable  RepresentedVariable  0..n
======================  ===================  ===========


hasRepresentedVariable
######################
An optional link to a represented variable which can be used by each instance variable created by a use of this measure.






Graph
=====

.. graphviz:: /images/graph/DataCapture/RepresentedMeasurement.dot