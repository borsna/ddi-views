.. _Capture:


Capture
*******
A measurement that describes a means of capturing data. This class can be extended to account for different specific means. Use a specific instantiation of a Capture to describe a means of capturing a measurement.






Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
analysisUnit     ExternalControlledVocabularyEntry  0..n
displayLabel     DisplayLabel                       0..n
measurementName  Name                               0..n
purpose          StructuredString                   0..1
source           ExternalControlledVocabularyEntry  0..1
usage            StructuredString                   0..1
===============  =================================  ===========


analysisUnit
############
Identifies the unit being analyzed such as a Person, Housing Unit, Enterprise, etc. 


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


measurementName
###############
A name for the measurement. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


purpose
#######
A description of the purpose or use of the Measurement. May be expressed in multiple languages and supports the use of structured content. 


source
######
The source of a capture structure defined briefly; typically using an external controlled vocabulary


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

======================  ===================  ===========
Name                    Type                 Cardinality
======================  ===================  ===========
hasConcept              Concept              0..n
hasExternalAid          ExternalAid          0..n
hasInstruction          Instruction          0..n
hasResponseDomain       ResponseDomain       0..1
intendedRepresentation  RepresentedVariable  0..1
======================  ===================  ===========


hasConcept
##########
Capture has a Concept




hasExternalAid
##############
Capture has an External Aid




hasInstruction
##############
Capture has an Instruction




hasResponseDomain
#################




intendedRepresentation
######################
Associates the InstatiatedMeasure and the InstantiatedQuestion with the RepresentedVariable they are intended to populate.






Graph
=====

.. graphviz:: /images/graph/DataCapture/Capture.dot