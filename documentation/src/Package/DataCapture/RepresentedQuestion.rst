.. _RepresentedQuestion:


RepresentedQuestion
*******************
The description of a reusable question, that can be used as a template that describes the components of a question



Extends
=======
:ref:`Capture`


Properties
==========

==============================  ================  ===========
Name                            Type              Cardinality
==============================  ================  ===========
estimatedResponseTimeInSeconds  Real              0..1
questionIntent                  StructuredString  0..1
questionText                    DynamicText       0..n
==============================  ================  ===========


estimatedResponseTimeInSeconds
##############################
An estimation of the number of seconds required to respond to the question. Used for estimating overall questionnaire completion time.


questionIntent
##############
The purpose or intent of the question.


questionText
############
The text of the question which may be literal or dynamic (altered to personalize the question text) in terms of content.

`


Relationships
=============

======================  ===================  ===========
Name                    Type                 Cardinality
======================  ===================  ===========
hasRepresentedVariable  RepresentedVariable  0..n
======================  ===================  ===========


hasRepresentedVariable
######################
An optional link to a represented variable which can be used by each instance variable created by a use of this question.






Graph
=====

.. graphviz:: /images/graph/DataCapture/RepresentedQuestion.dot