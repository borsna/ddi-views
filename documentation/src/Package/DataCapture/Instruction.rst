.. _Instruction:


Instruction
***********
Provides the content and description of data capture instructions. Contains the "how to" information for administering an instrument.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

===============  ============  ===========
Name             Type          Cardinality
===============  ============  ===========
displayLabel     DisplayLabel  0..n
instructionText  DynamicText   0..n
name             Name          0..n
===============  ============  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


instructionText
###############
The content of the Instruction text provided using DynamicText. Note that when using Dynamic Text, the full InstructionText must be repeated for multi-language versions of the content. The InstructionText may also be repeated to provide a dynamic and plain text version of the instruction. This allows for accurate rendering of the instruction in a non-dynamic environment like print.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 

`


Relationships
=============

==================  ================  ===========
Name                Type              Cardinality
==================  ================  ===========
associatedMaterial  ExternalMaterial  0..n
==================  ================  ===========


associatedMaterial
##################
An image or other external material associated with the Instruction, located at the provided URN or URL.






Graph
=====

.. graphviz:: /images/graph/DataCapture/Instruction.dot