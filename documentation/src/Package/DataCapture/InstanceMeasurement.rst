.. _InstanceMeasurement:


InstanceMeasurement
*******************
An instance measurement instantiates a represented measurement, so that it can be used as an Act in the Process Steps that define a data capture process.



Extends
=======
:ref:`InstrumentComponent`


Relationships
=============

============  ======================  ===========
Name          Type                    Cardinality
============  ======================  ===========
instantiates  RepresentedMeasurement  0..n
============  ======================  ===========


instantiates
############
The measurement being instantiated in the data collection process






Graph
=====

.. graphviz:: /images/graph/DataCapture/InstanceMeasurement.dot