***********
DataCapture
***********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Capture
   ConceptualInstrument
   ExternalAid
   ImplementedInstrument
   InstanceMeasurement
   InstanceQuestion
   Instruction
   InstrumentCode
   InstrumentComponent
   RepresentedMeasurement
   RepresentedQuestion
   ResponseDomain
   Statement



Graph
=====

.. graphviz:: /images/graph/DataCapture/DataCapture.dot