==============================
Packages
==============================
.. image:: ../../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   Agents/index.rst
   BaseObjects/index.rst
   ComplexDataTypes/index.rst
   ComplexProcess/index.rst
   Conceptual/index.rst
   CustomMetadata/index.rst
   Discovery/index.rst
   CollectionsPattern/index.rst
   Comparison/index.rst
   Correspondences/index.rst
   DataCapture/index.rst
   DDIDocument/index.rst
   DDIUtility/index.rst
   FormatDescription/index.rst
   Identification/index.rst
   LogicalDataDescription/index.rst
   Methodologies/index.rst
   MethodologyPattern/index.rst
   Primitives/index.rst
   ProcessPattern/index.rst
   SegmentLocationSpecifications/index.rst
   SignificationPattern/index.rst
   Representations/index.rst
   Workflows/index.rst
   Utility/index.rst
