.. _MeetsIntervalRelation:


MeetsIntervalRelation
*********************
Representation of the meets relation in Allen's interval algebra.

We say that an interval A meets another interval B if and only if A finishes when B begins.

More precisely, A.ends = B.start.

Instead of saying that A meets B we can also say that B is met by A (converse).



Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Temporal-Meets


symmetry
########
Fixed to Anti-Symmetric


transitivity
############
Fixed to Anti-Transitive

`


Relationships
=============

==========  ===========================  ===========
Name        Type                         Cardinality
==========  ===========================  ===========
contains    OrderedIntervalPair          0..n
realizes    ImmediatePrecedenceRelation  0..n
structures  WorkflowSequence             0..n
==========  ===========================  ===========


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class. 




structures
##########
Sequence to which the Temporal Interval Relation is applied






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/MeetsIntervalRelation.dot