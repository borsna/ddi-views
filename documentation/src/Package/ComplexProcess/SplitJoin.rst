.. _SplitJoin:


SplitJoin
*********
Here the process consists of concurrent execution of a bunch of process components with barrier synchronization. That is, SplitJoin completes when all of its components processes have completed. With Split and SplitJoin, we can define processes that have partial synchronization (e.g., split all and join some sub-bag)



Extends
=======
:ref:`ControlConstruct


Graph
=====

.. graphviz:: /images/graph/ComplexProcess/SplitJoin.dot