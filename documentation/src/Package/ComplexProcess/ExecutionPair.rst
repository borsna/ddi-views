.. _ExecutionPair:


ExecutionPair
*************
During execution ProcessSteps follow one another over time. This is also true when we parallelize because within each thread work flows. In every thread during execution we advance through a sequence of pairs. Each pair has a "prev" and a "cur". In the course of execution "cur" becomes "prev" and a new "cur" comes along.

In this context the Execution Pair is the current "prev" and "cur".

It is between the current "prev" and "cur" that messages flow. The output of prev can be mapped to the input of "cur". This mapping is commonly referred to as a "binding" or in the terms of a collection an OrderedMemberCorrespondence.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

====================  =================================  ===========
Name                  Type                               Cardinality
====================  =================================  ===========
executionPairPattern  ExternalControlledVocabularyEntry  0..1
====================  =================================  ===========


executionPairPattern
####################
An execution pair conforms to one of three patterns. In one case the pairs are siblings in a sequence. In a second case a binding might obtain between the control construct that parents the sequence and the sequence. In a third case a binding might obtain between the last element of a sequence and the output of the construct construct that has parented the sequence

`


Relationships
=============

==========  ===========  ===========
Name        Type         Cardinality
==========  ===========  ===========
hasBinding  Binding      0..n
realizes    OrderedPair  0..n
==========  ===========  ===========


hasBinding
##########




realizes
########
Class of the Collection pattern realized by this class.






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/ExecutionPair.dot