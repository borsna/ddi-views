.. _FinishesIntervalRelation:


FinishesIntervalRelation
************************
Representation of the finishes relation in Allen's interval algebra.

We say that an interval A finishes another interval B if and only if A begins after B but both finish at the same time.

More precisely, B.start < A.start < B.end = A.end.

Instead of saying that A finishes B we can also say that B is finished by A (converse)



Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Temporal-Finishes


symmetry
########
Fixed to Anti-Symmetric


transitivity
############
Fixed to Transitive


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
contains    OrderedIntervalPair  0..n
realizes    StrictOrderRelation  0..n
structures  WorkflowSequence     0..n
==========  ===================  ===========


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class. 




structures
##########
Sequence to which the Temporal Interval Relation is applied






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/FinishesIntervalRelation.dot