.. _StudyUnitControl:


StudyUnitControl
****************
StudyUnitControl references a StudyUnitType. StudyUnitControl enables the representation of a research protocol at the StudyUnit level. [If the only purpose of the object is to carry a StudyUnit, it should be seriously considered for melting with another object. Modeling rules state that any object needs to have a reality of its own. Carrying another object is not a reality. If I misunderstood, please clarify the description instead]



Extends
=======
:ref:`Act`


Properties
==========

============  ==========  ===========
Name          Type        Cardinality
============  ==========  ===========
hasStudyUnit  Annotation  0..1
============  ==========  ===========


hasStudyUnit
############
Provides the Annotation of the Study Unit




Graph
=====

.. graphviz:: /images/graph/ComplexProcess/StudyUnitControl.dot