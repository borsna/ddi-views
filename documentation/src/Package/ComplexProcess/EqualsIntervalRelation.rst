.. _EqualsIntervalRelation:


EqualsIntervalRelation
**********************
Representation of the equals relation in Allen's interval algebra.

We say that an interval A equals another interval B if and only if they both begin and finish at the same time.

More precisely, A.start = B.start < A.end = B.end.

Instead of saying that A equals B we can also say the B equals A (reflexive).




Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
============  =================================  ===========


reflexivity
###########
Fixed to Reflexive


semantics
#########
Fixed to Temporal-Equals


symmetry
########
Fixed to Symmetric


transitivity
############
Fixed to Transitive

`


Relationships
=============

==========  =====================  ===========
Name        Type                   Cardinality
==========  =====================  ===========
contains    UnorderedIntervalPair  0..n
realizes    EquivalenceRelation    0..n
structures  WorkflowSequence       0..n
==========  =====================  ===========


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class. 




structures
##########
Sequence to which the Temporal Interval Relation is applied






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/EqualsIntervalRelation.dot