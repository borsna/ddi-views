.. _Split:


Split
*****
The components of a Split process are a bag of process components to be executed concurrently. Split completes as soon as all of its component processes have been scheduled for execution



Extends
=======
:ref:`ControlConstruct


Graph
=====

.. graphviz:: /images/graph/ComplexProcess/Split.dot