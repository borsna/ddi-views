.. _StartsIntervalRelation:


StartsIntervalRelation
**********************
Representation of the starts relation in Allen's interval algebra.

We say that an interval A starts another interval B if and only if they both start at the same time but A finishes first.

More precisely, A.start = B.start < A.end <B.end

Instead of saying that A starts B we can also say that B is started by A (converse).




Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Temporal-Starts


symmetry
########
Fixed to Anti-Symmetric


transitivity
############
Fixed to Transitive


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
contains    OrderedIntervalPair  0..n
realizes    StrictOrderRelation  0..n
structures  WorkflowSequence     0..n
==========  ===================  ===========


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class. 




structures
##########
Sequence to which the Temporal Interval Relation is applied






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/StartsIntervalRelation.dot