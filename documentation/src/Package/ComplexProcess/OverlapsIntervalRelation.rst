.. _OverlapsIntervalRelation:


OverlapsIntervalRelation
************************
Representation of the overlaps relation in Allen's interval algebra.

We say that an interval A overlaps another interval B if and only if A begins before B but finishes during B.

More precisely, A.start < B.start < A.end < B.end.

Instead of saying that A overlaps B we can also say that B is overlapped by A (converse).




Extends
=======
:ref:`TemporalIntervalRelation`


Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
displayLabel  DisplayLabel                       0..1
reflexivity   ReflexivityType                    1..1
semantics     ExternalControlledVocabularyEntry  1..1
symmetry      SymmetryType                       1..1
transitivity  TransitivityType                   1..1
usage         StructuredString                   0..1
============  =================================  ===========


displayLabel
############
A display label for the OverlapsIntervalRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


reflexivity
###########
Fixed to Anti-Reflexive


semantics
#########
Fixed to Temporal_Overlaps


symmetry
########
Fixed to Anti-Symmetric


transitivity
############
Fixed to Neither


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.

`


Relationships
=============

==========  =========================  ===========
Name        Type                       Cardinality
==========  =========================  ===========
contains    OrderedIntervalPair        0..n
realizes    AcyclicPrecedenceRelation  0..n
structures  WorkflowSequence           0..n
==========  =========================  ===========


contains
########
Pairs of Process Steps in the temporal relation.




realizes
########
Class of the Collection pattern realized by this class. 




structures
##########
Sequence to which the Temporal Interval Relation is applied






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/OverlapsIntervalRelation.dot