.. _TransformationControl:


TransformationControl
*********************
Provides an extensible framework for specific transformation objects.



Extends
=======
:ref:`Act`


Properties
==========

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
activityDescription  StructuredString  0..1
===================  ================  ===========


activityDescription
###################
Describes the transformation activity

`


Relationships
=============

=================  ============  ===========
Name               Type          Cardinality
=================  ============  ===========
wasAssociatedWith  Agent         0..n
wasDerivedFrom     WorkflowStep  0..n
wasGeneratedBy     WorkflowStep  0..n
=================  ============  ===========


wasAssociatedWith
#################
The agent an activity is associated with




wasDerivedFrom
##############
The entity an activity uses




wasGeneratedBy
##############
The entity an activity generates






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/TransformationControl.dot