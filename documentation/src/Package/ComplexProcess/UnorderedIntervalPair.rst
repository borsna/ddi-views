.. _UnorderedIntervalPair:


UnorderedIntervalPair
*********************
Unordered pair of Process Steps in a Temporal Interval Relation.



Extends
=======
:ref:`Identifiable`


Relationships
=============

========  =============  ===========
Name      Type           Cardinality
========  =============  ===========
maps      WorkflowStep   0..n
realizes  UnorderedPair  0..n
========  =============  ===========


maps
####
Process Steps in the Unordered Pair. Process Steps that start and end at the same time




realizes
########
Class of the Collection pattern realized by this class. 






Graph
=====

.. graphviz:: /images/graph/ComplexProcess/UnorderedIntervalPair.dot