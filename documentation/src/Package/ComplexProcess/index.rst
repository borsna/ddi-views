**************
ComplexProcess
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   ContainsIntervalRelation
   EqualsIntervalRelation
   ExecutionPair
   FinishesIntervalRelation
   MeetsIntervalRelation
   OrderedIntervalPair
   OverlapsIntervalRelation
   PrecedesIntervalRelation
   Split
   SplitJoin
   StartsIntervalRelation
   StudyUnitControl
   TemporalIntervalRelation
   TransformationControl
   UnorderedIntervalPair



Graph
=====

.. graphviz:: /images/graph/ComplexProcess/ComplexProcess.dot