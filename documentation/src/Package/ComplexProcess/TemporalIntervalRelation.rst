.. _TemporalIntervalRelation:


TemporalIntervalRelation
************************
Relation that specifies temporal constraints between pairs of Process Steps in a Sequence following Allen's interval algebra. A Sequence in the Process Model is a Control Construct in which the elements of the sequence (i.e. Process Steps) have positions on a timeline with respect to each other.

In Allen's interval algebra between Process Steps a and b there are seven possible relations. All but one are asymmetric and thus have a converse. The exception is the situation where two intervals are coextensive. Then the relation and its converse are the same, namely equal, which is an equivalence relation.



Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

========  ============  ===========
Name      Type          Cardinality
========  ============  ===========
totality  TotalityType  1..1
========  ============  ===========


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.




Graph
=====

.. graphviz:: /images/graph/ComplexProcess/TemporalIntervalRelation.dot