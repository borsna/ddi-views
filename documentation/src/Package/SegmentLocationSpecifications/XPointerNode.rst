.. _XPointerNode:


XPointerNode
************
Contains an XPointer for identifying part of an XML document



Extends
=======
:ref:`Identifiable`


Properties
==========

==================  ======  ===========
Name                Type    Cardinality
==================  ======  ===========
xPointerExpression  String  0..1
==================  ======  ===========


xPointerExpression
##################
an XPointerExpression (http://www.w3.org/TR/xptr-xpointer/)




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/XPointerNode.dot