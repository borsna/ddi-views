.. _SegmentByNomenclature:


SegmentByNomenclature
*********************
The description of a segment using a descriptive term from a controlled vocabulary.



Extends
=======
:ref:`PhysicalSegmentLocation`


Properties
==========

====  =================================  ===========
Name  Type                               Cardinality
====  =================================  ===========
term  ExternalControlledVocabularyEntry  0..n
====  =================================  ===========


term
####
Points to the descriptive term expressed as a codeValue along with related information




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByNomenclature.dot