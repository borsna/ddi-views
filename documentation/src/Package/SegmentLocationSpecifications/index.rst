*****************************
SegmentLocationSpecifications
*****************************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   BeginEndClip
   PhysicalSegmentLocation
   SegmentByAudio
   SegmentByCustom
   SegmentByDDI
   SegmentByImage
   SegmentByNomenclature
   SegmentBySqlQuery
   SegmentByText
   SegmentByTriplestore
   SegmentByVideo
   SegmentByXML
   SparqlSelection
   SqlSelection
   StructureForSegmentOfCustom
   XPointerNode



Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentLocationSpecifications.dot