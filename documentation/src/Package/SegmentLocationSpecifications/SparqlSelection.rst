.. _SparqlSelection:


SparqlSelection
***************
Contains a SPARQL query and the necessary related information for retrieving a segment from a triple store.



Extends
=======
:ref:`Identifiable`


Properties
==========

===========  ======  ===========
Name         Type    Cardinality
===========  ======  ===========
endpoint     String  0..1
sparqlQuery  String  0..1
===========  ======  ===========


endpoint
########
A description of the endpoint to be queried by the SPARQL Query


sparqlQuery
###########
The text of the SPARQL query




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SparqlSelection.dot