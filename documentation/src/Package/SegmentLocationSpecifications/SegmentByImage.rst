.. _SegmentByImage:


SegmentByImage
**************
Defines an area of an image - e.g. by a rectangle, a polygon



Extends
=======
:ref:`PhysicalSegmentLocation`


Properties
==========

=========  =========  ===========
Name       Type       Cardinality
=========  =========  ===========
definedBy  ImageArea  0..1
=========  =========  ===========


definedBy
#########
The set of parameters describing the polygon, square, etc




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByImage.dot