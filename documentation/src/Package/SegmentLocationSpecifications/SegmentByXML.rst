.. _SegmentByXML:


SegmentByXML
************
Defines a segment of an XML file (or well formed HTML) using an XPointer.



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
definedBy  XPointerNode  1..1
=========  ============  ===========


definedBy
#########
The XPointers defining the segment.






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByXML.dot