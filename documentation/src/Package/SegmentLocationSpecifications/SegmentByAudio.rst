.. _SegmentByAudio:


SegmentByAudio
**************
A segment (clip) of an audio file



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
definedBy  BeginEndClip  1..1
=========  ============  ===========


definedBy
#########
The BeginEnd parameter specification(s) for the clip






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByAudio.dot