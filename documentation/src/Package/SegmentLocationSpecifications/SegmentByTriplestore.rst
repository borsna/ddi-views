.. _SegmentByTriplestore:


SegmentByTriplestore
********************
Points to a SPARQL query and related information 



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ===============  ===========
Name       Type             Cardinality
=========  ===============  ===========
definedBy  SparqlSelection  0..n
=========  ===============  ===========


definedBy
#########
Pointer to the SPARQL query and related information defining a segment of the triplestore






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByTriplestore.dot