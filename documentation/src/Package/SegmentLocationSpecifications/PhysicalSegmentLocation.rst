.. _PhysicalSegmentLocation:


PhysicalSegmentLocation
***********************
Defines the location of a segment in a DataStore (e.g. a text file). This is abstract since there are many different ways to describe the location of a segment - character counts, start and end times, etc.



Extends
=======
:ref:`AnnotatedIdentifiable


Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/PhysicalSegmentLocation.dot