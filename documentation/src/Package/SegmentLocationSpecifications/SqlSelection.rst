.. _SqlSelection:


SqlSelection
************
Contains an SQL statement and related information allowing the description of a segment of a relational database. 



Extends
=======
:ref:`Identifiable`


Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
databaseType     ExternalControlledVocabularyEntry  0..1
databaseVersion  String                             0..1
schemeName       String                             0..1
sqlStatement     String                             0..1
===============  =================================  ===========


databaseType
############
The type of the database. An example might be MySQL


databaseVersion
###############
The version of the database. For MySQl an example might be 5.7


schemeName
##########
The database schema


sqlStatement
############
The SQL statment (query)




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SqlSelection.dot