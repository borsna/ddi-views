.. _BeginEndClip:


BeginEndClip
************
Contains parameters for the beginning and ending of a Clip of a type drawn from a controlled vocabulary



Extends
=======
:ref:`Identifiable`


Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
clipBegin      Real                               0..1
clipEnd        Real                               0..1
clipType       ExternalControlledVocabularyEntry  0..1
otherClipType  String                             0..1
=============  =================================  ===========


clipBegin
#########
The parameter for the start of the clip


clipEnd
#######
the parameter for the end of the clip


clipType
########
The type of clip, preferably drawn from a controlled vocabulary.


otherClipType
#############
Text description of a choice of "other"




Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/BeginEndClip.dot