.. _SegmentByVideo:


SegmentByVideo
**************
Points to the BeginEndClip parameter specifications for a Video Clip



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
definedBy  BeginEndClip  1..1
=========  ============  ===========


definedBy
#########
The BeginEndClip parameter specification(s) for the clip






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByVideo.dot