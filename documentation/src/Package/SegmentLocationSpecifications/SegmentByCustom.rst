.. _SegmentByCustom:


SegmentByCustom
***************
Points to a CustomInstance and a StructureForSegmentByCustom to define segments using a scheme not built in to DDI. The CustomInstance contains a set of key, value pairs defining each segment. The StructureForSegmentByCustom defines the keys.



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=============  ===========================  ===========
Name           Type                         Cardinality
=============  ===========================  ===========
definedBy      CustomInstance               1..1
usesStructure  StructureForSegmentOfCustom  0..n
=============  ===========================  ===========


definedBy
#########
A CustomInstance which contains a set of CustomValues (key,value pairs) using the CustomStructure to define the location of a segment




usesStructure
#############
The CustomStructure defined for this method of describing segments






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByCustom.dot