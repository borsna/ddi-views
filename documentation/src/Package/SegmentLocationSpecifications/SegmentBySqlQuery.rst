.. _SegmentBySqlQuery:


SegmentBySqlQuery
*****************
Points to one or more specifications of an SQL query and related information



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
definedBy  SqlSelection  1..1
=========  ============  ===========


definedBy
#########
Pointer to the SQL statement and related information






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentBySqlQuery.dot