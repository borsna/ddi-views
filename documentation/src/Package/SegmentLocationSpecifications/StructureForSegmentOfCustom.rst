.. _StructureForSegmentOfCustom:


StructureForSegmentOfCustom
***************************
Defines a custom set of key,value pairs for the description of the location of segments in some type of PhysicalResource



Extends
=======
:ref:`CustomStructure


Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/StructureForSegmentOfCustom.dot