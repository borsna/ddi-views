.. _SegmentByDDI:


SegmentByDDI
************
Points to a set of DDI identifiers that together identify a segment of DDI.



Extends
=======
:ref:`PhysicalSegmentLocation`


Relationships
=============

=========  ============  ===========
Name       Type          Cardinality
=========  ============  ===========
definedBy  Identifiable  1..1
=========  ============  ===========


definedBy
#########
Pointers to  any DDI objects defined by a DDI Identifier (Agent, ID, Version)






Graph
=====

.. graphviz:: /images/graph/SegmentLocationSpecifications/SegmentByDDI.dot