*********************************
Associated Information and Files
*********************************

In addition to the documentation of the DDI-Views model. There are other products included with the review documents:
A fuller description can be found in the Production Framework section of *The model and its development*.

* Platform Specific Model (psm folder of the associated files bundle) 

  * ddi4psm.owl.xmi 
  * ddi4psm.xsd.xmi 

* Platform Specific Bindings (XML Schemas (XSD folder of the associated files bundle) 

  * xxxView.xsd - Functional Views
  * DDI_4-DR0.2.xsd - (the complete model for packages being released)
  * Other files are supporting schemas


