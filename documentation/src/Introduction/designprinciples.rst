Design Principles
==================

A set of design principles has been identified during the course of the development of DDI-Views, The list is shown below:

Design
-------

The model
* is developed in an agile, modular and iterative manner
* is responsive to community needs to produce actionable metadata
* should balance complexity with functionality and understandability
* is extensible and strives towards compatibility between different versions
* is maximally interoperable with relevant community standards
* supports a plurality of implementations

Documentation
--------------

The documentation of the model
* is clear, complete, and timely
* is concise, comprehensible, accessible, and useable by multiple communities
* provides justification for design decisions.
* provides reference and functional perspectives

Capability
-----------

The model and its documentation
* support the discovery, reuse, exchange, and sharing of (meta)data
* support the conceptualization, capture, production, management, and analysis of (meta)data
* support audit and reproducibility across the (meta)data lifecycle
